#include "CubicBezierTriangle3.h"
#include "../Core/Constants.h"
#include <algorithm>
#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    CubicBezierTriangle3::CubicBezierTriangle3():
        TenPointsBasedTriangle (TriangularMatrix<DCoordinate3>(4))
    {
        //that's how I will represent the used calculated integrals
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        //the calculated integrals, (x, y)[z]:   x -> x times partial derivate by the first parameter, y ->  y times partial derivate by the second parameter, z -> the z-th point's partial derivative
        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(1, 0)[7] = integrals(0, 1)[9] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = integrals(1, 0)[0] = -1.0 / 10.0;
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = 1.0 / 10.0;
        integrals(0, 1)[4] = integrals(1, 0)[4] = 1.0 / 5.0;

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(1, 1)[7] = integrals(0, 2)[9] = integrals(1, 1)[3] = integrals(1, 1)[5] = integrals(1, 1)[2] = integrals(2, 0)[0] = -3.0;
        integrals(1, 1)[6] = integrals(2, 0)[8] = integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(2, 0)[3] = integrals(1, 1)[8] = integrals(1, 1)[1] = integrals(2, 0)[1] = 3.0;
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = -6.0;
        integrals(0, 2)[4] = integrals(2, 0)[4] = 12.0;
        integrals(1, 1)[4] = 6.0;

        //UpdateControlPointByEnergyOptimization(integrals);

        _integrals = integrals;
    }

    CubicBezierTriangle3::CubicBezierTriangle3(TriangularMatrix<DCoordinate3> border_net):
        TenPointsBasedTriangle(border_net)
    {
        //that's how I will represent the used calculated integrals
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        //the calculated integrals, (x, y)[z]:   x -> x times partial derivate by the first parameter, y ->  y times partial derivate by the second parameter, z -> the z-th point's partial derivative
        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(1, 0)[7] = integrals(0, 1)[9] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = integrals(1, 0)[0] = -1.0 / 10.0;
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = 1.0 / 10.0;
        integrals(0, 1)[4] = integrals(1, 0)[4] = 1.0 / 5.0;

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(1, 1)[7] = integrals(0, 2)[9] = integrals(1, 1)[3] = integrals(1, 1)[5] = integrals(1, 1)[2] = integrals(2, 0)[0] = -3.0;
        integrals(1, 1)[6] = integrals(2, 0)[8] = integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(2, 0)[3] = integrals(1, 1)[8] = integrals(1, 1)[1] = integrals(2, 0)[1] = 3.0;
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = -6.0;
        integrals(0, 2)[4] = integrals(2, 0)[4] = 12.0;
        integrals(1, 1)[4] = 6.0;

        //UpdateControlPointByEnergyOptimization(integrals);

        _integrals = integrals;
        //another mode to represent the calculated integrals
//        TriangularMatrix<TriangularMatrix<GLdouble>> tau(3);
//        tau(1, 0).ResizeRows(4);
//        tau(1, 1).ResizeRows(4);
//        tau(1, 0)(2, 0) = -1.0 / 10.0;
//        tau(1, 0)(2, 1) = 1.0 / 5.0;
//        tau(1, 0)(2, 2) = -1.0 /10.0;
//        tau(1, 0)(3, 0) = -1.0 / 10.0;
//        tau(1, 0)(3, 1) = 1.0 / 10.0;
//        tau(1, 0)(3, 2) = 1.0 /10.0;
//        tau(1, 0)(3, 3) = -1.0 / 10.0;

//        tau(1, 1)(1, 1) = -1.0 / 10.0;
//        tau(1, 1)(2, 1) = 1.0 / 5.0;
//        tau(1, 1)(3, 1) = -1.0 /10.0;
//        tau(1, 1)(0, 0) = -1.0 / 10.0;
//        tau(1, 1)(1, 0) = 1.0 / 10.0;
//        tau(1, 1)(2, 0) = 1.0 /10.0;
//        tau(1, 1)(3, 0) = -1.0 / 10.0;

//        tau(2, 0).ResizeRows(4);
//        tau(2, 1).ResizeRows(4);
//        tau(2, 2).ResizeRows(4);

//        tau(2,0)(0,0) = 0.0;
//        tau(2,0)(1,0) = 0.0;
//        tau(2,0)(1,1) = 0.0;
//        tau(2,0)(2,0) = -6.0;
//        tau(2,0)(2,1) = 12.0;
//        tau(2,0)(2,2) = -6.0;
//        tau(2,0)(3,0) = -3.0;
//        tau(2,0)(3,1) = 3.0;
//        tau(2,0)(3,2) = 3.0;
//        tau(2,0)(3,3) = -3.0;

//        tau(2,1)(0,0) = 0.0;
//        tau(2,1)(1,0) = 3.0;
//        tau(2,1)(1,1) = -3.0;
//        tau(2,1)(2,0) = -3.0;
//        tau(2,1)(2,1) = 6.0;
//        tau(2,1)(2,2) = -3.0;
//        tau(2,1)(3,0) = 0.0;
//        tau(2,1)(3,1) = -3.0;
//        tau(2,1)(3,2) = 3.0;
//        tau(2,1)(3,3) = 0.0;

//        tau(2,2)(0,0) = -3.0;
//        tau(2,2)(1,0) = 3.0;
//        tau(2,2)(1,1) = -6.0;
//        tau(2,2)(2,0) = 3.0;
//        tau(2,2)(2,1) = 12.0;
//        tau(2,2)(2,2) = 0.0;
//        tau(2,2)(3,0) = -3.0;
//        tau(2,2)(3,1) = -6.0;
//        tau(2,2)(3,2) = 0.0;
//        tau(2,2)(3,3) = 0.0;

//        UpdateControlPointByEnergyOptimization(tau);
    }

    GLboolean CubicBezierTriangle3::BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const{

        values.ResizeRows(4);

        GLdouble u2 = u * u, u3 = u2 * u, v2 = v * v, v3 = v2 * v, w = 1.0 - u - v, w2 = w * w, w3 = w2 * w;

        values(0, 0) = u3;
        values(1, 1) = 3.0 * u2 * w;
        values(1, 0) = 3.0 * u2 * v;
        values(2, 2) = 3.0 * u * w2;
        values(2, 1) = 6.0 * u * v * w;
        values(2, 0) = 3.0 * u * v2;
        values(3, 3) = w3;
        values(3, 2) = 3.0 * v * w2;
        values(3, 1) = 3.0 * v2 * w;
        values(3, 0) = v3;

        return GL_TRUE;
    }

    GLboolean CubicBezierTriangle3::CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives &pd) const{

        if (!pd.ResizeRows(3))
       {
           return GL_FALSE;
       }

       TriangularMatrix<GLdouble> B(4), duB(4), dvB(4), duuB(4), dvvB(4), duvB(4), dvuB(4);
       GLdouble u2 = u * u, u3 = u2 * u, v2 = v * v, v3 = v2 * v, w = 1.0 - u - v, w2 = w * w, w3 = w2 * w;

//       B(0, 0) = u3;
//       B(1, 0) = 3.0 * u2 * w;
//       B(1, 1) = 3.0 * u2 * v;
//       B(2, 0) = 3.0 * u * w2;
//       B(2, 1) = 6.0 * u * v * w;
//       B(2, 2) = 3.0 * u * v2;
//       B(3, 0) = w3;
//       B(3, 1) = 3.0 * v * w2;
//       B(3, 2) = 3.0 * v2 * w;
//       B(3, 3) = v3;

//       duB(0, 0) = 3.0 * u2;
//       duB(1, 0) = 6.0 * u * w - 3.0 * u2;
//       duB(1, 1) = 6.0 * u * v;
//       duB(2, 0) = 3.0 * w2 - 6.0 * u * w;
//       duB(2, 1) = 6.0 * v * w - 6.0 * u * v;
//       duB(2, 2) = 3.0 * v2;
//       duB(3, 0) = -3.0 * w2;
//       duB(3, 1) = -6.0 * v * w;
//       duB(3, 2) = -3.0 * v2;
//       duB(3, 3) = 0.0;

//       duuB(0, 0) = 6.0 * u;
//       duuB(1, 0) = 6.0 * w - 12.0 * u;
//       duuB(1, 1) = 6.0 * v;
//       duuB(2, 0) = -12.0 * w + 6.0 * u;
//       duuB(2, 1) = -12.0 * v;
//       duuB(2, 2) = 0.0;
//       duuB(3, 0) = 6.0 * w;
//       duuB(3, 1) = 6.0 * v;
//       duuB(3, 2) = 0.0;
//       duuB(3, 3) = 0.0;

//       duvB(0, 0) = 0.0;
//       duvB(1, 0) = -6.0;
//       duvB(1, 1) = 6.0;
//       duvB(2, 0) = 12.0;
//       duvB(2, 1) = -12.0;
//       duvB(2, 2) = 0.0;
//       duvB(3, 0) = -6.0;
//       duvB(3, 1) = 6.0;
//       duvB(3, 2) = 0.0;
//       duvB(3, 3) = 0.0;

//       dvB(0, 0) = 0;
//       dvB(1, 0) = -3.0 * u2;
//       dvB(1, 1) = 3.0 * u2;
//       dvB(2, 0) = -6.0 * u * w;
//       dvB(2, 1) = 6.0 * u * w - 6.0 * u * v;
//       dvB(2, 2) = 6.0 * u * v;
//       dvB(3, 0) = -3.0 * w2;
//       dvB(3, 1) = 3.0 * w2 - 6.0 * v * w;
//       dvB(3, 2) = 6.0 * v * w - 3.0 * v2;
//       dvB(3, 3) = 3.0 * v2;

//       dvvB(0, 0) = 0;
//       dvvB(1, 0) = 0.0;
//       dvvB(1, 1) = 0.0;
//       dvvB(2, 0) = 6.0 * u;
//       dvvB(2, 1) = -12.0 * u;
//       dvvB(2, 2) = 6.0 * u;
//       dvvB(3, 0) = 6.0 * w;
//       dvvB(3, 1) = -12.0 * w + 6.0 * v;
//       dvvB(3, 2) = 6.0 * w - 12.0 * v;
//       dvvB(3, 3) = 6.0 * v;

//       dvuB(0, 0) = 0;
//       dvuB(1, 0) = 0.0;
//       dvuB(1, 1) = 0.0;
//       dvuB(2, 0) = 6.0;
//       dvuB(2, 1) = -12.0;
//       dvuB(2, 2) = 6.0;
//       dvuB(3, 0) = -6.0;
//       dvuB(3, 1) = 12.0;
//       dvuB(3, 2) = -6.0;
//       dvuB(3, 3) = 0.0;

       B(0, 0) = u3;
       B(1, 1) = 3.0 * u2 * w;
       B(1, 0) = 3.0 * u2 * v;
       B(2, 2) = 3.0 * u * w2;
       B(2, 1) = 6.0 * u * v * w;
       B(2, 0) = 3.0 * u * v2;
       B(3, 3) = w3;
       B(3, 2) = 3.0 * v * w2;
       B(3, 1) = 3.0 * v2 * w;
       B(3, 0) = v3;

       duB(0, 0) = 3.0 * u2;
       duB(1, 1) = 6.0 * u * w - 3.0 * u2;
       duB(1, 0) = 6.0 * u * v;
       duB(2, 2) = 3.0 * w2 - 6.0 * u * w;
       duB(2, 1) = 6.0 * v * w - 6.0 * u * v;
       duB(2, 0) = 3.0 * v2;
       duB(3, 3) = -3.0 * w2;
       duB(3, 2) = -6.0 * v * w;
       duB(3, 1) = -3.0 * v2;
       duB(3, 0) = 0.0;

       dvB(0, 0) = 0;
       dvB(1, 1) = -3.0 * u2;
       dvB(1, 0) = 3.0 * u2;
       dvB(2, 2) = -6.0 * u * w;
       dvB(2, 1) = 6.0 * u * w - 6.0 * u * v;
       dvB(2, 0) = 6.0 * u * v;
       dvB(3, 3) = -3.0 * w2;
       dvB(3, 2) = 3.0 * w2 - 6.0 * v * w;
       dvB(3, 1) = 6.0 * v * w - 3.0 * v2;
       dvB(3, 0) = 3.0 * v2;

       duuB(0, 0) = 6.0 * u;
       duuB(1, 1) = 6.0 * w - 12.0 * u;
       duuB(1, 0) = 6.0 * v;
       duuB(2, 2) = -12.0 * w + 6.0 * u;
       duuB(2, 1) = -12.0 * v;
       duuB(2, 0) = 0.0;
       duuB(3, 3) = 6.0 * w;
       duuB(3, 2) = 6.0 * v;
       duuB(3, 1) = 0.0;
       duuB(3, 0) = 0.0;

       duvB(0, 0) = 0.0;
       duvB(1, 1) = -6.0;
       duvB(1, 0) = 6.0;
       duvB(2, 2) = 12.0;
       duvB(2, 1) = -12.0;
       duvB(2, 0) = 0.0;
       duvB(3, 3) = -6.0;
       duvB(3, 2) = 6.0;
       duvB(3, 1) = 0.0;
       duvB(3, 0) = 0.0;

       dvvB(0, 0) = 0;
       dvvB(1, 1) = 0.0;
       dvvB(1, 0) = 0.0;
       dvvB(2, 0) = 6.0 * u;
       dvvB(2, 1) = -12.0 * u;
       dvvB(2, 2) = 6.0 * u;
       dvvB(3, 3) = 6.0 * w;
       dvvB(3, 2) = -12.0 * w + 6.0 * v;
       dvvB(3, 1) = 6.0 * w - 12.0 * v;
       dvvB(3, 0) = 6.0 * v;

       dvuB(0, 0) = 0;
       dvuB(1, 1) = 0.0;
       dvuB(1, 0) = 0.0;
       dvuB(2, 0) = 6.0;
       dvuB(2, 1) = -12.0;
       dvuB(2, 2) = 6.0;
       dvuB(3, 3) = -6.0;
       dvuB(3, 2) = 12.0;
       dvuB(3, 1) = -6.0;
       dvuB(3, 0) = 0.0;

       pd.LoadNullVectors();

       DCoordinate3 &point = pd(0, 0), &diff1u = pd(1, 0), &diff1v = pd(1, 1), &diffluu = pd(2, 0), &diffluv = pd(2, 1), &difflvv = pd(2, 2);

       for (GLuint k = 0; k < 4; ++k)
       {
           for (GLuint l = 0; l <= k; ++l)
           {
               point  += _net(k, l) *   B(k, l);
               diff1u += _net(k, l) * duB(k, l);
               diff1v += _net(k, l) * dvB(k, l);
               diffluu += _net(k, l) * duuB(k, l);
               diffluv += _net(k, l) * duvB(k, l);
               difflvv += _net(k, l) * dvvB(k, l);
           }
       }

       return GL_TRUE;
    }
}
