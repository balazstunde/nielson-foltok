#pragma once

#include "../TenPointsBasedTriangles/TenPointsBasedTriangle.h"
#include "../Core/Matrices.h"

namespace cagd {
    class AlgebraicTrigonometricTriangle3: public TenPointsBasedTriangle{

    public:
        AlgebraicTrigonometricTriangle3(GLdouble beta = 1.0);
        //special contructor
        AlgebraicTrigonometricTriangle3(TriangularMatrix<DCoordinate3> border_net, GLdouble beta);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const;
        GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const;
    };
}
