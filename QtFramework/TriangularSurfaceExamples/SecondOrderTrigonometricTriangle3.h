#pragma once
#include "../TenPointsBasedTriangles/TenPointsBasedTriangle.h"
#include "../Core/Matrices.h"

namespace cagd {
    class SecondOrderTrigonometricTriangle3: public TenPointsBasedTriangle{

    private:
        GLdouble _c1;
        GLdouble _c2;
        GLdouble _c3;

    public:
        //default/special constructor
        SecondOrderTrigonometricTriangle3(GLdouble beta = 1.0);
        //special contructor
        SecondOrderTrigonometricTriangle3(TriangularMatrix<DCoordinate3> border_net, GLdouble beta);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const;
        GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const;
    };
}
