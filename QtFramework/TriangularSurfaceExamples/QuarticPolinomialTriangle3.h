#pragma once
#include "../TenPointsBasedTriangles/TenPointsBasedTriangle.h"
#include "../Core/Matrices.h"

namespace cagd {
    class QuarticPolinomialTriangle3: public TenPointsBasedTriangle{

    public:
        //default constructor
        QuarticPolinomialTriangle3();
        //special contructor
        QuarticPolinomialTriangle3(TriangularMatrix<DCoordinate3> border_net);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const;
        GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const;
    };
}
