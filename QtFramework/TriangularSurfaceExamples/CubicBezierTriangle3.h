#pragma once
#include "../Core/Matrices.h"
#include "../TenPointsBasedTriangles/TenPointsBasedTriangle.h"

namespace cagd {
    class CubicBezierTriangle3: public TenPointsBasedTriangle{

    public:
        //default contructor
        CubicBezierTriangle3();

        // special constructor
        CubicBezierTriangle3(TriangularMatrix<DCoordinate3> border_net);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const;
        GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const;
    };
}
