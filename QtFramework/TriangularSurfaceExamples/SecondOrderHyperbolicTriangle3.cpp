#include "SecondOrderHyperbolicTriangle3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    SecondOrderHyperbolicTriangle3::SecondOrderHyperbolicTriangle3(GLdouble beta):
        TriangularSurface3 (beta),
        _c1(1.0 / pow(sinh(_beta / 2.0), 4.0)),
        _c2(4.0 * cosh(_beta / 2.0)),
        _c3(2 + 4.0 * pow(cosh(_beta / 2), 2.0))
    {
        //UpdateControlPointsByEnergyOptimization();
    }

    GLboolean SecondOrderHyperbolicTriangle3::BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const{

        values.ResizeRows(4);

        GLdouble w = _beta - u - v;

        GLdouble sb = sinh(_beta / 2.0), sb4 = pow(sb, 4.0), sb5 = sb4 * sb,
                 cb = cosh(_beta / 2.0), cb2 = cb * cb, cb3 = cb2 * cb;

        GLdouble su = sinh(u / 2.0), su2 = su * su, su3 = su2 * su, su4 = su2 * su2,
                 cu = cosh(u / 2.0), cu2 = cu * cu;

        GLdouble sv = sinh(v / 2.0), sv2 = sv * sv, sv3 = sv2 * sv, sv4 = sv2 * sv2,
                 cv = cosh(v / 2.0), cv2 = cv * cv;

        GLdouble sw = sinh(w / 2.0), sw2 = sw * sw, sw3 = sw2 * sw, sw4 = sw2 * sw2,
                 cw = cosh(w / 2.0), cw2 = cw * cw;

        GLdouble r_4_4_0 = 1.0 / sb4,
                 r_4_3_0 = 4.0 * cb / sb4,
                 r_4_2_0 = (2.0 + 4.0 * cb2) / sb4,
                 r_4_3_1 = (4.0 + 8.0 * cb2) / sb5,
                 r_4_2_1 = (16.0 * cb + 8.0 * cb3) / sb5,
                 r_4_2_2 = (10.0 + 20.0 * cb2) / sb4;

        //values

        values(0, 0) = r_4_4_0 * su4;

        values(1, 1) = r_4_3_0 * su3 * sw * cv + 0.5 * r_4_2_0 * su2 * sw2 * cv2;
        values(1, 0) = r_4_3_0 * su3 * sv * cw + 0.5 * r_4_2_0 * su2 * sv2 * cw2;

        values(2, 2) = 0.5 * r_4_2_0 * su2 * sw2 * cv2 + r_4_3_0 * su * sw3 * cv;
        values(2, 1) = r_4_3_1 * (su3 * sw * sv + sv3 * su * sw + sw3 * sv * su) +
                  r_4_2_1 * (su2 * sw2 * cv * sv + su2 * sv2 * cw * sw + sw2 * sv2 * cu * su) +
                  r_4_2_2 * su2 * sv2 * sw2;
        values(2, 0) = 0.5 * r_4_2_0 * su2 * sv2 * cw2 + r_4_3_0 * su * sv3 * cw;

        values(3, 3) = r_4_4_0 * sw4;
        values(3, 2) = r_4_3_0 * sw3 * sv * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        values(3, 1) = r_4_3_0 * sv3 * sw * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        values(3, 0) = r_4_4_0 * sv4;

        return GL_TRUE;
    }

    GLboolean SecondOrderHyperbolicTriangle3::CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives &pd) const{

        if (!pd.ResizeRows(3))
       {
           return GL_FALSE;
       }

        GLdouble w = _beta - u - v;

        GLdouble sb = sinh(_beta / 2.0), sb4 = pow(sb, 4.0), sb5 = sb4 * sb,
                 cb = cosh(_beta / 2.0), cb2 = cb * cb, cb3 = cb2 * cb;

        GLdouble su = sinh(u / 2.0), su2 = su * su, su3 = su2 * su, su4 = su2 * su2,
                 cu = cosh(u / 2.0), cu2 = cu * cu;

        GLdouble sv = sinh(v / 2.0), sv2 = sv * sv, sv3 = sv2 * sv, sv4 = sv2 * sv2,
                 cv = cosh(v / 2.0), cv2 = cv * cv;

        GLdouble sw = sinh(w / 2.0), sw2 = sw * sw, sw3 = sw2 * sw, sw4 = sw2 * sw2,
                 cw = cosh(w / 2.0), cw2 = cw * cw;

        GLdouble r_4_4_0 = 1.0 / sb4,
                 r_4_3_0 = 4.0 * cb / sb4,
                 r_4_2_0 = (2.0 + 4.0 * cb2) / sb4,
                 r_4_3_1 = (4.0 + 8.0 * cb2) / sb5,
                 r_4_2_1 = (16.0 * cb + 8.0 * cb3) / sb5,
                 r_4_2_2 = (10.0 + 20.0 * cb2) / sb4;

        TriangularMatrix<GLdouble> H(4), duH(4), dvH(4), duuT(4), duvT(4);

        //H

        H(0, 0) = r_4_4_0 * su4;

        H(1, 1) = r_4_3_0 * su3 * sw * cv + 0.5 * r_4_2_0 * su2 * sw2 * cv2;
        H(1, 0) = r_4_3_0 * su3 * sv * cw + 0.5 * r_4_2_0 * su2 * sv2 * cw2;

        H(2, 2) = 0.5 * r_4_2_0 * su2 * sw2 * cv2 + r_4_3_0 * su * sw3 * cv;
        H(2, 1) = r_4_3_1 * (su3 * sw * sv + sv3 * su * sw + sw3 * sv * su) +
                  r_4_2_1 * (su2 * sw2 * cv * sv + su2 * sv2 * cw * sw + sw2 * sv2 * cu * su) +
                  r_4_2_2 * su2 * sv2 * sw2;
        H(2, 0) = 0.5 * r_4_2_0 * su2 * sv2 * cw2 + r_4_3_0 * su * sv3 * cw;

        H(3, 3) = r_4_4_0 * sw4;
        H(3, 2) = r_4_3_0 * sw3 * sv * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        H(3, 1) = r_4_3_0 * sv3 * sw * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        H(3, 0) = r_4_4_0 * sv4;


        //duH

        duH(0, 0) = r_4_4_0 * 2.0 * su3 * cu;

        duH(1, 1) = r_4_3_0 * (1.5 * su * cu * sw - 0.5 * su3 * cw) * cv + 0.5 * r_4_2_0 * (su * cu * sw2 - su2 * sw * cw) * cv2;
        duH(1, 0) = r_4_3_0 * (1.5 * su2 * cu * cw - 0.5 * su3 * sw) * sv + 0.5 * r_4_2_0 * (su * cu * cw2 - su2 * cw * sw) * sv2;

        duH(2, 2) = 0.5 * r_4_2_0 * (su * cu * sw2 - su2 * sw * cw) * cv2 + r_4_3_0 * (0.5 * cu * sw3 -1.5 * su * sw2 * cw) * cv;
        duH(2, 1) = r_4_3_1 * ((1.5 * su2 * cu * sw - 0.5 * su3 * cw) * sv + sv3 * (0.5 * cu * sw - 0.5 * su * cw) + (-1.5 * sw2 * cw * su + 0.5 * sw3 * cu) * sv) +
                  r_4_2_1 * ((su * cu * sw2 - su2 * sw * cw) * cv * sv + (su * cu * cw * sw - 0.5 * su2 * sw2 - 0.5 * su2 * cw2) * sv2 + (-sw * cw * cu * su + 0.5 * sw2 * su2 + 0.5 * sw2 * cu2) * sv2) +
                  r_4_2_2 * (su * cu * sw2 - su2 * sw * cw) * sv2;
        duH(2, 0) = 0.5 * r_4_2_0 * (su * cu * cw2 - su2 * cw * sw) * sv2 + r_4_3_0 * (0.5 * cu * cw - 0.5 * su * sw) * sv3;

        duH(3, 3) = r_4_4_0 * -2.0 * sw3 * cw;
        duH(3, 2) = r_4_3_0 * (-1.5 * sw2 * cw * cu + 0.5 * sw3 * su) * sv + 0.5 * r_4_2_0 * (-sw * cw * cu2 + sw2 * cu * su) * sv2;
        duH(3, 1) = r_4_3_0 * sv3 * (-0.5 * cw * cu + 0.5 * sw * su) + 0.5 * r_4_2_0 * (-sw * cw * cu2 + sw2 * cu * su) * sv2;
        duH(3, 0) = 0.0;

        //dvH

        dvH(0, 0) = 0.0;

        dvH(1, 1) = r_4_3_0 * su3 * (-0.5 * cw * cv + 0.5 * sw * sv) + 0.5 * r_4_2_0 * su2 * (-sw * cw * cv2 + sw2 * cv * sv);
        dvH(1, 0) = r_4_3_0 * su3 * (0.5 * cv * cw - 0.5 * sv * sw) + 0.5 * r_4_2_0 * su2 * (sv * cv * cw2 - sv2 * cw * sw);

        dvH(2, 2) = 0.5 * r_4_2_0 * su2 * (-sw * cw * cv2 + sw2 * cv * sv) + r_4_3_0 * su * (-1.5 * sw2 * cw * cv + 0.5 * sw3 * sv);
        dvH(2, 1) = r_4_3_1 * (su3 * (-0.5 * cw * sv + 0.5 * sw * cv) + (1.5 * sv2 * cv * sw - 0.5 * sv3 * cw) * su + (-1.5 * sw2 * cw * sv + 0.5 * sw3 * cv) * su) +
                  r_4_2_1 * (su2 * (-sw * cw * cv * sv + 0.5 * sw2 * sv2 + 0.5 * sw2 * cv2) + su2 * (sv * cv * cw * sw - 0.5 * sv2 * sw2 - 0.5 * sv2 * cw2) + (-sw * cw * sv2 + sw2 * sv * cv) * cu * su) +
                  r_4_2_2 * su2 * (sv * cv * sw2 - sv2 * sw * cw);
        dvH(2, 0) = 0.5 * r_4_2_0 * su2 * (sv * cv * cw2 - sv2 * cw * sw) + r_4_3_0 * su * (1.5 * sv2 * cv * cw - 0.5 * sv3 * sw);

        dvH(3, 3) = r_4_4_0 * -2.0 * sw3 * cw;
        dvH(3, 2) = r_4_3_0 * (-1.5 * sw2 * cw * sv + 0.5 * sw3 * cv) * cu + 0.5 * r_4_2_0 * (-sw * cw * sv2 + sw2 * sv * cv) * cu2;
        dvH(3, 1) = r_4_3_0 * (1.5 * sv2 * cv * sw - 0.5 * sv3 * cw) * cu + 0.5 * r_4_2_0 * (-sw * cw * sv2 + sw2 * sv * cv) * cu2;
        dvH(3, 0) = r_4_4_0 * 2.0 * sv3 * cv;

       pd.LoadNullVectors();

       DCoordinate3 &point = pd(0, 0), &diff1u = pd(1, 0), &diff1v = pd(1, 1);//, &diffluu = pd(2, 0), &diffluv = pd(2, 1), &difflvv = pd(2, 2);

       for (GLuint k = 0; k < 4; ++k)
       {
           for (GLuint l = 0; l <= k; ++l)
           {
               point  += _net(k, l) *   H(k, l);
               diff1u += _net(k, l) * duH(k, l);
               diff1v += _net(k, l) * dvH(k, l);
//               diffluu += _net(k, l) * duuH(k, l);
//               diffluv += _net(k, l) * duvH(k, l);
//               difflvv += _net(k, l) * dvvH(k, l);
           }
       }

       return GL_TRUE;
    }
}
