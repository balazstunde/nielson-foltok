#pragma once
#include "../Core/TriangularSurface3.h"
#include "../Core/Matrices.h"

namespace cagd {
    class SecondOrderHyperbolicTriangle3: public TriangularSurface3{

    private:
        GLdouble _c1;
        GLdouble _c2;
        GLdouble _c3;

    public:
        //default contructor
        SecondOrderHyperbolicTriangle3(GLdouble beta);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const;
        GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const;
    };
}
