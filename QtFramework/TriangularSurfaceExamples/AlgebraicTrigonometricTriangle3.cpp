#include "AlgebraicTrigonometricTriangle3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    AlgebraicTrigonometricTriangle3::AlgebraicTrigonometricTriangle3(GLdouble beta):
        TenPointsBasedTriangle (TriangularMatrix<DCoordinate3>(4), beta)
    {
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        GLdouble sb = sin(_beta), cb = cos(_beta);
        GLdouble sb_2 = sin(_beta / 2.0), cb_2 = cos(_beta / 2.0);
        GLdouble fr = 1.0 / ((2 * sb - _beta - _beta * cb) * (_beta - sb));
        GLdouble c1 = 1.0 / (_beta - sb);
        GLdouble c2 = sb  * fr;
        GLdouble c3 = (4 * (3 * _beta + 4 * sb - _beta * cb) * cb_2) * fr;
        GLdouble c4 = (4 * sb * cb_2) * fr;
        GLdouble b2 = pow(_beta, 2.0), b3 = pow(_beta, 3.0), b4 = pow(_beta, 4.0);
        GLdouble cb_2_2 = pow(cb_2, 2.0), cb_2_3 = pow(cb_2, 3.0);

        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(0, 1)[9] = integrals(1, 0)[0] = (c1 / 48.0) * (-3 * c3 * (_beta * sb_2 + 2 * cb_2 - b2 * cb_2 - 2 * cb_2_3) + c4 * (6 * _beta * sb_2 + b3 * sb_2 + 12 * cb_2 - 9 * b2  * cb_2 - 12 * cb_2_3 + 6 * _beta * sb_2 * cb_2_2));
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = (c2 / 96.0) * (c3 * (_beta * sb_2 * (45 - 4 * b2 - 12 * cb_2_2) + 3 * cb_2 * (14 - 9 * b2 - 14 * cb_2_2)) - c4 * (_beta * sb_2 * (39 + 18 * cb_2_2 - b2) + cb_2 * (78 - 36 * b2 - b4 - 6 * (2 * b2 + 13) * cb_2_2)));
        integrals(1, 0)[7] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = ((_beta * c2 * sb_2) / 16.0) * (c3 * (8 - b2 - 8 * cb_2_2 - 2 * _beta * sb_2 * cb_2) + _beta * c4 * (_beta + 2 * _beta * cb_2_2 - 6 * sb_2 * cb_2));
        integrals(1, 0)[8] = integrals(1, 0)[5] = integrals(0, 1)[1] = integrals(0, 1)[2] = (c2 / 96.0) / (c3 * (_beta * (15 - 2 * b2 - 84 * cb_2_2) * sb_2 + 3 * (58 - 7 * b2 - (58 - 4 * b2) * cb_2_2) * cb_2) + c4 * (_beta * (15 + b2) * sb_2 - 6 * _beta * (25 - 2 * b2) * sb_2 * cb_2_2 + (30 - 12 * b2 - b4 - (30 - 72 * b2) * cb_2_2) * cb_2));
        integrals(1, 0)[9] = integrals(0, 1)[0] = 0.0;
        integrals(0, 1)[4] = integrals(1, 0)[4] = (1.0 / 96.0) * (-3 * pow(c3, 2.0) * (4 - b2 - 4 * cb - _beta * sb) - 6 * c3 * c4 * (2 * b2 - 3 * _beta * sb + b2 * cb) + _beta * pow(c4, 2.0) * (b3 - 3 * b2 * sb - 6 * _beta * cb + 6 * sb));

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(0, 2)[9] = integrals(2, 0)[0] = (-c1 / 48.0) * (3 * c3 * (3 * _beta * sb_2 - 2 * cb_2 - b2 * cb_2  + 2 * cb_2_3) + c4 * (-_beta * (36 + b2) * sb_2 + 3 * (8 + 3 * b2 - 8 * cb_2_2 + 2 * _beta * sb_2 * cb_2) * cb_2));
        integrals(1, 1)[6] = (c1 / 48.0) * (-3 * c3 * (_beta * sb_2 + (2 - b2 - 2 * cb_2_2) * cb_2) + c4 * (_beta * (6 + b2) * sb_2 + 3 * (4 - 3 * b2 - 4 * cb_2_2 + 2 * _beta * sb_2 * cb_2) * cb_2));
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(2, 0)[3] = integrals(2, 0)[1] = (c2 / 96.0) * (c3 * (_beta * (51 - 4 * b2 + 12 * cb_2_2) * sb_2 - 7 * (6 + 3 * b2 - 6 * cb_2_2) * cb_2) - c4 * (3 * _beta * (69 - 5 * b2 + 2 * cb_2_2) * sb_2 - (162 + 78 * b2 + b4 - 6 * (27 + 2 * b2) * cb_2_2) * cb_2));
        integrals(1, 1)[7] = integrals(1, 1)[3] = (c2 / 96.0) * (c3 * (_beta * (15 - 4 * b2) * sb_2 - 3 * (6 - b2 + 2 * (2 * _beta * sb_2 - 3 * cb_2) * cb_2) * cb_2) - c4 * (_beta * (63 - 19 * b2) * sb_2 - (66 - 18 * b2 + b4 + 6 * (7 * _beta * sb_2 - (11 - 2 * b2) * cb_2) * cb_2) * cb_2));
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = (c2 / 16.0) * (c3 * (-b3 * sb_2 - (16 - 4 * b2 - 2 * (2 * _beta * sb_2 + (8 - b2) * cb_2) * cb_2) * cb_2) + c4 * (5 * b3 * sb_2 + 2 * _beta * (-4 * _beta + ((6 - b2) * sb_2 + _beta * cb_2) * cb_2) * cb_2));
        integrals(1, 1)[8] = integrals(1, 1)[1] = (c2 / 48.0) * (3 * c3 * (_beta * sb_2 + (2 - b2 - 2 * cb_2_2) * cb_2) - c4 * (_beta * (6 + b2) * sb_2 + 3 * (4 - 3 * b2 + 2 * (_beta * sb_2 - 2 * cb_2) * cb_2) * cb_2));
        integrals(2, 0)[8] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = (c2 / 96.0) * (-c3 * (_beta * (15 + 2 * b2) * sb_2 - 3 * (38 - b2 - 2 * (2 * _beta * sb_2 + (19 + 2 * b2) * cb_2) * cb_2) * cb_2) + c4 * (_beta * (63 + 11 * b2) * sb_2 - (66 + 6 * b2 + b4 + 6 * (_beta * (19 + 2 * b2) * sb_2 - (11 + 8 * b2) * cb_2) * cb_2) * cb_2));
        integrals(0, 2)[4] = integrals(1, 1)[4] = integrals(2, 0)[4] = (_beta / 96.0) * (3 * pow(c3, 2.0) * (_beta - sb) - 6 * c3 * c4 * (4 * _beta - 3 * sb - _beta * cb) + pow(c4, 2.0) * (48 * _beta + b3 - 30 * sb + 3 * b2 * sb - 18 * _beta * cb));
        integrals(1, 1)[4] /= 2.0;
        integrals(1, 1)[5] = integrals(1, 1)[2] = (c2 / 96.0) * (-c3 * (_beta * (15 + 2 * b2) * sb_2 - (18 + 9 * b2 + 6 * (2 * _beta * sb_2 - (2 * b2 + 3) * cb_2) * cb_2) * cb_2) + c4 * (_beta * (63 + 11 * b2) * sb_2 - (66 + 18 * b2 + b4 + (6 * _beta * (7 + 2 * b2) * sb_2 - 6 * (11 + 4 * b2) * cb_2) * cb_2) * cb_2));
        integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;

        _integrals = integrals;
    }

    AlgebraicTrigonometricTriangle3::AlgebraicTrigonometricTriangle3(TriangularMatrix<DCoordinate3> border_net, GLdouble beta):
        TenPointsBasedTriangle(border_net, beta)
    {
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        GLdouble sb = sin(_beta), cb = cos(_beta);
        GLdouble sb_2 = sin(_beta / 2.0), cb_2 = cos(_beta / 2.0);
        GLdouble fr = (2 * sb - _beta - _beta * cb) * (_beta - sb);
        GLdouble c1 = 1.0 / (_beta - sb);
        GLdouble c2 = sb  / fr;
        GLdouble c3 = (4 * (3 * _beta + 4 * sb - _beta * cb) * cb_2) / fr;
        GLdouble c4 = (4 * sb * cb_2) / fr;
        GLdouble b2 = pow(_beta, 2.0), b3 = pow(_beta, 3.0), b4 = pow(_beta, 4.0);
        GLdouble cb_2_2 = pow(cb_2, 2.0), cb_2_3 = pow(cb_2, 3.0);

        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(0, 1)[9] = integrals(1, 0)[0] = (c1 / 48.0) * (-3 * c3 * (_beta * sb_2 + 2 * cb_2 - b2 * cb_2 - 2 * cb_2_3) + c4 * (6 * _beta * sb_2 + b3 * sb_2 + 12 * cb_2 - 9 * b2  * cb_2 - 12 * cb_2_3 + 6 * _beta * sb_2 * cb_2_2));
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = (c2 / 96.0) * (c3 * (_beta * sb_2 * (45 - 4 * b2 - 12 * cb_2_2) + 3 * cb_2 * (14 - 9 * b2 - 14 * cb_2_2)) - c4 * (_beta * sb_2 * (39 + 18 * cb_2_2 - b2) + cb_2 * (78 - 36 * b2 - b4 - 6 * (2 * b2 + 13) * cb_2_2)));
        integrals(1, 0)[7] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = ((_beta * c2 * sb_2) / 16.0) * (c3 * (8 - b2 - 8 * cb_2_2 - 2 * _beta * sb_2 * cb_2) + _beta * c4 * (_beta + 2 * _beta * cb_2_2 - 6 * sb_2 * cb_2));
        integrals(1, 0)[8] = integrals(1, 0)[5] = integrals(0, 1)[1] = integrals(0, 1)[2] = (c2 / 96.0) / (c3 * (_beta * (15 - 2 * b2 - 84 * cb_2_2) * sb_2 + 3 * (58 - 7 * b2 - (58 - 4 * b2) * cb_2_2) * cb_2) + c4 * (_beta * (15 + b2) * sb_2 - 6 * _beta * (25 - 2 * b2) * sb_2 * cb_2_2 + (30 - 12 * b2 - b4 - (30 - 72 * b2) * cb_2_2) * cb_2));
        integrals(1, 0)[9] = integrals(0, 1)[0] = 0.0;
        integrals(0, 1)[4] = integrals(1, 0)[4] = (1.0 / 96.0) * (-3 * pow(c3, 2.0) * (4 - b2 - 4 * cb - _beta * sb) - 6 * c3 * c4 * (2 * b2 - 3 * _beta * sb + b2 * cb) + _beta * pow(c4, 2.0) * (b3 - 3 * b2 * sb - 6 * _beta * cb + 6 * sb));

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(0, 2)[9] = integrals(2, 0)[0] = (-c1 / 48.0) * (3 * c3 * (3 * _beta * sb_2 - 2 * cb_2 - b2 * cb_2  + 2 * cb_2_3) + c4 * (-_beta * (36 + b2) * sb_2 + 3 * (8 + 3 * b2 - 8 * cb_2_2 + 2 * _beta * sb_2 * cb_2) * cb_2));
        integrals(1, 1)[6] = (c1 / 48.0) * (-3 * c3 * (_beta * sb_2 + (2 - b2 - 2 * cb_2_2) * cb_2) + c4 * (_beta * (6 + b2) * sb_2 + 3 * (4 - 3 * b2 - 4 * cb_2_2 + 2 * _beta * sb_2 * cb_2) * cb_2));
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(2, 0)[3] = integrals(2, 0)[1] = (c2 / 96.0) * (c3 * (_beta * (51 - 4 * b2 + 12 * cb_2_2) * sb_2 - 7 * (6 + 3 * b2 - 6 * cb_2_2) * cb_2) - c4 * (3 * _beta * (69 - 5 * b2 + 2 * cb_2_2) * sb_2 - (162 + 78 * b2 + b4 - 6 * (27 + 2 * b2) * cb_2_2) * cb_2));
        integrals(1, 1)[7] = integrals(1, 1)[3] = (c2 / 96.0) * (c3 * (_beta * (15 - 4 * b2) * sb_2 - 3 * (6 - b2 + 2 * (2 * _beta * sb_2 - 3 * cb_2) * cb_2) * cb_2) - c4 * (_beta * (63 - 19 * b2) * sb_2 - (66 - 18 * b2 + b4 + 6 * (7 * _beta * sb_2 - (11 - 2 * b2) * cb_2) * cb_2) * cb_2));
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = (c2 / 16.0) * (c3 * (-b3 * sb_2 - (16 - 4 * b2 - 2 * (2 * _beta * sb_2 + (8 - b2) * cb_2) * cb_2) * cb_2) + c4 * (5 * b3 * sb_2 + 2 * _beta * (-4 * _beta + ((6 - b2) * sb_2 + _beta * cb_2) * cb_2) * cb_2));
        integrals(1, 1)[8] = integrals(1, 1)[1] = (c2 / 48.0) * (3 * c3 * (_beta * sb_2 + (2 - b2 - 2 * cb_2_2) * cb_2) - c4 * (_beta * (6 + b2) * sb_2 + 3 * (4 - 3 * b2 + 2 * (_beta * sb_2 - 2 * cb_2) * cb_2) * cb_2));
        integrals(2, 0)[8] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = (c2 / 96.0) * (-c3 * (_beta * (15 + 2 * b2) * sb_2 - 3 * (38 - b2 - 2 * (2 * _beta * sb_2 + (19 + 2 * b2) * cb_2) * cb_2) * cb_2) + c4 * (_beta * (63 + 11 * b2) * sb_2 - (66 + 6 * b2 + b4 + 6 * (_beta * (19 + 2 * b2) * sb_2 - (11 + 8 * b2) * cb_2) * cb_2) * cb_2));
        integrals(0, 2)[4] = integrals(1, 1)[4] = integrals(2, 0)[4] = (_beta / 96.0) * (3 * pow(c3, 2.0) * (_beta - sb) - 6 * c3 * c4 * (4 * _beta - 3 * sb - _beta * cb) + pow(c4, 2.0) * (48 * _beta + b3 - 30 * sb + 3 * b2 * sb - 18 * _beta * cb));
        integrals(1, 1)[4] /= 2.0;
        integrals(1, 1)[5] = integrals(1, 1)[2] = (c2 / 96.0) * (-c3 * (_beta * (15 + 2 * b2) * sb_2 - (18 + 9 * b2 + 6 * (2 * _beta * sb_2 - (2 * b2 + 3) * cb_2) * cb_2) * cb_2) + c4 * (_beta * (63 + 11 * b2) * sb_2 - (66 + 18 * b2 + b4 + (6 * _beta * (7 + 2 * b2) * sb_2 - 6 * (11 + 4 * b2) * cb_2) * cb_2) * cb_2));
        integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;

        _integrals = integrals;
    }

    GLboolean AlgebraicTrigonometricTriangle3::BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const{

        values.ResizeRows(4);

        GLdouble w = _beta - u - v;

        GLdouble sb = sin(_beta),
                 cb = cos(_beta);

        GLdouble su = sin(u),
                 cu = cos(u);

        GLdouble sv = sin(v),
                 cv = cos(v);

        GLdouble sw = sin(w),
                 cw = cos(w);

        GLdouble su_2 = sin(u / 2.0),
                 cu_2 = cos(u / 2.0);

        GLdouble sv_2 = sin(v / 2.0),
                 cv_2 = cos(v / 2.0);

        GLdouble sw_2 = sin(w / 2.0),
                 cw_2 = cos(w / 2.0);

        GLdouble sbu = sin(_beta - u), sbv = sin(_beta - v), sbw = sin(_beta - w),
                cbu = cos(_beta - u), cbv = cos(_beta - v), cbw = cos(_beta - w);

        GLdouble c_2_1_0 = 1.0 / ((2 * sb - _beta - _beta * cb) * (_beta - sb));
        GLdouble c_3_0_0 = 1.0 / (_beta - sb);
        GLdouble c_1_1_1_a = (4 * (3 * _beta + 4 * sb - _beta * cb) * cos(_beta / 2.0)) * c_2_1_0;
        GLdouble c_1_1_1_b = (4 * sb * cos(_beta / 2.0)) * c_2_1_0;

        TriangularMatrix<GLdouble> A(4), duA(4), dvA(4), duuA(4), duvA(4), dvvA(4);

        //values

        values(0, 0) = c_3_0_0 * (u - su);

        values(1, 1) = c_2_1_0 * (w + sw + su - sbv + u * cbv - (_beta - v) * cu) * sb;
        values(1, 0) = c_2_1_0 * (v + sv + su - sbw + u * cbw - (_beta - w) * cu) * sb;

        values(2, 2) = c_2_1_0 * (u + su + sw - sbv + w * cbv - (_beta - v) * cw) * sb;
        values(2, 1) = c_1_1_1_a * su_2 * sv_2 * sw_2 - c_1_1_1_b * (u * cu_2 * sv_2 * sw_2 + v * su_2 * cv_2 * sw_2 + w * su_2 * sv_2 * cw_2);
        values(2, 0) = c_2_1_0 * (u + su + sv - sbw + v * cbw - (_beta - w) * cv) * sb;

        values(3, 3) = c_3_0_0 * (w - sw);
        values(3, 2) = c_2_1_0 * (v + sv + sw - sbu + w * cbu - (_beta - u) * cw) * sb;
        values(3, 1) = c_2_1_0 * (w + sw + sv - sbu + v * cbu - (_beta - u) * cv) * sb;
        values(3, 0) = c_3_0_0 * (v - sv);

        return GL_TRUE;
    }

    GLboolean AlgebraicTrigonometricTriangle3::CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives &pd) const{

        if (!pd.ResizeRows(3))
       {
           return GL_FALSE;
       }

        GLdouble w = _beta - u - v;

        GLdouble sb = sin(_beta),
                 cb = cos(_beta);

        GLdouble su = sin(u),
                 cu = cos(u);

        GLdouble sv = sin(v),
                 cv = cos(v);

        GLdouble sw = sin(w),
                 cw = cos(w);

        GLdouble su_2 = sin(u / 2.0),
                 cu_2 = cos(u / 2.0);

        GLdouble sv_2 = sin(v / 2.0),
                 cv_2 = cos(v / 2.0);

        GLdouble sw_2 = sin(w / 2.0),
                 cw_2 = cos(w / 2.0);

        GLdouble sbu = sin(_beta - u), sbv = sin(_beta - v), sbw = sin(_beta - w),
                cbu = cos(_beta - u), cbv = cos(_beta - v), cbw = cos(_beta - w);

        GLdouble c_2_1_0 = 1.0 / ((2 * sb - _beta - _beta * cb) * (_beta - sb));
        GLdouble c_3_0_0 = 1.0 / (_beta - sb);
        GLdouble c_1_1_1_a = (4 * (3 * _beta + 4 * sb - _beta * cb) * cos(_beta / 2.0)) * c_2_1_0;
        GLdouble c_1_1_1_b = (4 * sb * cos(_beta / 2.0)) * c_2_1_0;

        TriangularMatrix<GLdouble> A(4), duA(4), dvA(4), duuA(4), duvA(4), dvvA(4);

        //A

        A(0, 0) = c_3_0_0 * (u - su);

        A(1, 1) = c_2_1_0 * (w + sw + su - sbv + u * cbv - (_beta - v) * cu) * sb;
        A(1, 0) = c_2_1_0 * (v + sv + su - sbw + u * cbw - (_beta - w) * cu) * sb;

        A(2, 2) = c_2_1_0 * (u + su + sw - sbv + w * cbv - (_beta - v) * cw) * sb;
        A(2, 1) = c_1_1_1_a * su_2 * sv_2 * sw_2 - c_1_1_1_b * (u * cu_2 * sv_2 * sw_2 + v * su_2 * cv_2 * sw_2 + w * su_2 * sv_2 * cw_2);
        A(2, 0) = c_2_1_0 * (u + su + sv - sbw + v * cbw - (_beta - w) * cv) * sb;

        A(3, 3) = c_3_0_0 * (w - sw);
        A(3, 2) = c_2_1_0 * (v + sv + sw - sbu + w * cbu - (_beta - u) * cw) * sb;
        A(3, 1) = c_2_1_0 * (w + sw + sv - sbu + v * cbu - (_beta - u) * cv) * sb;
        A(3, 0) = c_3_0_0 * (v - sv);

        //duA

        duA(0, 0) = c_3_0_0 * (1 - cu);

        duA(1, 1) = c_2_1_0 * (-1 - cw + cu + cbv + (_beta - v) * su) * sb;
        duA(1, 0) = c_2_1_0 * (cu - cbw + (cbw - u * sbw) + _beta * su + (-cu - w * su)) * sb;

        duA(2, 2) = c_2_1_0 * (1 + cu - cw - cbv - (_beta - v) * sw) * sb;
        duA(2, 1) = c_1_1_1_a * sv_2 * (0.5 * cu_2 * sw_2 - 0.5 * su_2 * cw_2) - c_1_1_1_b * ((cu_2 * sw_2 - 0.5 * u * su_2 * sw_2 - 0.5 * u * cu_2 * cw_2) * sv_2 + v * cv_2 * (0.5 * cu_2 * sw_2 - 0.5 * su_2 * cw_2) + (-su_2 * cw_2 + 0.5 * w * cu_2 * cw_2 + 0.5 * w * su_2 * sw_2) * sv_2);
        duA(2, 0) = c_2_1_0 * (1 + cu - cbw - v * sbw - cv) * sb;

        duA(3, 3) = c_3_0_0 * (-1 + cw);
        duA(3, 2) = c_2_1_0 * (-cw + cbu + (-cbu + w * sbu) - _beta * sw + (cw + u * sw)) * sb;
        duA(3, 1) = c_2_1_0 * (-1 - cw + cbu + v * sbu + cv) * sb;
        duA(3, 0) = 0.0;

        //duuA

        duA(0, 0) = c_3_0_0 * (su);

        duA(1, 1) = c_2_1_0 * (-sw - su + (_beta - v) * cu) * sb;
        duA(1, 0) = c_2_1_0 * (-su + sbw + (-sbw - (sbw + u * cbw)) + _beta * cu + (su - (-su + w * cu))) * sb;

        duA(2, 2) = c_2_1_0 * (1 - su - sw + (_beta - v) * cw) * sb;
        duA(2, 1) = c_1_1_1_a * sv_2 * (0.5 * (-0.5 * su_2 * sw_2 - 0.5 * cu_2 * cw_2) - 0.5 * (0.5 * cu_2 * cw_2 + 0.5 * su_2 * sw_2)) - c_1_1_1_b * (((-0.5 * su_2 * sw_2 - 0.5 * cu_2 * cw_2) - 0.5 * (su_2 * sw_2 + 0.5 * u * cu_2 * sw_2 - 0.5 * u * su_2 * cw_2) - 0.5 * (cu_2 * cw_2 - 0.5 * u * su_2 * cw_2 + 0.5 * u * cu_2 * sw_2)) * sv_2 + v * cv_2 * (0.5 * (-0.5 * su_2 * sw_2 - 0.5 * cu_2 * cw_2) - 0.5 * (0.5 * cu_2 * cw_2 + 0.5 * su_2 * sw_2)) + (-(0.5 * cu_2 * cw_2 + 0.5 * su_2 * sw_2) + 0.5 * (-cu_2 * cw_2 - 0.5 * w * su_2 * cw_2 + 0.5 * w * cu_2 * sw_2) + 0.5 *(-su_2 * sw_2 + 0.5 * w * cu_2 * sw_2 - 0.5 * w * su_2 * cw_2)) * sv_2);
        duA(2, 0) = c_2_1_0 * (1 - su + sbw - v * cbw) * sb;

        duA(3, 3) = c_3_0_0 * (sw);
        duA(3, 2) = c_2_1_0 * (-sw + sbu + (-sbu +(-sbu - w * cbu)) + _beta * cw + (sw + (sw - u * cw))) * sb;
        duA(3, 1) = c_2_1_0 * (sw + sbu - v * cbu) * sb;
        duA(3, 0) = 0.0;

        //dvA

        dvA(0, 0) = 0.0;

        dvA(1, 1) = c_2_1_0 * (-1 - cw + cbv + u * sbv + cu) * sb;
        dvA(1, 0) = c_2_1_0 * (1 + cv - cbw - u * sbw - cu) * sb;

        dvA(2, 2) = c_2_1_0 * (-cw + cbv + (-cbv + w * sbv) - _beta * sw + (cw + v * sw)) * sb;
        dvA(2, 1) = c_1_1_1_a * su_2 * (0.5 * cv_2 * sw_2 - 0.5 * sv_2 * cw_2) - c_1_1_1_b * (u * cu_2 * (0.5 * cv_2 * sw_2 - 0.5 * sv_2 * cw_2) + su_2 * (cv_2 * sw_2 - 0.5 * v * sv_2 * sw_2 - 0.5 * v * cv_2 * cw_2) + (-sv_2 * cw_2 + 0.5 * w * cv_2 * cw_2 + 0.5 * w * sv_2 * sw_2) * su_2);
        dvA(2, 0) = c_2_1_0 * (cv - cbw + (cbw - v * sbw) + _beta * sv +(-cv - w * sv)) * sb;

        dvA(3, 3) = c_3_0_0 * (-1 + cw);
        dvA(3, 2) = c_2_1_0 * (1 + cv - cw - cbu - (_beta - u) * sw) * sb;
        dvA(3, 1) = c_2_1_0 * (-1 - cw + cv + cbu + (_beta - u) * sv) * sb;
        dvA(3, 0) = c_3_0_0 * (1 - cv);

        //dvvA

        dvA(0, 0) = 0.0;

        dvA(1, 1) = c_2_1_0 * (-1 - sw + sbv - u * cbv) * sb;
        dvA(1, 0) = c_2_1_0 * (1 - sv - sbw + u * cbw) * sb;

        dvA(2, 2) = c_2_1_0 * (-sw + sbv + (-sbv + (-sbv - w * cbv)) + _beta * cw + (sw + (sw - v * cw))) * sb;
        dvA(2, 1) = c_1_1_1_a * su_2 * (0.5 * (-0.5 * sv_2 * sw_2 - 0.5 * cv_2 * cw_2) - 0.5 * (0.5 * cv_2 * cw_2 + 0.5 * sv_2 * sw_2)) - c_1_1_1_b * (u * cu_2 * (0.5 * (-0.5 * sv_2 * sw_2 - 0.5 * cv_2 * cw_2) - 0.5 * (0.5 * cv_2 * cw_2 + 0.5 * sv_2 * sw_2)) + su_2 * ((-0.5 * sv_2 * sw_2 - 0.5 * cv_2 * cw_2) - 0.5 * (sv_2 * sw_2 + 0.5 * v * cv_2 * sw_2 - 0.5 * v * sv_2 * cw_2) - 0.5 * (cv_2 * cw_2 - 0.5 * v * sv_2 * cw_2 + 0.5 * v * cv_2 * sw_2)) + (-(0.5 * cv_2 * cw_2 + 0.5 * sv_2 * sw_2) + 0.5 * (-cv_2 * cw_2 - 0.5 * w * sv_2 * cw_2 + 0.5 * w * cv_2 * sw_2) + 0.5 * (-sv_2 * sw_2 + 0.5 * w * cv_2 * sw_2 - 0.5 * w * sv_2 * cw_2)) * su_2);
        dvA(2, 0) = c_2_1_0 * (-sv + sbw + (-sbw - (sbw + v * cbw)) + _beta * cv +(sv - (-sv + w * cv))) * sb;

        dvA(3, 3) = c_3_0_0 * (sw);
        dvA(3, 2) = c_2_1_0 * (-sv - sw + (_beta - u) * cw) * sb;
        dvA(3, 1) = c_2_1_0 * (sw - sv + sbu + (_beta - u) * cv) * sb;
        dvA(3, 0) = c_3_0_0 * (sv);

       pd.LoadNullVectors();

       DCoordinate3 &point = pd(0, 0), &diff1u = pd(1, 0), &diff1v = pd(1, 1), &diffluu = pd(2, 0), &diffluv = pd(2, 1), &difflvv = pd(2, 2);

       for (GLuint k = 0; k < 4; ++k)
       {
           for (GLuint l = 0; l <= k; ++l)
           {
               point  += _net(k, l) * A(k, l);
               diff1u += _net(k, l) * duA(k, l);
               diff1v += _net(k, l) * dvA(k, l);
               diffluu += _net(k, l) * duuA(k, l);
               diffluv += _net(k, l) * duvA(k, l);
               difflvv += _net(k, l) * dvvA(k, l);
           }
       }

       return GL_TRUE;
    }
}
