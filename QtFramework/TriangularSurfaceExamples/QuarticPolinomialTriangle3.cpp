#include "QuarticPolinomialTriangle3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    QuarticPolinomialTriangle3::QuarticPolinomialTriangle3():
        TenPointsBasedTriangle (TriangularMatrix<DCoordinate3>(4))
    {
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(0, 1)[9] = integrals(1, 0)[0] = -6.0 / 35.0;
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = 6.0 / 35.0;
        integrals(1, 0)[7] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = -11.0 / 35.0;
        integrals(1, 0)[8] = integrals(1, 0)[5] = integrals(0, 1)[1] = integrals(0, 1)[2] = -3.0 / 35.0;
        integrals(1, 0)[9] = integrals(0, 1)[0] = 0.0;
        integrals(0, 1)[4] = integrals(1, 0)[4] = 4.0 / 5.0;

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(0, 2)[9] = integrals(2, 0)[0] = -24.0 / 5.0;
        integrals(1, 1)[6] = 12.0 / 5.0;
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(1, 1)[8] = integrals(2, 0)[3] = integrals(1, 1)[1] = integrals(2, 0)[1] = 24.0 / 5.0;
        integrals(1, 1)[7] = integrals(2, 0)[8] = integrals(1, 1)[3] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = -36.0 / 5.0;
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = -84.0 / 5.0;
        integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;
        integrals(0, 2)[4] = integrals(1, 1)[4] = integrals(2, 0)[4] = 48.0;
        integrals(1, 1)[4] /= 2.0;
        integrals(1, 1)[5] = integrals(1, 1)[2] = -54.0 / 5.0;

        _integrals = integrals;
    }

    QuarticPolinomialTriangle3::QuarticPolinomialTriangle3(TriangularMatrix<DCoordinate3> border_net):
       TenPointsBasedTriangle(border_net)
    {
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(0, 1)[9] = integrals(1, 0)[0] = -6.0 / 35.0;
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = 6.0 / 35.0;
        integrals(1, 0)[7] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = -11.0 / 35.0;
        integrals(1, 0)[8] = integrals(1, 0)[5] = integrals(0, 1)[1] = integrals(0, 1)[2] = -3.0 / 35.0;
        integrals(1, 0)[9] = integrals(0, 1)[0] = 0.0;
        integrals(0, 1)[4] = integrals(1, 0)[4] = 4.0 / 5.0;

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(0, 2)[9] = integrals(2, 0)[0] = -24.0 / 5.0;
        integrals(1, 1)[6] = 12.0 / 5.0;
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(1, 1)[8] = integrals(2, 0)[3] = integrals(1, 1)[1] = integrals(2, 0)[1] = 24.0 / 5.0;
        integrals(1, 1)[7] = integrals(2, 0)[8] = integrals(1, 1)[3] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = -36.0 / 5.0;
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = -84.0 / 5.0;
        integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;
        integrals(0, 2)[4] = integrals(1, 1)[4] = integrals(2, 0)[4] = 48.0;
        integrals(1, 1)[4] /= 2.0;
        integrals(1, 1)[5] = integrals(1, 1)[2] = -54.0 / 5.0;

        _integrals = integrals;

        //another mode to represent the calculated integrals
//        TriangularMatrix<TriangularMatrix<GLdouble>> tau(3);
//        tau(1, 0).ResizeRows(4);
//        tau(1, 1).ResizeRows(4);
//        tau(1, 0)(0, 0) = 0;
//        tau(1, 0)(1, 0) = -3.0 / 35.0;
//        tau(1, 0)(1, 1) = -3.0 / 35.0;
//        tau(1, 0)(2, 0) = -11.0 / 35.0;
//        tau(1, 0)(2, 1) = 4.0 / 5.0;
//        tau(1, 0)(2, 2) = -11.0 / 35.0;
//        tau(1, 0)(3, 0) = -6.0 / 35.0;
//        tau(1, 0)(3, 1) = 6.0 / 35.0;
//        tau(1, 0)(3, 2) = 6.0 /35.0;
//        tau(1, 0)(3, 3) = -6.0 / 35.0;

//        tau(1, 1)(0, 0) = -6.0 / 35.0;
//        tau(1, 1)(1, 0) = 6.0 / 35.0;
//        tau(1, 1)(1, 1) = -11.0 / 35.0;
//        tau(1, 1)(2, 0) = 6.0 / 35.0;
//        tau(1, 1)(2, 1) = 4.0 / 5.0;
//        tau(1, 1)(2, 2) = -3.0 / 35.0;
//        tau(1, 1)(3, 0) = -6.0 / 35.0;
//        tau(1, 1)(3, 1) = -11.0 / 35.0;
//        tau(1, 1)(3, 2) = -3.0 / 35.0;
//        tau(1, 1)(3, 3) = 0.0;

//        tau(2, 0).ResizeRows(4);
//        tau(2, 1).ResizeRows(4);
//        tau(2, 2).ResizeRows(4);

//        tau(2,0)(0,0) = 0.0;
//        tau(2,0)(1,0) = -36.0 / 5.0;
//        tau(2,0)(1,1) = -36.0 / 5.0;
//        tau(2,0)(2,0) = -84.0 / 5.0;
//        tau(2,0)(2,1) = 48.0;
//        tau(2,0)(2,2) = -84.0 / 5.0;
//        tau(2,0)(3,0) = -24.0 / 5.0;
//        tau(2,0)(3,1) = 24.0 / 5.0;
//        tau(2,0)(3,2) = 24.0 / 5.0;
//        tau(2,0)(3,3) = -24.0 / 5.0;

//        tau(2,1)(0,0) = 0.0;
//        tau(2,1)(1,0) = 24.0 / 5.0;
//        tau(2,1)(1,1) = -54.0 / 5.0;
//        tau(2,1)(2,0) = -36.0 / 5.0;
//        tau(2,1)(2,1) = 48.0;
//        tau(2,1)(2,2) = -54.0 / 5.0;
//        tau(2,1)(3,0) = 12.0 / 5.0;
//        tau(2,1)(3,1) = -36.0 / 5.0;
//        tau(2,1)(3,2) = 24.0 / 5.0;
//        tau(2,1)(3,3) = 0.0;

//        tau(2,2)(0,0) = -24.0 / 5.0;
//        tau(2,2)(1,0) = 24.0 / 5.0;
//        tau(2,2)(1,1) = -84.0 / 5.0;
//        tau(2,2)(2,0) = 24.0 / 5.0;
//        tau(2,2)(2,1) = 48.0;
//        tau(2,2)(2,2) = -36.0 / 5.0;
//        tau(2,2)(3,0) = -24.0 / 5.0;
//        tau(2,2)(3,1) = -84.0 / 5.0;
//        tau(2,2)(3,2) = -36.0 / 5.0;
//        tau(2,2)(3,3) = 0.0;

//        UpdateControlPointByEnergyOptimization(tau);
    }

    GLboolean QuarticPolinomialTriangle3::BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const{

        values.ResizeRows(4);

        GLdouble u2 = u * u, u3 = u2 * u, u4 = u3 * u, v2 = v * v, v3 = v2 * v, v4 = v3 * v, w = 1.0 - u - v, w2 = w * w, w3 = w2 * w, w4 = w3 * w;

        values(0, 0) = u4;
        values(1, 1) = 4.0 * u3 * w + 3.0 * u2 * w2;
        values(1, 0) = 4.0 * u3 * v + 3.0 * u2 * v2;
        values(2, 2) = 4.0 * u * w3 + 3.0 * u2 * w2;
        values(2, 1) = 6.0 * u * v * w;
        values(2, 0) = 4.0 * u * v3 + 3.0 * u2 * v2;
        values(3, 3) = w4;
        values(3, 2) = 4.0 * v * w3 + 3.0 * v2 * w2;
        values(3, 1) = 4.0 * v3 * w + 3.0 * v2 * w2;
        values(3, 0) = v4;

        return GL_TRUE;
    }

    GLboolean QuarticPolinomialTriangle3::CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives &pd) const{

        if (!pd.ResizeRows(3))
       {
           return GL_FALSE;
       }

       TriangularMatrix<GLdouble> Q(4), duQ(4), dvQ(4), duuQ(4), dvvQ(4), duvQ(4), dvuQ(4);
       GLdouble u2 = u * u, u3 = u2 * u, u4 = u3 * u, v2 = v * v, v3 = v2 * v, v4 = v3 * v, w = 1.0 - u - v, w2 = w * w, w3 = w2 * w, w4 = w3 * w;

       Q(0, 0) = u4;

       Q(1, 1) = 4.0 * u3 * w + 3.0 * u2 * w2;
       Q(1, 0) = 4.0 * u3 * v + 3.0 * u2 * v2;

       Q(2, 2) = 4.0 * u * w3 + 3.0 * u2 * w2;
       Q(2, 1) = 12.0 * u2 * v * w + 12.0 * u * v2 * w + 12.0 * u * v * w2;
       Q(2, 0) = 4.0 * u * v3 + 3.0 * u2 * v2;

       Q(3, 3) = w4;
       Q(3, 2) = 4.0 * v * w3 + 3.0 * v2 * w2;
       Q(3, 1) = 4.0 * v3 * w + 3.0 * v2 * w2;
       Q(3, 0) = v4;


       duQ(0, 0) = 4.0 * u3;

       duQ(1, 1) = 6.0 * u2 * w - 4.0 * u3 * w + 6.0 * u * w2;
       duQ(1, 0) = 12.0 * u2 * v + 6.0 * u * v2;

       duQ(2, 2) = 4.0 * w3 - 6.0 * u * w2 - 6.0 * u2 * w;
       duQ(2, 1) = -12.0 * u2 * v + 12.0 * v2 * w - 12.0 * u * v2 + 12.0 * v * w2;
       duQ(2, 0) = 4.0 * v3 + 6.0 * u * v2;

       duQ(3, 3) = -4.0 * w3;
       duQ(3, 2) = -12.0 * v * w2 - 6.0 * v2 * w;
       duQ(3, 1) = -4.0 * v3 - 6.0 * v2 * w;
       duQ(3, 0) = 0.0;


       duuQ(0, 0) = 12.0 * u2;

       duuQ(1, 1) = -6.0 * u2 - 12.0 * u2 * w + 4.0 * u3 + 6.0 * w2;
       duuQ(1, 0) = 24.0 * u * v + 6.0 * v2;

       duuQ(2, 2) = -18.0 * w2 + 6.0 * u2;
       duuQ(2, 1) = -24.0 * u * v - 24.0 * v2 - 24.0 * v * w;
       duuQ(2, 0) = 6.0 * v2;

       duuQ(3, 3) = 12.0 * w2;
       duuQ(3, 2) = 24.0 * v * w + 6.0 * v2;
       duuQ(3, 1) = 6.0 * v2;
       duuQ(3, 0) = 0.0;


       duvQ(0, 0) = 0.0;

       duvQ(1, 1) = -6.0 * u2 + 4.0 * u3 - 12.0 * u * w;
       duvQ(1, 0) = 12.0 * u2 + 12.0 * u * v;

       duvQ(2, 2) = -12.0 * w2 + 12.0 * u * w + 6.0 * u2;
       duvQ(2, 1) = -12.0 * u2 - 12.0 * v2 - 24.0 * u * v + 12.0 * w2;
       duvQ(2, 0) = 12.0 * v2 + 12.0 * u * v;

       duvQ(3, 3) = 12.0 * w2;
       duvQ(3, 2) = -12.0 * w2 + 12.0 * v * w + 6.0 * v2;
       duvQ(3, 1) = -12.0 * v2 - 12.0 * v * w + 6.0 * v2;
       duvQ(3, 0) = 0.0;


       dvQ(0, 0) = 12.0;

       dvQ(1, 1) = -4.0 * u3 - 6.0 * u2 * w;
       dvQ(1, 0) = 4.0 * u3 + 6.0 * u2 * v;

       dvQ(2, 2) = -12.0 * u * w2 - 6.0 * u2 * w;
       dvQ(2, 1) = 12.0 * u2 * w - 12.0 * u2 * v - 12.0 * u * v2 + 12.0 * u * w2;
       dvQ(2, 0) = 12.0 * u * v2 + 6.0 * u2 * v;

       dvQ(3, 3) = -4.0 * w3;
       dvQ(3, 2) = 4.0 * w3 - 6.0 * v * w2 - 6.0 * v2 * w;
       dvQ(3, 1) = 6.0 * v2 * w - 4.0 * v3 * w + 6.0 * v * w2;
       dvQ(3, 0) = 4.0 * v3;


       dvvQ(0, 0) = 0.0;

       dvvQ(1, 1) = 6.0 * u2;
       dvvQ(1, 0) = 6.0 * u2;

       dvvQ(2, 2) = 24.0 * u * w + 6.0 * u2;
       dvvQ(2, 1) = -24.0 * u2 - 24.0 * u * v - 24.0 * u * w;
       dvvQ(2, 0) = 24.0 * u * v + 6.0 * u2;

       dvvQ(3, 3) = 12.0 * w2;
       dvvQ(3, 2) = -18.0 * w2 + 6.0 * v2;
       dvvQ(3, 1) = -6.0 * v2 - 12.0 * v2 * w + 4.0 * v3 + 6.0 * w2;
       dvvQ(3, 0) = 12.0 * v2;


       dvuQ(0, 0) = 0.0;

       dvuQ(1, 1) = -12.0 * u2 - 12.0 * u * w + 6.0 * u2;
       dvuQ(1, 0) = 12.0 * u2 + 12.0 * u * v;

       dvuQ(2, 2) = -12.0 * w2 + 12.0 * u * w + 6.0 * u2;
       dvuQ(2, 1) = 12.0 * u2 * w - 12.0 * u2 * v - 12.0 * u * v2 + 12.0 * u * w2;
       dvuQ(2, 0) = 12.0 * v2 + 12.0 * u * v;

       dvuQ(3, 3) = 12.0 * w2;
       dvuQ(3, 2) = -12.0 * w2 + 12.0 * v * w + 6.0 * v2;
       dvuQ(3, 1) = -6.0 * v2 + 4.0 * v3 - 12.0 * v * w;
       dvuQ(3, 0) = 0.0;

       pd.LoadNullVectors();

       DCoordinate3 &point = pd(0, 0), &diff1u = pd(1, 0), &diff1v = pd(1, 1), &diffluu = pd(2, 0), &diffluv = pd(2, 1), &difflvv = pd(2, 2);

       for (GLuint k = 0; k < 4; ++k)
       {
           for (GLuint l = 0; l <= k; ++l)
           {
               point  += _net(k, l) *   Q(k, l);
               diff1u += _net(k, l) * duQ(k, l);
               diff1v += _net(k, l) * dvQ(k, l);
               diffluu += _net(k, l) * duuQ(k, l);
               diffluv += _net(k, l) * duvQ(k, l);
               difflvv += _net(k, l) * dvvQ(k, l);
           }
       }

       return GL_TRUE;
    }
}
