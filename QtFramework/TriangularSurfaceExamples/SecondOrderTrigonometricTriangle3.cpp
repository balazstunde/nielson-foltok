#include "SecondOrderTrigonometricTriangle3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    SecondOrderTrigonometricTriangle3::SecondOrderTrigonometricTriangle3(GLdouble beta):
        TenPointsBasedTriangle (TriangularMatrix<DCoordinate3>(4), beta),
        _c1(1.0 / pow(sin(_beta / 2.0), 4.0)),
        _c2(4.0 * cos(_beta / 2.0)),
        _c3(2 + 4.0 * pow(cos(_beta / 2.0), 2.0))
    {
        GLdouble sb_2 = sin(_beta / 2.0), cb_2 = cos(_beta / 2.0);
        GLdouble b2 = pow(_beta, 2.0);
        GLdouble cb_2_2 = pow(cb_2, 2.0), cb_2_4 = pow(cb_2, 4.0), cb_2_6 = pow(cb_2, 6.0), cb_2_8 = pow(cb_2, 8.0);

        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        GLdouble c = pow(sb_2, 8);

        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(0, 1)[9] = integrals(1, 0)[0] = (1.0 / (c * 1152.0)) * (384 - 81 * b2 - 2330 * pow(cb_2, 2.0) + 288 * b2 * pow(cb_2, 2) + 1770 * pow(cb_2, 4) - 180 * b2 * pow(cb_2, 4) + 312 * pow(cb_2, 6) - 136 * pow(cb_2, 8) - 3 * _beta * cb_2 * sb_2 * (37.0 - 286 * pow(cb_2, 2)));
        integrals(1, 0)[7] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = (1.0 / (c * 2304.0)) * (-27 * b2 + 2 * (890 + 18 * b2) * cb_2_2 - 180 * (1 + b2) * cb_2_4 + 48 * (3 * b2 - 31) * cb_2_6 - 112 * cb_2_8 - 12 * _beta * sb_2 * cb_2 * (37 - 6 * cb_2_2 + 130 * cb_2_4 - 20 * cb_2_6));
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = (1.0 / (c * 2304.0))* (640 - 45 * b2 - 12 * (143 + 3 * b2) * cb_2_2 + 36 * (93 - 7 * b2) * cb_2_4 - 16 * (169 -9 * b2) * cb_2_6 + 432 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (1 + 14 * cb_2_2 - 38 * cb_2_4 - 4 * cb_2_6));
        integrals(1, 0)[8] = integrals(1, 0)[5] = integrals(0, 1)[1] = integrals(0, 1)[2] = (1.0 / (c * 2304.0)) * (-27 * b2 + 4 * (113 - 27 * b2) * cb_2_2 + 12 * (13 - 9 * b2) * cb_2_4 - 1200 * cb_2_6 + 592 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (9 + 6 * cb_2_2 - 14 * cb_2_4 + 20 * cb_2_6));
        integrals(0, 1)[4] = integrals(1, 0)[4] = (1.0 / (c * 1152.0)) * (-1408 + 261 * b2 + 4 * (1036 - 117 * b2) * cb_2_2 - 12 * (572 - 75 * b2) * cb_2_4 + 32 * (149 - 9 * b2) * cb_2_6 - 640 * cb_2_8 + 6 * _beta * sb_2 * cb_2 * (91 - 338 * cb_2_2 + 364 * cb_2_4 - 72 * cb_2_6));
        integrals(0, 1)[0] = integrals(1, 0)[9] = 0.0;

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(0, 2)[9] = integrals(2, 0)[0] = (1.0 / (c * 576.0)) * (-336 - 27 * b2 - 4 * (319 - 63 * b2) * cb_2_2 + 12 * (181 - 21 * b2) * cb_2_4 - 864 * cb_2_6 + 304 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (1 + 74 * cb_2_2));
        integrals(1, 1)[6] = (1.0 / (c * 576.0)) * (-144 - 27 * b2 - 4 * (451 - 63 * b2) * cb_2_2 + 84 * (23 - 3 * b2) * cb_2_4 + 288 * cb_2_6 - 272 * cb_2_8 + 12 * _beta * cb_2 * sb_2 * (19 + 62 * cb_2_2));
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(2, 0)[3] = integrals(2, 0)[1] = (1.0 / (c * 1152)) * (224 - 9 * b2 + 6 * (137 - 39 * b2) * cb_2_2 + 6 * (43 + 6 * b2) * cb_2_4 + 8 * (80 + 9 * b2) * cb_2_6 - 1944 * cb_2_8 + 3 * _beta * sb_2 * cb_2 * (131 - 242 * cb_2_2 - 472 * cb_2_4 - 80 * cb_2_6));
        integrals(1, 1)[7] = integrals(1, 1)[3] = (1.0 / (c * 2304.0)) * (-224 - 45 * b2 + 8 * (541 - 45 * b2) * cb_2_2 - 36 * (50 - 11 * b2) * cb_2_4 - 16 * (202 - 9 * b2) * cb_2_6 + 928 * cb_2_8 - 6 * _beta * sb_2 * cb_2 * (193 + 14 * cb_2_2 + 236 * cb_2_4 + 40 * cb_2_6));
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = (1.0 / (c * 1152.0)) * (-384 - 54 * b2 + 2 * (1357 - 99 * b2) * cb_2_2 - 6 * (115 - 48 * b2) * cb_2_4 - 24 * (88 - 3 * b2) * cb_2_6 + 472 * cb_2_8 - 3 * _beta * sb_2 * cb_2 * (341 - 150 * cb_2_2 + 176 * cb_2_4 + 224 * cb_2_6));
        integrals(1, 1)[8] = integrals(1, 1)[1] = (1.0 / (c * 2304.0)) * (-224 - 45 * b2 + 12 * (131 - 15 * b2) * cb_2_2 - 12 * (79 + 15 * b2) * cb_2_4 + 2432 * cb_2_6 - 2832 * cb_2_8 + 24 * _beta * sb_2 * cb_2 * (49 - 53 * cb_2_2 - 43 * cb_2_4 - 10 * cb_2_6));
        integrals(2, 0)[8] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = (1.0 / (c * 576.0)) * (-192 - 27 * b2 + 4 * (143 - 27 * b2) * cb_2_2 + 12 * (115 - 9 * b2) * cb_2_4 - 1296 * cb_2_6 - 464 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (6 - 38 * cb_2_4 - 28 * cb_2_6));
        integrals(0, 2)[4] = integrals(1, 1)[4] = integrals(2, 0)[4] = (1.0 / (c * 576.0)) * (1216 + 171 * b2 - 16 * (133 - 9 * b2) * cb_2_2 - 12 * (556 - 33 * b2) * cb_2_4 + 16 * (362 - 9 * b2) * cb_2_6 + 1792 * cb_2_8 + 6 * _beta * sb_2 * cb_2 * (77 - 250 * cb_2_2 + 476 * cb_2_4 + 264 * cb_2_6));
        integrals(1, 1)[4] /= 2.0;
        integrals(1, 1)[5] = integrals(1, 1)[2] = (1.0 / (c * 2304.0)) * (-480 - 27 * b2 - 4 * (41 + 27 * b2) * cb_2_2 + 12 * (463 - 9 * b2) * cb_2_4 - 5568 * cb_2_6 + 656 * cb_2_8 - 24 * _beta * sb_2 * cb_2 * (39 - 57 * cb_2_2 + 17 * cb_2_4 + 46 * cb_2_6));
        integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;


        _integrals = integrals;
    }

    SecondOrderTrigonometricTriangle3::SecondOrderTrigonometricTriangle3(TriangularMatrix<DCoordinate3> border_net, GLdouble beta):
        TenPointsBasedTriangle(border_net, beta),
        _c1(1.0 / pow(sin(_beta / 2.0), 4.0)),
        _c2(4.0 * cos(_beta / 2.0)),
        _c3(2 + 4.0 * pow(cos(_beta / 2.0), 2.0))
    {
        GLdouble sb_2 = sin(_beta / 2.0), cb_2 = cos(_beta / 2.0);
        GLdouble b2 = pow(_beta, 2.0);
        GLdouble cb_2_2 = pow(cb_2, 2.0), cb_2_4 = pow(cb_2, 4.0), cb_2_6 = pow(cb_2, 6.0), cb_2_8 = pow(cb_2, 8.0);

        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> integrals(3);
        for(GLuint i = 0; i < 3; i++) {
            for(GLuint j = 0; j < 3 - i; j++){
                integrals(i, j).ResizeColumns(10);
                integrals(i, j).FillWithZeros();
            }
        }

        GLdouble c = pow(sb_2, 8);

        integrals(0, 1)[6] = integrals(1, 0)[6] = integrals(0, 1)[9] = integrals(1, 0)[0] = (1.0 / (c * 1152.0)) * (384 - 81 * b2 - 2330 * pow(cb_2, 2.0) + 288 * b2 * pow(cb_2, 2) + 1770 * pow(cb_2, 4) - 180 * b2 * pow(cb_2, 4) + 312 * pow(cb_2, 6) - 136 * pow(cb_2, 8) - 3 * _beta * cb_2 * sb_2 * (37.0 - 286 * pow(cb_2, 2)));
        integrals(1, 0)[7] = integrals(0, 1)[3] = integrals(0, 1)[5] = integrals(1, 0)[2] = (1.0 / (c * 2304.0)) * (-27 * b2 + 2 * (890 + 18 * b2) * cb_2_2 - 180 * (1 + b2) * cb_2_4 + 48 * (3 * b2 - 31) * cb_2_6 - 112 * cb_2_8 - 12 * _beta * sb_2 * cb_2 * (37 - 6 * cb_2_2 + 130 * cb_2_4 - 20 * cb_2_6));
        integrals(0, 1)[7] = integrals(0, 1)[8] = integrals(1, 0)[3] = integrals(1, 0)[1] = (1.0 / (c * 2304.0))* (640 - 45 * b2 - 12 * (143 + 3 * b2) * cb_2_2 + 36 * (93 - 7 * b2) * cb_2_4 - 16 * (169 -9 * b2) * cb_2_6 + 432 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (1 + 14 * cb_2_2 - 38 * cb_2_4 - 4 * cb_2_6));
        integrals(1, 0)[8] = integrals(1, 0)[5] = integrals(0, 1)[1] = integrals(0, 1)[2] = (1.0 / (c * 2304.0)) * (-27 * b2 + 4 * (113 - 27 * b2) * cb_2_2 + 12 * (13 - 9 * b2) * cb_2_4 - 1200 * cb_2_6 + 592 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (9 + 6 * cb_2_2 - 14 * cb_2_4 + 20 * cb_2_6));
        integrals(0, 1)[4] = integrals(1, 0)[4] = (1.0 / (c * 1152.0)) * (-1408 + 261 * b2 + 4 * (1036 - 117 * b2) * cb_2_2 - 12 * (572 - 75 * b2) * cb_2_4 + 32 * (149 - 9 * b2) * cb_2_6 - 640 * cb_2_8 + 6 * _beta * sb_2 * cb_2 * (91 - 338 * cb_2_2 + 364 * cb_2_4 - 72 * cb_2_6));
        integrals(0, 1)[0] = integrals(1, 0)[9] = 0.0;

        integrals(0, 2)[6] = integrals(2, 0)[6] = integrals(0, 2)[9] = integrals(2, 0)[0] = (1.0 / (c * 576.0)) * (-336 - 27 * b2 - 4 * (319 - 63 * b2) * cb_2_2 + 12 * (181 - 21 * b2) * cb_2_4 - 864 * cb_2_6 + 304 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (1 + 74 * cb_2_2));
        integrals(1, 1)[6] = (1.0 / (c * 576.0)) * (-144 - 27 * b2 - 4 * (451 - 63 * b2) * cb_2_2 + 84 * (23 - 3 * b2) * cb_2_4 + 288 * cb_2_6 - 272 * cb_2_8 + 12 * _beta * cb_2 * sb_2 * (19 + 62 * cb_2_2));
        integrals(0, 2)[7] = integrals(0, 2)[8] = integrals(2, 0)[3] = integrals(2, 0)[1] = (1.0 / (c * 1152)) * (224 - 9 * b2 + 6 * (137 - 39 * b2) * cb_2_2 + 6 * (43 + 6 * b2) * cb_2_4 + 8 * (80 + 9 * b2) * cb_2_6 - 1944 * cb_2_8 + 3 * _beta * sb_2 * cb_2 * (131 - 242 * cb_2_2 - 472 * cb_2_4 - 80 * cb_2_6));
        integrals(1, 1)[7] = integrals(1, 1)[3] = (1.0 / (c * 2304.0)) * (-224 - 45 * b2 + 8 * (541 - 45 * b2) * cb_2_2 - 36 * (50 - 11 * b2) * cb_2_4 - 16 * (202 - 9 * b2) * cb_2_6 + 928 * cb_2_8 - 6 * _beta * sb_2 * cb_2 * (193 + 14 * cb_2_2 + 236 * cb_2_4 + 40 * cb_2_6));
        integrals(2, 0)[7] = integrals(0, 2)[3] = integrals(0, 2)[5] = integrals(2, 0)[2] = (1.0 / (c * 1152.0)) * (-384 - 54 * b2 + 2 * (1357 - 99 * b2) * cb_2_2 - 6 * (115 - 48 * b2) * cb_2_4 - 24 * (88 - 3 * b2) * cb_2_6 + 472 * cb_2_8 - 3 * _beta * sb_2 * cb_2 * (341 - 150 * cb_2_2 + 176 * cb_2_4 + 224 * cb_2_6));
        integrals(1, 1)[8] = integrals(1, 1)[1] = (1.0 / (c * 2304.0)) * (-224 - 45 * b2 + 12 * (131 - 15 * b2) * cb_2_2 - 12 * (79 + 15 * b2) * cb_2_4 + 2432 * cb_2_6 - 2832 * cb_2_8 + 24 * _beta * sb_2 * cb_2 * (49 - 53 * cb_2_2 - 43 * cb_2_4 - 10 * cb_2_6));
        integrals(2, 0)[8] = integrals(2, 0)[5] = integrals(0, 2)[1] = integrals(0, 2)[2] = (1.0 / (c * 576.0)) * (-192 - 27 * b2 + 4 * (143 - 27 * b2) * cb_2_2 + 12 * (115 - 9 * b2) * cb_2_4 - 1296 * cb_2_6 - 464 * cb_2_8 + 12 * _beta * sb_2 * cb_2 * (6 - 38 * cb_2_4 - 28 * cb_2_6));
        integrals(0, 2)[4] = integrals(1, 1)[4] = integrals(2, 0)[4] = (1.0 / (c * 576.0)) * (1216 + 171 * b2 - 16 * (133 - 9 * b2) * cb_2_2 - 12 * (556 - 33 * b2) * cb_2_4 + 16 * (362 - 9 * b2) * cb_2_6 + 1792 * cb_2_8 + 6 * _beta * sb_2 * cb_2 * (77 - 250 * cb_2_2 + 476 * cb_2_4 + 264 * cb_2_6));
        integrals(1, 1)[4] /= 2.0;
        integrals(1, 1)[5] = integrals(1, 1)[2] = (1.0 / (c * 2304.0)) * (-480 - 27 * b2 - 4 * (41 + 27 * b2) * cb_2_2 + 12 * (463 - 9 * b2) * cb_2_4 - 5568 * cb_2_6 + 656 * cb_2_8 - 24 * _beta * sb_2 * cb_2 * (39 - 57 * cb_2_2 + 17 * cb_2_4 + 46 * cb_2_6));
        integrals(1, 1)[9] = integrals(2, 0)[9] = integrals(0, 2)[0] = integrals(1, 1)[0] = 0.0;


        _integrals = integrals;
    }

    GLboolean SecondOrderTrigonometricTriangle3::BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const{

        values.ResizeRows(4);

        GLdouble w = _beta - u - v;

        GLdouble sb = sin(_beta / 2.0), sb4 = pow(sb, 4.0), sb5 = sb4 * sb,
                 cb = cos(_beta / 2.0), cb2 = cb * cb, cb3 = cb2 * cb;

        GLdouble su = sin(u / 2.0), su2 = su * su, su3 = su2 * su, su4 = su2 * su2,
                 cu = cos(u / 2.0), cu2 = cu * cu;

        GLdouble sv = sin(v / 2.0), sv2 = sv * sv, sv3 = sv2 * sv, sv4 = sv2 * sv2,
                 cv = cos(v / 2.0), cv2 = cv * cv;

        GLdouble sw = sin(w / 2.0), sw2 = sw * sw, sw3 = sw2 * sw, sw4 = sw2 * sw2,
                 cw = cos(w / 2.0), cw2 = cw * cw;

        GLdouble r_4_4_0 = 1.0 / sb4,
                 r_4_3_0 = 4.0 * cb / sb4,
                 r_4_2_0 = (2.0 + 4.0 * cb2) / sb4,
                 r_4_3_1 = (4.0 + 8.0 * cb2) / sb5,
                 r_4_2_1 = (16.0 * cb + 8.0 * cb3) / sb5,
                 r_4_2_2 = (10.0 + 20.0 * cb2) / sb4;

        //value
        values(0, 0) = r_4_4_0 * su4;

        values(1, 1) = r_4_3_0 * su3 * sw * cv + 0.5 * r_4_2_0 * su2 * sw2 * cv2;
        values(1, 0) = r_4_3_0 * su3 * sv * cw + 0.5 * r_4_2_0 * su2 * sv2 * cw2;

        values(2, 2) = 0.5 * r_4_2_0 * su2 * sw2 * cv2 + r_4_3_0 * su * sw3 * cv;
        values(2, 1) = r_4_3_1 * (su3 * sw * sv + sv3 * su * sw + sw3 * sv * su) +
                  r_4_2_1 * (su2 * sw2 * cv * sv + su2 * sv2 * cw * sw + sw2 * sv2 * cu * su) +
                  r_4_2_2 * su2 * sv2 * sw2;
        values(2, 0) = 0.5 * r_4_2_0 * su2 * sv2 * cw2 + r_4_3_0 * su * sv3 * cw;

        values(3, 3) = r_4_4_0 * sw4;
        values(3, 2) = r_4_3_0 * sw3 * sv * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        values(3, 1) = r_4_3_0 * sv3 * sw * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        values(3, 0) = r_4_4_0 * sv4;

        return GL_TRUE;
    }

    GLboolean SecondOrderTrigonometricTriangle3::CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives &pd) const{

        if (!pd.ResizeRows(3))
       {
           return GL_FALSE;
       }

        GLdouble w = _beta - u - v;

        GLdouble sb = sin(_beta / 2.0), sb4 = pow(sb, 4.0), sb5 = sb4 * sb,
                 cb = cos(_beta / 2.0), cb2 = cb * cb, cb3 = cb2 * cb;

        GLdouble su = sin(u / 2.0), su2 = su * su, su3 = su2 * su, su4 = su2 * su2,
                 cu = cos(u / 2.0), cu2 = cu * cu;

        GLdouble sv = sin(v / 2.0), sv2 = sv * sv, sv3 = sv2 * sv, sv4 = sv2 * sv2,
                 cv = cos(v / 2.0), cv2 = cv * cv;

        GLdouble sw = sin(w / 2.0), sw2 = sw * sw, sw3 = sw2 * sw, sw4 = sw2 * sw2,
                 cw = cos(w / 2.0), cw2 = cw * cw;

        GLdouble r_4_4_0 = 1.0 / sb4,
                 r_4_3_0 = 4.0 * cb / sb4,
                 r_4_2_0 = (2.0 + 4.0 * cb2) / sb4,
                 r_4_3_1 = (4.0 + 8.0 * cb2) / sb5,
                 r_4_2_1 = (16.0 * cb + 8.0 * cb3) / sb5,
                 r_4_2_2 = (10.0 + 20.0 * cb2) / sb4;

        TriangularMatrix<GLdouble> T(4), duT(4), dvT(4), duuT(4), duvT(4);

        //T

        T(0, 0) = r_4_4_0 * su4;

        T(1, 1) = r_4_3_0 * su3 * sw * cv + 0.5 * r_4_2_0 * su2 * sw2 * cv2;
        T(1, 0) = r_4_3_0 * su3 * sv * cw + 0.5 * r_4_2_0 * su2 * sv2 * cw2;

        T(2, 2) = 0.5 * r_4_2_0 * su2 * sw2 * cv2 + r_4_3_0 * su * sw3 * cv;
        T(2, 1) = r_4_3_1 * (su3 * sw * sv + sv3 * su * sw + sw3 * sv * su) +
                  r_4_2_1 * (su2 * sw2 * cv * sv + su2 * sv2 * cw * sw + sw2 * sv2 * cu * su) +
                  r_4_2_2 * su2 * sv2 * sw2;
        T(2, 0) = 0.5 * r_4_2_0 * su2 * sv2 * cw2 + r_4_3_0 * su * sv3 * cw;

        T(3, 3) = r_4_4_0 * sw4;
        T(3, 2) = r_4_3_0 * sw3 * sv * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        T(3, 1) = r_4_3_0 * sv3 * sw * cu + 0.5 * r_4_2_0 * sw2 * sv2 * cu2;
        T(3, 0) = r_4_4_0 * sv4;

        //duT

        duT(0, 0) = 2.0 * r_4_4_0 * su3 * cu;

        duT(1, 1) = r_4_3_0 * (1.5 * su2 * cu * sw * cv - 0.5 * su3 * cw * cv) + 0.5 * r_4_2_0 * (su * cu * sw2 * cv2 - su2 * sw * cw * cv2);
        duT(1, 0) = r_4_3_0 * (1.5 * su2 * cu * sv * cw + 0.5 * su3 * sv * sw) + 0.5 * r_4_2_0 * (su * cu * sv2 * cw2 + su2 * sv2 * cw * sw);

        duT(2, 2) = 0.5 * r_4_2_0 * (su * cu * sw2 * cv2 - su2 * sw * cw * cv2) + r_4_3_0 * (0.5 * cu * sw3 * cv - 1.5 * su * sw2 * cw * cv);

        duT(2, 1) = r_4_3_1 * ((1.5 * su2 * cu * sw * sv - 0.5 * su3 * cw * sv) + (0.5 * sv3 * cu * sw - 0.5 * sv3 * su * cw) + (-1.5 * sw2 * cw * sv * su + 0.5 * sw3 * sv * cu)) +
                    r_4_2_1 * ((su * cu * sw2 * cv * sv - su2 * sw * cw * cv * sv) + (su * cu * sv2 * cw * sw + 0.5 * su2 * sv2 * sw2 - 0.5 * su2 * sv2 * cw2) + (-sw * cw * sv2 * cu * su - 0.5 * sw2 * sv2 * su2 + 0.5 * sw2 * sv2 * cu2)) +
                    r_4_2_2 * (su * cu * sv2 * sw2 - su2 * sv2 * sw * cw);

        duT(2, 0) = 0.5 * r_4_2_0 * (su * cu * sv2 * cw2 + su2 * sv2 * cw * sw) + r_4_3_0 * (0.5 * cu * sv3 * cw + 0.5 * su * sv3 * sw);

        duT(3, 3) = -2.0 * r_4_4_0 * sw3 * cw;
        duT(3, 2) = r_4_3_0 * (-1.5 * sw2 * cw * sv * cu - 0.5 * sw3 * sv * su) + 0.5 * r_4_2_0 * (-sw * cw * sv2 * cu2 - sw2 * sv2 * cu * su);
        duT(3, 1) = r_4_3_0 * (-0.5 * sv3 * cw * cu - 0.5 * sv3 * sw * su) + 0.5 * r_4_2_0 * (-sw * cw * sv2 * cu2 - sw2 * sv2 * cu * su);
        duT(3, 0) = 0.0;


        //duuT

        duuT(0, 0) = 2.0 * r_4_4_0 * 1.5 * su2 * cu2;
        duuT(1, 1) = r_4_3_0 * (1.5 * (su * cu2 * sw - 0.5 * su2 * su * sw - 0.5 * su2 * cu * cw) * cv - 0.5 * (1.5 * su2 * cu * cw + 0.5 * su3 * sw) * cv) + 0.5 * r_4_2_0 * ((0.5 * cu2 * sw2 - 0.5 * su2 * sw2 - su * cu * sw * cw) * cv2 - (su * cu * sw * cw - 0.5 * su2 * cw2 + 0.5 * su2 * sw2) * cv2);
        duuT(1, 0) = r_4_3_0 * (1.5 * (su * cu2 * cw - 0.5 * su2 * su * cw + 0.5 * su2 * cu * sw) * sv + 0.5 * (1.5 * su * cu * sw - 0.5 * su3 * cw) * sv) + 0.5 * r_4_2_0 * ((0.5 * cu2 * cw2 - 0.5 * su2 * cw2 + 0.5 * su * cu * sw2) * sv2 + sv2 * (su * cu * cw * sw + 0.5 * su2 * sw2 - 0.5 * su2 * cw2));

        duuT(2, 2) = 0.5 * r_4_2_0 * ((0.5 * cu2 * sw2 - 0.5 * su2 * sw2 - su * cu * sw * cw) * cv2 - (su * cu * sw * cw - 0.5 * su2 * cw2 + 0.5 * su2 * sw2) * cv2) + r_4_3_0 * (0.5 * (-0.5 * su * sw3 - 1.5 * cu * sw2 * cw) * cv - 1.5 * (0.5 * cu * sw2 * cw - su * sw * cw2 + 0.5 * su * sw3) * cv);

        duuT(2, 1) = r_4_3_1 * ((1.5 * (su * cu2 * sw - 0.5 * su3 * sw - 0.5 * su2 * cu * cw) * sv - 0.5 * (1.5 * su2 * cu * cw + 0.5 * su3 * sw) * sv) + (0.5 * sv3 * (-0.5 * su * sw - 0.5 * cu * cw) - 0.5 * sv3 * (0.5 * cu * cw + 0.5 * su * sw)) + (-1.5 * (-sw * cw2 * su + 0.5 * sw3 * su + 0.5 * sw2 * cw * cu) * sv + 0.5 * (-1.5 * sw2 * cw2 * cu - 0.5 * sw3 * su) * sv)) +
                    r_4_2_1 * (((0.5 * cu2 * sw2 - 0.5 * su2 * sw2 - su * cu * sw * cw) * cv * sv - (su * cu * sw * cw - 0.5 * su2 * cw2 + 0.5 * su2 * sw2) * cv * sv) + ((0.5 * cu2 * cw * sw - 0.5 * su2 * cw * sw + 0.5 * su * cu * sw2 - 0.5 * su * cu * cw2) * sv2 + 0.5 * (su * cu * sw2 - su2 * sw * cw) * sv2 - 0.5 * (su * cu * cw2 + su2 * cw * sw) * sv2) + (-(-0.5 * cw2 * cu * su + 0.5 * sw2 * cu * su - 0.5 * sw * cw * su2 + 0.5 * sw * cw * cu2) * sv2 - 0.5 * (-sw * cw * su2 + sw2 * su * cu) * sv2 + 0.5 * (-sw * cw * cu2 - sw2 * cu * su) * sv2)) +
                    r_4_2_2 * ((0.5 * cu2 * sw2 - 0.5 * su2 * sw2 - su * cu * sw * cw) * sv2 - (su * cu * sw * cw - 0.5 * su2 * cw2 + 0.5 * su2 * sw2) * sv2);

        duuT(2, 0) = 0.5 * r_4_2_0 * ((0.5 * cu2 * cw2 - 0.5 *  su2 * cw2 + su * cu * cw * sw) * sv2 + (su * cu * cw * sw + 0.5 * su2 * sw2 - 0.5 * su2 * cw2) * sv2) + r_4_3_0 * (0.5 * (-0.5 * su * cw + 0.5 * cu * sw) * sv3 + 0.5 * (0.5 * cu * sw - 0.5 * su * cw) * sv3);

        duuT(3, 3) = -2.0 * r_4_4_0 * (-1.5 * sw2 * cw2 + 0.5 * sw4);
        duuT(3, 2) = r_4_3_0 * (-1.5 * (-sw * cw2 * cu + 0.5 * sw3 * cu - 0.5 * sw2 * cw * su) * sv - 0.5 * (-1.5 * sw2 * cw * su +  0.5 * sw3 * cu) * sv) + 0.5 * r_4_2_0 * (-(-0.5 * cw2 * cu2 + 0.5 * sw2 * cu2 - sw * cw * cu * su) * sv2 - (-sw * cw * cu * su - 0.5 * sw2 * su2 + 0.5 * sw2 * cu2) * sv2);
        duuT(3, 1) = r_4_3_0 * (-0.5 * sv3 * (0.5 * sw * cu - 0.5 * cw * su) - 0.5 * sv3 * (-0.5 * cw * su + 0.5 * sw * cu)) + 0.5 * r_4_2_0 * (-(-0.5 * cw2 * cu2 + 0.5 * sw2 * cu2 - sw * cw * cu * su) * sv2 - (-sw * cw * cu * su - 0.5 * sw2 * su2 + 0.5 * sw2 * cu2) * sv2);
        duuT(3, 0) = 0.0;

        //dvT

        dvT(0, 0) = 0.0;

        dvT(1, 1) = r_4_3_0 * (-0.5 * su3 * cw * cv - 0.5 * su3 * sw * sv) + 0.5 * r_4_2_0 * (- su2 * sw * cw * cv2 - su2 * sw2 * cv * sv);
        dvT(1, 0) = r_4_3_0 * (0.5 * su3 * cv * cw + 0.5 * su3 * sv * sw) + 0.5 * r_4_2_0 * (su2 * sv * cv * cw2 + su2 * sv2 * cw * sw);

        dvT(2, 2) = 0.5 * r_4_2_0 * (-su2 * sw * cw * cv2 - su2 * sw2 * cv * sv) + r_4_3_0 * (-1.5 * su * sw2 * cw * cv - 0.5 * su * sw3 * sv);
        dvT(2, 1) = r_4_3_1 * ((-0.5 * su3 * cw * sv + 0.5 * su3 * sw * cv) + (1.5 * sv2 * cv * su * sw - 0.5 * sv3 * su * cw) + (-1.5 * sw2 * cw * sv * su + 0.5 * sw3 * cv * su)) +
                    r_4_2_1 * ((-su2 * sw * cw * cv * sv - 0.5 * su2 * sw2 * sv2 + 0.5 * su2 * sw2 * cv2) + (su2 * sv * cv * cw * sw + 0.5 * su2 * sv2 * sw2 - 0.5 * su2 * sv2 * cw2) + (-sw * cw * sv2 * cu * su + sw2 * sv * cv * cu * su)) +
                    r_4_2_2 * (su2 * sv * cv * sw2 - su2 * sv2 * sw * cw);
        dvT(2, 0) = 0.5 * r_4_2_0 * (su2 * sv * cv * cw2 + su2 * sv2 * cw * sw) + r_4_3_0 * (1.5 * su * sv2 * cv * cw + 0.5 * su * sv3 * sw);

        dvT(3, 3) = - 2.0 * r_4_4_0 * sw3 * cw;
        dvT(3, 2) = r_4_3_0 * (-1.5 * sw2 * cw * sv * cu + 0.5 * sw3 * cv * cu) + 0.5 * r_4_2_0 * (sw2 * sv * cv * cu2 - sw * cw * sv2 * cu2);
        dvT(3, 1) = r_4_3_0 * (1.5 * sv2 * cv * sw * cu - 0.5 * sv3 * cw * cu) + 0.5 * r_4_2_0 * (-sw * cw * sv2 * cu2 + sw2 * sv * cv * cu2);
        dvT(3, 0) = 2.0 * r_4_4_0 * sv3 * cv;

        pd.LoadNullVectors();

        DCoordinate3 &point = pd(0, 0), &diff1u = pd(1, 0), &diff1v = pd(1, 1), &diffluu = pd(2, 0), &diffluv = pd(2, 1), &difflvv = pd(2, 2);

        for (GLuint k = 0; k < 4; ++k)
        {
            for (GLuint l = 0; l <= k; ++l)
            {
                point  += _net(k, l) *   T(k, l);
                diff1u += _net(k, l) * duT(k, l);
                diff1v += _net(k, l) * dvT(k, l);
                diffluu += _net(k, l) * duuT(k, l);
//                diffluv += _net(k, l) * duvT(k, l);
//                difflvv += _net(k, l) * dvvT(k, l);
            }
        }

       return GL_TRUE;
    }
}
