#include "MainWindow.h"

namespace cagd
{
    MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
    {
        setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */
        QApplication::setOverrideCursor(Qt::WaitCursor);

        _side_widget = new SideWidget(this);

        _scroll_area = new QScrollArea(this);
        _scroll_area->setWidget(_side_widget);
        _scroll_area->setSizePolicy(_side_widget->sizePolicy());
        _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        _gl_widget = new GLWidget(this);

        centralWidget()->setLayout(new QHBoxLayout());
        centralWidget()->layout()->addWidget(_gl_widget);
        centralWidget()->layout()->addWidget(_scroll_area);

        // creating a signal slot mechanism between the rendering context and the side widget
        connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));

        connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        connect(_side_widget->theta_1, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_theta_1(double)));
        connect(_side_widget->theta_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_theta_2(double)));

        connect(_side_widget->basis_function, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_basis_function(int)));
        connect(_side_widget->weight_function, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_weight_function(int)));
        connect(_side_widget->shape_parameter, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_shape_parameter(double)));

//        connect(_side_widget->triangles, SIGNAL(toggled(bool)), _gl_widget, SLOT(set_triangle(bool)));
//        connect(_side_widget->curves, SIGNAL(toggled(bool)), _gl_widget, SLOT(set_curve(bool)));
//        connect(_side_widget->shapes_curves, SIGNAL(toggled(bool)), _gl_widget, SLOT(set_shape_curve_network(bool)));
//        connect(_side_widget->shapes_C0, SIGNAL(toggled(bool)), _gl_widget, SLOT(set_shape_C0(bool)));

        connect(_side_widget->shader, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_shader(int)));
        connect(_side_widget->material, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_material(int)));
        connect(_side_widget->poligon_mode, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_poligon_mode(int)));

        connect(_side_widget->div_point_count, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_div_point_count(int)));

        //epsilon parameters
        connect(_side_widget->eps_1, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_eps_1(double)));
        connect(_side_widget->eps_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_eps_2(double)));

        //show vertices and the scale factor
        connect(_side_widget->vertices_check, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_vertices_check(int)));
        connect(_side_widget->vertices_size, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_vertices_size(double)));

        //show normals and the scale factor
        connect(_side_widget->vertex_normals_check, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_normals_check(int)));
        connect(_side_widget->vertex_normals_size, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_normals_size(double)));

        //show tangents and the scale factor
        connect(_side_widget->tangents_check, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_tangents_check(int)));
        connect(_side_widget->tangents_size, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_tangents_size(double)));

        //show boundaries
        connect(_side_widget->boundaries_check, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_boundaries_check(int)));

        //show boundaries normals and the scale factor
        connect(_side_widget->boundary_normals_check, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_boundaries_normal_check(int)));
        connect(_side_widget->boundary_normals_size, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_boundaries_normal_size(double)));

        //show geometry or not
        connect(_side_widget->show_geometry, SIGNAL(stateChanged(int)), _gl_widget, SLOT(set_geometry_check(int)));

        //set the selected geometry
        connect(_side_widget->my_geometry, SIGNAL(currentIndexChanged(int)), _gl_widget, SLOT(set_geometry(int)));

        connect(_side_widget->vertex_number, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_vertex_number(int)));

        //connect the actions with functions (models)
        connect(actionelephant_v102_f200, SIGNAL(triggered(bool)), _gl_widget, SLOT(elephant_v102_f200_checked(bool)));
        connect(actionm2_v3495_f6667, SIGNAL(triggered(bool)), _gl_widget, SLOT(m2_v3495_f666_checked(bool)));
        connect(actionspot_v122_f240, SIGNAL(triggered(bool)), _gl_widget, SLOT(spot_v122_f240_checked(bool)));
        connect(actionbunny_v113_f222, SIGNAL(triggered(bool)), _gl_widget, SLOT(bunny_v113_f222_checked(bool)));
        connect(actionbunny_v239_f474, SIGNAL(triggered(bool)), _gl_widget, SLOT(bunny_v239_f474_checked(bool)));
        connect(actionhorse_v341_f678, SIGNAL(triggered(bool)), _gl_widget, SLOT(horse_v341_f678_checked(bool)));
        connect(actionm195_v2472_f4809, SIGNAL(triggered(bool)), _gl_widget, SLOT(m195_v2472_f4809_checked(bool)));
        connect(actionangel_v1352_f2706, SIGNAL(triggered(bool)), _gl_widget, SLOT(angel_v1352_f2706_checked(bool)));
        connect(actiondragon_v1386_f2772, SIGNAL(triggered(bool)), _gl_widget, SLOT(dragon_v1386_f2772_checked(bool)));
        connect(actionicosahedron_v12_f20, SIGNAL(triggered(bool)), _gl_widget, SLOT(icosahedron_v12_f20_checked(bool)));

        connect(_gl_widget, SIGNAL(statusMessageChanged(const QString&)), statusBar(), SLOT(showMessage(const QString&)));
        connect(_gl_widget, SIGNAL(setCalculationTimeCurveNetwork(double)), _side_widget->time_curve_network, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(setCalculationTimeC0Surfaces(double)), _side_widget->time_c0_patches, SLOT(setValue(double)));
        connect(_gl_widget, SIGNAL(setCalculationTimeG1Surfaces(double)), _side_widget->time_g1_patches, SLOT(setValue(double)));

        connect(_gl_widget, SIGNAL(setCursor(const QCursor&)), this, SLOT(changeCursor(const QCursor&)));
    }

    //--------------------------------
    // implementation of private slots
    //--------------------------------
    void MainWindow::on_action_Quit_triggered()
    {
        qApp->exit(0);
    }

    void MainWindow::changeCursor(const QCursor& cursor){
        QApplication::setOverrideCursor(cursor);
    }
}
