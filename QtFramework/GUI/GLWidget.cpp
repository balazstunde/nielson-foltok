#include "GLWidget.h"
#include <GL/glu.h>
#include "../Test/TestFunctions.h"
#include "../Core/Exceptions.h"
#include "../Core/Constants.h"
#include "../Core/Matrices.h"
#include "../Core/RealSquareMatrices.h"
#include "../Core/Materials.h"
#include "../SpecificStructures/TriangulatedHalfEdgeDataStructure3.h"

#include <map>
#include "fstream"
#include "../Parametric/ParametricSurfaces3.h"
#include <Core/Exceptions.h>
#include <iostream>
#include <QTimer>
#include <QTime>

using namespace std;
using namespace cagd;

namespace cagd
{
    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent, const QGLFormat &format): QGLWidget(format, parent)
    {
        _timer = new QTimer(this);
        _timer->setInterval(0);

    }


    //--------------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------
    void GLWidget::initializeGL()
    {

        // creating a perspective projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = (float)width() / (float)height();
        _z_near = 1.0;
        _z_far = 1000.0;
        _fovy = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

        // enabling depth test
        glEnable(GL_DEPTH_TEST);

        // setting the color of background
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;
        //_theta_1 = _theta_2 = _theta_3 = 0.0;
        _theta.ResizeColumns(4);
        _theta[0] = 0.0;
        _theta[3] = 0.0;
        _theta[1] = 0.2;
        _theta[2] = 0.0;

        // set default values of epsilon parameters
        _epsilon.ResizeColumns(3);
        _epsilon[0] = 0.0;
        _epsilon[1] = 0.2;
        _epsilon[2] = 0.0;

        _shape_parameter = 1.0;
        _basis_function = 0;

        try
        {
            // initializing the OpenGL Extension Wrangler library
            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                "Try to update your driver or buy a new graphics adapter!");
            }


            glEnable(GL_LIGHTING);
            glEnable(GL_NORMALIZE);

            HCoordinate3 direction(0.0f, 0.0f, 1.0f, 0.0f);
            Color4       ambient(0.4f, 0.4f, 0.4f, 1.0f);
            Color4       diffuse(0.8f, 0.8f, 0.8f, 1.0f);
            Color4       specular(1.0f, 1.0f, 1.0f, 1.0f);
            _dl = new DirectionalLight(GL_LIGHT0, direction, ambient, diffuse, specular);

            initBezierCurve();
            initQuarticPolinomialCurve();
            initSecondOrderTrigonometricCurve();
//            initSecondOrderHyperbolicCurve();
            initFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3();
            initBezierTriangle();
            initQuarticPolinomialTriangle();
            initAlgebraicTrigonometricTriangle();
//            initSecondOrderHyperbolicTriangle();
            initSecondOrderTrigonometricTriangle();
            initActualModel(_actual_file_name);
            initSphere();
            if (!_two_sided_lighting.InstallShaders("shaders/two_sided_lighting.vert",
                                        "shaders/two_sided_lighting.frag"))
            {
                throw Exception("Could not load the two sided shader files!");
            }

            if (!_reflection_lines.InstallShaders("shaders/reflection_lines.vert",
                                        "shaders/reflection_lines.frag"))
            {
                throw Exception("Could not load the reflection shader files!");
            }

            if (!_toonify.InstallShaders("shaders/toon.vert",
                                        "shaders/toon.frag"))
            {
                throw Exception("Could not load the toon shader files!");
            }

            _toonify.Enable();
            _toonify.SetUniformVariable4f("default_outline_color", 0.827f, 0.792f, 0.588f, 1.0f);
            _toonify.Disable();

            _reflection_lines.Enable();
            _reflection_lines.SetUniformVariable1f("scale_factor", 4.0);
            _reflection_lines.SetUniformVariable1f("smoothing", 2.0);
            _reflection_lines.SetUniformVariable1f("shading", 1.0);
            _reflection_lines.Disable();

            glEnable(GL_LIGHTING);
//            _dl->Enable();

        }
        catch (Exception &e)
        {
            cout << e << endl;

        }

    }

    void GLWidget::initSphere(){
        if(_sphere.LoadFromOFF("Models/sphere.off", true)){
            if(_sphere.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                _angle = 0.0;
                _timer->start();
            }
        }else{
            cout << "Something went wrong" << endl;
        }
    }

    void GLWidget::initActualModel(string file_name){

//        if(!_actual_model_mesh.LoadFromOFF(file_name, true)){
//            throw Exception("Could not load the model.");
//        }else{
//            if(!_actual_model_mesh.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
//                throw Exception("Could not update the VBO.");
//            }
//        }

        _actual_model = new TriangulatedHalfEdgeDataStructure3();
        if(!_actual_model){
            throw Exception("Could not create the model.");
        }


        emit setCursor(Qt::BusyCursor);
//        cout << file_name << endl;

        emit statusMessageChanged("Loading the model from an .off file...");
        if(!_actual_model->LoadFromOFF(file_name, true)){
            throw Exception("Could not load the .off file.");
        }

        QTime timer;

        timer.start();
        switch (_basis_function) {
        case 0:{
            emit statusMessageChanged("Generating boundary curves...");
            _actual_model->GenerateBoundaryCurves(TriangulatedHalfEdgeDataStructure3::BoundaryType::SECOND_ORDER_TRIGONOMETRIC, _shape_parameter, _theta, 2, _div_point_count);
//            cout << "Curves done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeCurveNetwork(timer.elapsed() / 1000.0);
            timer.restart();
            _actual_model->GenerateC0TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::SECOND_ORDER_TRIGONOMETRIC, _shape_parameter, _epsilon, _div_point_count);
//            cout << "C0 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeC0Surfaces(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating G1 surfaces...");
            _actual_model->GenerateG1TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::SECOND_ORDER_TRIGONOMETRIC, _shape_parameter, _theta, _div_point_count, _weight_function);
//            cout << "G1 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeG1Surfaces(timer.elapsed() / 1000.0);
            break;
        }
        case 1:{
            emit statusMessageChanged("Generating boundary curves...");
            _actual_model->GenerateBoundaryCurves(TriangulatedHalfEdgeDataStructure3::BoundaryType::QUARTIC_POLYNOMIAL, _shape_parameter, _theta, 2, _div_point_count);
//            cout << "Curves done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeCurveNetwork(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating C0 surfaces...");
            _actual_model->GenerateC0TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::QUARTIC_POLYNOMIAL, _shape_parameter, _epsilon, _div_point_count);
//            cout << "C0 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeC0Surfaces(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating G1 surfaces...");
            _actual_model->GenerateG1TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::QUARTIC_POLYNOMIAL, _shape_parameter, _theta, _div_point_count, _weight_function);
//            cout << "G1 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeG1Surfaces(timer.elapsed() / 1000.0);
            break;
        }

        case 2:{
            emit statusMessageChanged("Generating boundary curves...");
            _actual_model->GenerateBoundaryCurves(TriangulatedHalfEdgeDataStructure3::BoundaryType::ALGEBRAIC_TRIGONOMETRIC, _shape_parameter, _theta, 2, _div_point_count);
//            cout << "Curves done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeCurveNetwork(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating C0 surfaces...");
            _actual_model->GenerateC0TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::ALGEBRAIC_TRIGONOMETRIC, _shape_parameter, _epsilon, _div_point_count);
//            cout << "C0 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeC0Surfaces(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating G1 surfaces...");
            _actual_model->GenerateG1TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::ALGEBRAIC_TRIGONOMETRIC, _shape_parameter, _theta, _div_point_count, _weight_function);
//            cout << "G1 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeG1Surfaces(timer.elapsed() / 1000.0);
            break;
        }

        case 3:{
            emit statusMessageChanged("Generating boundary curves...");
            _actual_model->GenerateBoundaryCurves(TriangulatedHalfEdgeDataStructure3::BoundaryType::CUBIC_BEZIER, _shape_parameter, _theta, 2, _div_point_count);
//            cout << "Curves done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeCurveNetwork(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating C0 surfaces...");
            _actual_model->GenerateC0TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::CUBIC_BEZIER, _shape_parameter, _epsilon, _div_point_count);
//            cout << "C0 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeC0Surfaces(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating G1 surfaces...");
            _actual_model->GenerateG1TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::CUBIC_BEZIER, _shape_parameter, _theta, _div_point_count, _weight_function);
//            cout << "G1 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeG1Surfaces(timer.elapsed() / 1000.0);
            break;
        }

         default:{
            emit statusMessageChanged("Generating boundary curves...");
            _actual_model->GenerateBoundaryCurves(TriangulatedHalfEdgeDataStructure3::BoundaryType::CUBIC_BEZIER, _shape_parameter, _theta, 2, _div_point_count);
//            cout << "Curves done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeCurveNetwork(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating C0 surfaces...");
            _actual_model->GenerateC0TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::CUBIC_BEZIER, _shape_parameter, _epsilon, _div_point_count);
//            cout << "C0 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeC0Surfaces(timer.elapsed() / 1000.0);
            timer.restart();
            emit statusMessageChanged("Generating G1 surfaces...");
            _actual_model->GenerateG1TriangularSurfaces(TriangulatedHalfEdgeDataStructure3::BoundaryType::CUBIC_BEZIER, _shape_parameter, _theta, _div_point_count, _weight_function);
//            cout << "G1 surfaces done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
            emit setCalculationTimeG1Surfaces(timer.elapsed() / 1000.0);
            break;
        }

        }
        emit setCursor(Qt::ArrowCursor);
        emit statusMessageChanged("Done.");
    }

    void GLWidget::initBezierTriangle(){
        TriangularMatrix<DCoordinate3> border_net;
        border_net.ResizeRows(4);

        GLdouble angle = 2.0 * PI / 3.0;
        DCoordinate3 p(cos(0 * angle), sin(0 * angle)),
                     q(cos(1 * angle), sin(1 * angle)),
                     r(cos(2 * angle), sin(2 * angle));

        GLuint div_point_count = 4;

        GLdouble ds = 1.0 / (GLdouble)(div_point_count - 1);

        for (GLuint i = 0; i < div_point_count; ++i)
        {
            GLdouble s = min(i * ds, 1.0);

            DCoordinate3 x = p + (q - p) * s;
            DCoordinate3 y = p + (r - p) * s;

            GLdouble dt = i ? 1.0 / (GLdouble)i : 0.0;

            for (GLuint j = 0; j <= i; ++j)
            {
                GLdouble t = min(j * dt, 1.0);
                DCoordinate3 z = x + (y - x) * t;

                border_net(i, j) = z;
//                border_net(i, j)[2] = -2.0 + 4.0 * (i+j) / 9.0;
            }
        }

//        border_net(0, 0)[2] += 1.0;// = DCoordinate3(0.0, 0.0, 0.0);

        border_net(1, 0)[2] += 1.0;
        border_net(2, 0)[2] -= 1.0;

        border_net(1, 1)[2] -= 1.75;
        border_net(2, 2)[2] += 1.5;

        _bt = new (nothrow)CubicBezierTriangle3( border_net);

        if (!_bt){
            throw Exception("Could not create the cubic Bezier triangle!");
        }
        _bt->UpdateControlPointByEnergyOptimization(_epsilon);

        if (!_bt->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the cubic Bezier triangle's control polygon!");
        }

        //_max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
       // _div_point_count          = 10;
        _img_bt = _bt->GenerateImage(_div_point_count);

        if (!_img_bt){
            throw Exception("Could not create the image of the cubic Bezier triangle!");
        }

        if (!_img_bt->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the cubic Bezier triangle's image!");
        }

    }

    void GLWidget::initQuarticPolinomialTriangle(){
        TriangularMatrix<DCoordinate3> border_net;
        border_net.ResizeRows(4);

        GLdouble angle = 2.0 * PI / 3.0;
        DCoordinate3 p(cos(0 * angle), sin(0 * angle)),
                     q(cos(1 * angle), sin(1 * angle)),
                     r(cos(2 * angle), sin(2 * angle));

        GLuint div_point_count = 4;

        GLdouble ds = 1.0 / (GLdouble)(div_point_count - 1);

        for (GLuint i = 0; i < div_point_count; ++i)
        {
            GLdouble s = min(i * ds, 1.0);

            DCoordinate3 x = p + (q - p) * s;
            DCoordinate3 y = p + (r - p) * s;

            GLdouble dt = i ? 1.0 / (GLdouble)i : 0.0;

            for (GLuint j = 0; j <= i; ++j)
            {
                GLdouble t = min(j * dt, 1.0);
                DCoordinate3 z = x + (y - x) * t;

                border_net(i, j) = z;
//                border_net(i, j)[2] = -2.0 + 4.0 * ((i+j) * 1.0) / 9.0;
            }
        }

        //        border_net(0, 0)[2] += 1.0;// = DCoordinate3(0.0, 0.0, 0.0);

                border_net(1, 0)[2] += 1.0;
                border_net(2, 0)[2] -= 1.0;

                border_net(1, 1)[2] -= 1.75;
                border_net(2, 2)[2] += 1.5;

        _qt = new (nothrow)QuarticPolinomialTriangle3(border_net);

        if (!_qt){
            throw Exception("Could not create the cubic Quartic polinomial triangle!");
        }
        _qt->UpdateControlPointByEnergyOptimization(_epsilon);

        if (!_qt->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the cubic Quartic polinomial triangle's control polygon!");
        }

        //_max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
        //_div_point_count          = 30;
        _img_qt = _qt->GenerateImage(_div_point_count);

        if (!_img_qt){
            throw Exception("Could not create the image of the cubic Quartic polinomial triangle!");
        }

        if (!_img_qt->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the cubic Quartic polinomial triangle's image!");
        }
    }

    void GLWidget::initAlgebraicTrigonometricTriangle(){
        TriangularMatrix<DCoordinate3> border_net;
        border_net.ResizeRows(4);

        GLdouble angle = 2.0 * PI / 3.0;
        DCoordinate3 p(cos(0 * angle), sin(0 * angle)),
                     q(cos(1 * angle), sin(1 * angle)),
                     r(cos(2 * angle), sin(2 * angle));

        GLuint div_point_count = 4;

        GLdouble ds = 1.0 / (GLdouble)(div_point_count - 1);

        for (GLuint i = 0; i < div_point_count; ++i)
        {
            GLdouble s = min(i * ds, 1.0);

            DCoordinate3 x = p + (q - p) * s;
            DCoordinate3 y = p + (r - p) * s;

            GLdouble dt = i ? 1.0 / (GLdouble)i : 0.0;

            for (GLuint j = 0; j <= i; ++j)
            {
                GLdouble t = min(j * dt, 1.0);
                DCoordinate3 z = x + (y - x) * t;

                border_net(i, j) = z;
//                border_net(i, j)[2] = -2.0 + 4.0 * (i+j) / 9.0;
            }
        }

//        border_net(0, 0)[2] += 1.0;// = DCoordinate3(0.0, 0.0, 0.0);

        border_net(1, 0)[2] += 1.0;
        border_net(2, 0)[2] -= 1.0;

        border_net(1, 1)[2] -= 1.75;
        border_net(2, 2)[2] += 1.5;

        _att = new (nothrow)AlgebraicTrigonometricTriangle3( border_net, _shape_parameter);

        if (!_att){
            throw Exception("Could not create the cubic Bezier triangle!");
        }
        _att->UpdateControlPointByEnergyOptimization(_epsilon);

        if (!_att->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the algebraic trigonometric triangle's control polygon!");
        }

        //_max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
       // _div_point_count          = 10;
        _img_att = _att->GenerateImage(_div_point_count);

        if (!_img_att){
            throw Exception("Could not create the image of the algebraic trigonoemtric triangle!");
        }

        if (!_img_att->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the algebraic trigonometric triangle's image!");
        }

    }

    void GLWidget::initSecondOrderTrigonometricTriangle(){
        TriangularMatrix<DCoordinate3> border_net;
        border_net.ResizeRows(4);

        GLdouble angle = 2.0 * PI / 3.0;
        DCoordinate3 p(cos(0 * angle), sin(0 * angle)),
                     q(cos(1 * angle), sin(1 * angle)),
                     r(cos(2 * angle), sin(2 * angle));

        GLuint div_point_count = 4;

        GLdouble ds = 1.0 / (GLdouble)(div_point_count - 1);

        for (GLuint i = 0; i < div_point_count; ++i)
        {
            GLdouble s = min(i * ds, 1.0);

            DCoordinate3 x = p + (q - p) * s;
            DCoordinate3 y = p + (r - p) * s;

            GLdouble dt = i ? 1.0 / (GLdouble)i : 0.0;

            for (GLuint j = 0; j <= i; ++j)
            {
                GLdouble t = min(j * dt, 1.0);
                DCoordinate3 z = x + (y - x) * t;

                border_net(i, j) = z;
//                border_net(i, j)[2] = -2.0 + 4.0 * (i+j) / 9.0;
            }
        }


//        border_net(0, 0)[2] += 1.0;// = DCoordinate3(0.0, 0.0, 0.0);

        border_net(1, 0)[2] += 1.0;
        border_net(2, 0)[2] -= 1.0;

        border_net(1, 1)[2] -= 1.75;
        border_net(2, 2)[2] += 1.5;
        _stt = new (nothrow)SecondOrderTrigonometricTriangle3(border_net, _shape_parameter);

        if (!_stt){
            throw Exception("Could not create the cubic Second order trigonometric triangle!");
        }

//        _epsilon[2] = 0.2;
        _stt->UpdateControlPointByEnergyOptimization(_epsilon);

        if (!_stt->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the cubic Second order trigonometric triangle's control polygon!");
        }

        //_max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
        //_div_point_count          = 20;
        _img_stt = _stt->GenerateImage(_div_point_count);

        //cout << *_img_stt << endl;

        if (!_img_stt){
            throw Exception("Could not create the image of the cubic Second order trigonometric triangle!");
        }

        if (!_img_stt->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the cubic Second order trigonometric triangle's image!");
        }
    }

    void GLWidget::initBezierCurve(){
        _bc = new (nothrow) CubicBezierCurve3(DCoordinate3(-1,1,0), DCoordinate3(1,1,0), DCoordinate3(1,1,0), DCoordinate3(-1,1,0));

        if (!_bc){
            throw Exception("Could not create the cubic Bezier curve!");
        }

//        GLdouble step = TWO_PI / (4);
//        for (GLuint i = 0; i < 4; i++){
//            GLdouble u = i * step;  // uniform subdivision point in the interval [0,2pi]
//            DCoordinate3 &cp_i = (*_bc)[i]; // reference to the ith control point

//            cp_i[0] = cos(u);               // points on a unit circle
//            cp_i[1] = sin(u);

//            // if you whish, you can also define the z-coordinate, e.g.,
//            //cp_i[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
//        }

        //(*_bc)[2][0] += 1.50;
        //(*_bc)[2][1] -= 1.50;
        _bc->UpdateControlPointsByEnergyOptimization(_theta);

        if (!_bc->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the cubic Bezier curve's control polygon!");
        }

        _max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
        //_div_point_count          = 30;
        _img_bc = _bc->GenerateImage(_max_order_of_derivatives, _div_point_count);

        if (!_img_bc){
            throw Exception("Could not create the image of the cubic Bezier curve!");
        }

        if (!_img_bc->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the cubic Bezier curve's image!");
        }
    }

    void GLWidget::initQuarticPolinomialCurve(){
        _qpc = new (nothrow) QuarticPolinomialCurve3(DCoordinate3(-1,1,0), DCoordinate3(1,1,0), DCoordinate3(1,1,0), DCoordinate3(-1,1,0));

        if (!_qpc){
            throw Exception("Could not create the quartic polinomial curve!");
        }

//        GLdouble step = TWO_PI / 4.0;
//        for (GLuint i = 0; i < 4; i++){
//            GLdouble u = i * step;  // uniform subdivision point in the interval [0, 1]
//            DCoordinate3 &cp_i = (*_qpc)[i]; // reference to the ith control point

//            cp_i[0] = cos(u);               // points on a unit circle
//            cp_i[1] = sin(u);

//            // if you whish, you can also define the z-coordinate, e.g.,
//            cp_i[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
//        }

//        (*_qpc)[2][0] += 1.50;
//        (*_qpc)[2][1] -= 1.50;

        _qpc->UpdateControlPointsByEnergyOptimization(_theta);

        if (!_qpc->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the quartic polinomial curve's control polygon!");
        }

        _max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
       // _div_point_count          = 200;
        _img_qpc = _qpc->GenerateImage(_max_order_of_derivatives, _div_point_count);

        if (!_img_qpc){
            throw Exception("Could not create the image of the quartic polinomial curve!");
        }

        if (!_img_qpc->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the quartic polinomial curve's image!");
        }
    }

    void GLWidget::initSecondOrderTrigonometricCurve(){
        _stc = new (nothrow) SecondOrderTrigonometricCurve3(_shape_parameter, DCoordinate3(-1,1,0), DCoordinate3(1,1,0), DCoordinate3(1,1,0), DCoordinate3(-1,1,0));

        if (!_stc){
            throw Exception("Could not create the second order trigonometric curve!");
        }

//        GLdouble step = TWO_PI / 4.0;
//        for (GLuint i = 0; i < 4; i++){
//            GLdouble u = i * step;  // uniform subdivision point in the interval [0, 1]
//            DCoordinate3 &cp_i = (*_stc)[i]; // reference to the ith control point

//            cp_i[0] = cos(u);               // points on a unit circle
//            cp_i[1] = sin(u);

//            // if you whish, you can also define the z-coordinate, e.g.,
//            //cp_i[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
//        }

        _stc->UpdateControlPointsByEnergyOptimization(_theta);

        if (!_stc->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the second order trigonometric curve's control polygon!");
        }

        _max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
        //_div_point_count          = 50;
        _img_stc = _stc->GenerateImage(_max_order_of_derivatives, _div_point_count);

        if (!_img_stc){
            throw Exception("Could not create the image of the second order trigonometric curve!");
        }

        if (!_img_stc->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the second order trigonometric curve's image!");
        }
    }

    void GLWidget::initSecondOrderHyperbolicCurve(){
        _shc = new (nothrow) SecondOrderHyperbolicCurve3(_shape_parameter, DCoordinate3(-1,1,0), DCoordinate3(1,1,0), DCoordinate3(1,1,0), DCoordinate3(-1,1,0));

        if (!_shc){
            throw Exception("Could not create the second order hyperbolic curve!");
        }

//        GLdouble step = TWO_PI / 4.0;
//        for (GLuint i = 0; i < 4; i++){
//            GLdouble u = i * step;  // uniform subdivision point in the interval [0, 1]
//            DCoordinate3 &cp_i = (*_shc)[i]; // reference to the ith control point

//            cp_i[0] = cos(u);               // points on a unit circle
//            cp_i[1] = sin(u);

//            // if you whish, you can also define the z-coordinate, e.g.,
//            //cp_i[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
//        }

//        (*_shc)[2][0] += 1.50;
//        (*_shc)[2][1] -= 1.50;

        _shc->UpdateControlPointsByEnergyOptimization(_theta);

        if (!_shc->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the second order hyperbolic curve's control polygon!");
        }

        _max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
        //_div_point_count          = 50;
        _img_shc = _shc->GenerateImage(_max_order_of_derivatives, _div_point_count);

        if (!_img_shc){
            throw Exception("Could not create the image of the second order hyperbolic curve!");
        }

        if (!_img_shc->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the second order hyperbolic curve's image!");
        }
    }

    void GLWidget::initFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(){
        _fatc = new (nothrow) FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(_shape_parameter, DCoordinate3(-1,1,0), DCoordinate3(1,1,0), DCoordinate3(1,1,0), DCoordinate3(-1,1,0));

        if (!_fatc){
            throw Exception("Could not create the first order algebraic trig curve!");
        }

//        GLdouble step = TWO_PI / 4.0;
//        for (GLuint i = 0; i < 4; i++){
//            GLdouble u = i * step;  // uniform subdivision point in the interval [0, 1]
//            DCoordinate3 &cp_i = (*_fatc)[i]; // reference to the ith control point

//            cp_i[0] = cos(u);               // points on a unit circle
//            cp_i[1] = sin(u);

//            // if you whish, you can also define the z-coordinate, e.g.,
//            //cp_i[2] = -2.0 + 4.0 * (GLdouble)rand() / (GLdouble)RAND_MAX;
//        }

        _fatc->UpdateControlPointsByEnergyOptimization(_theta);

        if (!_fatc->UpdateVertexBufferObjectsOfData()){
            throw Exception("Could not update the vertex buffer object of the first order algebraic trig curve's control polygon!");
        }

        _max_order_of_derivatives = 2;   // increasing this number, you can also evaluate/render higher order derivatives
        //_div_point_count          = 50;
        _img_fatc = _fatc->GenerateImage(_max_order_of_derivatives, _div_point_count);

        if (!_img_fatc){
            throw Exception("Could not create the image of the first order algebraic trig curve!");
        }

        if (!_img_fatc->UpdateVertexBufferObjects()){
            throw Exception("Could not update the vertex buffer objects of the first order algebraic trig curve's image!");
        }
    }

    void GLWidget::paintGL()
    {
        // clears the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // stores/duplicates the original model view matrix
        glPushMatrix();

        // applying transformations
        glRotatef(_angle_x, 1.0, 0.0, 0.0);
        glRotatef(_angle_y, 0.0, 1.0, 0.0);
        glRotatef(_angle_z, 0.0, 0.0, 1.0);
        glTranslated(_trans_x, _trans_y, _trans_z);
        glScaled(_zoom, _zoom, _zoom);

        renderActualModel();
//        cout << "paintGL" << endl;
        glPopMatrix();
    }

    void GLWidget::renderActualModel(){

        emit setCursor(Qt::BusyCursor);
        //if (_actual_model){
                    if(_geometry > 1  && _actual_model){
                        if(_show_vertices) showVertices();
                        if(_show_normals) showNormals(_actual_model);
                        if(_show_tangents) showTangents(_actual_model);
                        if(_show_boundaries){
                            if(_geometry ==2)
                                showBoundaries(_actual_model);
                            else{
                                if(_geometry == 3 || _geometry == 4 || _geometry == 5){
                                    showBoundaryCurves(_actual_model);
                                }
                            }
                        }
                        if(_show_boundaries_normals) {
                            if(_geometry == 2 || _geometry == 3){
                                showC0BoundariesNormals(_actual_model);
                            }else{
                                if(_geometry == 4 || _geometry == 5){
                                    showG1BoundariesNormals(_actual_model);
                                }
                            }
                        }
                    }

                    if(_poligon_mode && _geometry == 1) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


                    if(_geometry == 0 || _geometry == 4){
                        glDisable(GL_LIGHTING);
                        _two_sided_lighting.Disable();
                        _reflection_lines.Disable();
                        _toonify.Disable();
                    }else{
                        glEnable(GL_LIGHTING);
                        switch (_shader) {
                        case 0: _two_sided_lighting.Enable(); break;
                        case 1: _reflection_lines.Enable(); break;
                        case 2: _toonify.Enable(); break;
                        }

                        switch (_material) {
                        case 0: MatFBBrass.Apply(); break;
                        case 1: glDisable(GL_LIGHTING); break;
                        case 2: MatFBGold.Apply(); break;
                        case 3: MatFBSilver.Apply(); break;
                        case 4: MatFBEmerald.Apply(); break;
                        case 5: MatFBPearl.Apply(); break;
                        case 6: MatFBRuby.Apply(); break;
                        case 7: MatFBTurquoise.Apply(); break;
                        }
                    }

        //            glPushMatrix();
                        if(_show_geometry){
                            switch (_geometry) {
                            case 0:{//curves
                                switch (_basis_function) {
                                case 0: renderSecondOrderTrigonometricCurve();
                                    break;
                                case 1: renderQuarticPolinomialCurve();
                                    break;
                                case 2: renderFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3();
                                    break;
                                case 3: renderBezierCurve();
                                    break;
                                }

                                break;
                            }
                            case 1: {//triangles
                                switch (_basis_function) {
                                case 0: renderSecondOrderTrigonometricTriangle();
                                    break;
                                case 1: renderQuarticPolinomialTriangle();
                                    break;
                                case 2: renderAlgebraicTrigonometricTriangle();
                                    break;
                                case 3: renderBezierTriangle();
                                    break;
                                }
                            break;
                            }
                            case 2: //faces
        //                        _actual_model_mesh.Render();
                                emit statusMessageChanged("Rendering the faces...");
                                _actual_model->RenderFaces();
                                break;
                            case 3: //C0 surf
                                emit statusMessageChanged("Rendering the C0 meshes...");
                                _actual_model->RenderC0Meshes();
                                break;
                            case 4: //point cloud
                                emit statusMessageChanged("Rendering G1 meshes as point cloud...");
                                _actual_model->RenderG1MeshesAsPointCloud(static_cast<GLuint>(_vertex_number), _div_point_count);
                                break;
                            case 5: //G1 surf
                                emit statusMessageChanged("Rendering G1 meshes...");
                                _actual_model->RenderG1Meshes();
                                break;
                            }
                        }

        //            glPopMatrix();
//                }
        emit setCursor(Qt::ArrowCursor);
        emit statusMessageChanged("Done");
    }

    void GLWidget::renderFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(){
        glDisable(GL_LIGHTING);

        if (_fatc){
            glColor3f(0.5f, 0.5f, 0.5f);
            _fatc->RenderData(GL_LINE_LOOP);  // control polygon

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _fatc->RenderData(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_fatc){
            glColor3f(1.0f, 0.0f, 0.0f);
            glLineWidth(2.0);
            _img_fatc->RenderDerivatives(0,  GL_LINE_LOOP ); // 0 = zeroth order derivatives = curve points

            glColor3f(0.0f, 1.0f, 0.0f);
            glLineWidth(1.0);
            _img_fatc->RenderDerivatives(1, GL_LINES);      // 1 = first order derivatives = tangent vectors

            glColor3f(0.1f, 0.5f, 0.9f);
            _img_fatc->RenderDerivatives(2, GL_LINES);      // 2 = second order derivatives = acceleration vectors
        }
    }

    void GLWidget::renderSecondOrderHyperbolicCurve(){
        glDisable(GL_LIGHTING);

        if (_shc){
            glColor3f(0.5f, 0.5f, 0.5f);
            _shc->RenderData(GL_LINE_LOOP);  // control polygon

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _shc->RenderData(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_shc){
            glColor3f(1.0f, 0.0f, 0.0f);
            _img_shc->RenderDerivatives(0,  GL_LINE_LOOP ); // 0 = zeroth order derivatives = curve points

            glColor3f(0.0f, 1.0f, 0.0f);
            _img_shc->RenderDerivatives(1, GL_LINES);      // 1 = first order derivatives = tangent vectors

            glColor3f(0.1f, 0.5f, 0.9f);
            _img_shc->RenderDerivatives(2, GL_LINES);      // 2 = second order derivatives = acceleration vectors
        }
    }

    void GLWidget::renderSecondOrderTrigonometricCurve(){
        glDisable(GL_LIGHTING);

        if (_stc){
            glColor3f(0.5f, 0.5f, 0.5f);
            _stc->RenderData(GL_LINE_LOOP);  // control polygon

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _stc->RenderData(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_stc){
            glColor3f(1.0f, 0.0f, 0.0f);
            glLineWidth(2.0);
            _img_stc->RenderDerivatives(0,  GL_LINE_LOOP ); // 0 = zeroth order derivatives = curve points

            glColor3f(0.0f, 1.0f, 0.0f);
            glLineWidth(1.0);
            _img_stc->RenderDerivatives(1, GL_LINES);      // 1 = first order derivatives = tangent vectors

            glColor3f(0.1f, 0.5f, 0.9f);
            _img_stc->RenderDerivatives(2, GL_LINES);      // 2 = second order derivatives = acceleration vectors
        }
    }

    void GLWidget::renderQuarticPolinomialCurve(){
        glDisable(GL_LIGHTING);

        if (_qpc){
            glColor3f(0.5f, 0.5f, 0.5f);
            _qpc->RenderData(GL_LINE_LOOP);  // control polygon

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _qpc->RenderData(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_qpc){
            glColor3f(1.0f, 0.0f, 0.0f);
            glLineWidth(2.0);
            _img_qpc->RenderDerivatives(0,  GL_LINE_LOOP ); // 0 = zeroth order derivatives = curve points

            glColor3f(0.0f, 1.0f, 0.0f);
            glLineWidth(1.0);
            _img_qpc->RenderDerivatives(1, GL_LINES);      // 1 = first order derivatives = tangent vectors

            glColor3f(0.1f, 0.5f, 0.9f);
            _img_qpc->RenderDerivatives(2, GL_LINES);      // 2 = second order derivatives = acceleration vectors
        }
    }

    void GLWidget::renderBezierCurve(){
        glDisable(GL_LIGHTING);

        if (_bc){
            glColor3f(0.5f, 0.5f, 0.5f);
            _bc->RenderData(GL_LINE_LOOP);  // control polygon

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _bc->RenderData(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_bc){
            glColor3f(1.0f, 0.0f, 0.0f);
            glLineWidth(2.0);
            _img_bc->RenderDerivatives(0,  GL_LINE_STRIP ); // 0 = zeroth order derivatives = curve points

            glLineWidth(1.0);
            glColor3f(0.0f, 1.0f, 0.0f);
            _img_bc->RenderDerivatives(1, GL_LINES);      // 1 = first order derivatives = tangent vectors

            glColor3f(0.1f, 0.5f, 0.9f);
            _img_bc->RenderDerivatives(2, GL_LINES);      // 2 = second order derivatives = acceleration vectors
        }
    }

    void GLWidget::renderBezierTriangle(){

        if (_bt){
            glDisable(GL_LIGHTING);
            _reflection_lines.Disable();
            _two_sided_lighting.Disable();
            _toonify.Disable();
            glColor3f(0.5f, 0.5f, 0.5f);
            _bt->RenderControlNet(GL_TRIANGLES);  // control net
            //cout << _bt-> _net(2 , 1) << endl;

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _bt->RenderControlNet(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_bt){
            glEnable(GL_LIGHTING);
            switch (_shader) {
            case 0: _two_sided_lighting.Enable(); break;
            case 1: _reflection_lines.Enable(); break;
            case 2: _toonify.Enable(); break;
            }
            glColor3f(1.0f, 0.0f, 0.0f);
            if(_poligon_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            switch (_material) {
            case 0: MatFBBrass.Apply(); break;
            case 1: glDisable(GL_LIGHTING); break;
            case 2: MatFBGold.Apply(); break;
            case 3: MatFBSilver.Apply(); break;
            case 4: MatFBEmerald.Apply(); break;
            case 5: MatFBPearl.Apply(); break;
            case 6: MatFBRuby.Apply(); break;
            case 7: MatFBTurquoise.Apply(); break;
            }
            _img_bt->Render();
        }
    }

    void GLWidget::renderQuarticPolinomialTriangle(){
        if (_qt){
            glDisable(GL_LIGHTING);
            _reflection_lines.Disable();
            _two_sided_lighting.Disable();
            _toonify.Disable();
            glColor3f(0.5f, 0.5f, 0.5f);
            _qt->RenderControlNet(GL_TRIANGLES);  // control net

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _qt->RenderControlNet(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_qt){
            glEnable(GL_LIGHTING);
            switch (_shader) {
            case 0: _two_sided_lighting.Enable(); break;
            case 1: _reflection_lines.Enable(); break;
            case 2: _toonify.Enable(); break;
            }
            glColor3f(1.0f, 0.0f, 0.0f);
            if(_poligon_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            switch (_material) {
            case 0: MatFBBrass.Apply(); break;
            case 1: break;
            case 2: MatFBGold.Apply(); break;
            case 3: MatFBSilver.Apply(); break;
            case 4: MatFBEmerald.Apply(); break;
            case 5: MatFBPearl.Apply(); break;
            case 6: MatFBRuby.Apply(); break;
            case 7: MatFBTurquoise.Apply(); break;
            }
            _img_qt->Render();
        }
    }

    void GLWidget::renderAlgebraicTrigonometricTriangle(){
        if (_att){
            glDisable(GL_LIGHTING);
            _reflection_lines.Disable();
            _two_sided_lighting.Disable();
            _toonify.Disable();
            glColor3f(0.5f, 0.5f, 0.5f);
            _att->RenderControlNet(GL_TRIANGLES);  // control net

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _att->RenderControlNet(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_att){
            glEnable(GL_LIGHTING);
            switch (_shader) {
            case 0: _two_sided_lighting.Enable(); break;
            case 1: _reflection_lines.Enable(); break;
            case 2: _toonify.Enable(); break;
            }
            glColor3f(1.0f, 0.0f, 0.0f);
            if(_poligon_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            switch (_material) {
            case 0: MatFBBrass.Apply(); break;
            case 1: break;
            case 2: MatFBGold.Apply(); break;
            case 3: MatFBSilver.Apply(); break;
            case 4: MatFBEmerald.Apply(); break;
            case 5: MatFBPearl.Apply(); break;
            case 6: MatFBRuby.Apply(); break;
            case 7: MatFBTurquoise.Apply(); break;
            }
            _img_att->Render();
        }
    }

    void GLWidget::renderSecondOrderHyperbolicTriangle(){
        if (_sht){
            glDisable(GL_LIGHTING);
            _reflection_lines.Disable();
            _two_sided_lighting.Disable();
            _toonify.Disable();
            glColor3f(0.5f, 0.5f, 0.5f);
            _sht->RenderControlNet(GL_TRIANGLES);  // control net

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _sht->RenderControlNet(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_sht){
            glEnable(GL_LIGHTING);
            switch (_shader) {
            case 0: _two_sided_lighting.Enable(); break;
            case 1: _reflection_lines.Enable(); break;
            case 2: _toonify.Enable(); break;
            }
            glColor3f(1.0f, 0.0f, 0.0f);
            if(_poligon_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            switch (_material) {
            case 0: MatFBBrass.Apply(); break;
            case 1:  break;
            case 2: MatFBGold.Apply(); break;
            case 3: MatFBSilver.Apply(); break;
            case 4: MatFBEmerald.Apply(); break;
            case 5: MatFBPearl.Apply(); break;
            case 6: MatFBRuby.Apply(); break;
            case 7: MatFBTurquoise.Apply(); break;
            }
            _img_sht->Render();
        }
    }

    void GLWidget::renderSecondOrderTrigonometricTriangle(){
        if (_stt){
            glDisable(GL_LIGHTING);
            _reflection_lines.Disable();
            _two_sided_lighting.Disable();
            _toonify.Disable();
            glColor3f(0.5f, 0.5f, 0.5f);
            _stt->RenderControlNet(GL_TRIANGLES);  // control net

            glPointSize(5.0);
            glColor3f(1.0f, 0.0f, 0.0f);
            _stt->RenderControlNet(GL_POINTS);     // control points
            glPointSize(1.0);
        }

        if (_img_stt){
//            glColor3d(1.0, 0.8, 0.0);
//            _img_stt->RenderNormals();
            glEnable(GL_LIGHTING);
            switch (_shader) {
            case 0: _two_sided_lighting.Enable(); break;
            case 1: _reflection_lines.Enable(); break;
            case 2: _toonify.Enable(); break;
            }
            glColor3f(1.0f, 0.0f, 0.0f);
            if(_poligon_mode) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            switch (_material) {
            case 0: MatFBBrass.Apply(); break;
            case 1: break;
            case 2: MatFBGold.Apply(); break;
            case 3: MatFBSilver.Apply(); break;
            case 4: MatFBEmerald.Apply(); break;
            case 5: MatFBPearl.Apply(); break;
            case 6: MatFBRuby.Apply(); break;
            case 7: MatFBTurquoise.Apply(); break;
            }
            _img_stt->Render();
        }
    }


    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = (float)w / (float)h;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        updateGL();
    }

    void GLWidget::_animate(){
        GLfloat *vertex = _mouse.MapVertexBuffer(GL_READ_WRITE);
        GLfloat *normal = _mouse.MapNormalBuffer(GL_READ_ONLY);

        _angle += DEG_TO_RADIAN;
        if(_angle >= TWO_PI) {
            _angle -= TWO_PI;
        }

        GLfloat scale = sin(_angle) / 3000.0;
        for(GLuint i = 0; i < _mouse.VertexCount(); ++i){
            for(GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex, ++normal){
                *vertex += scale * (*normal);
            }
        }

        _mouse.UnmapVertexBuffer();
        _mouse.UnmapNormalBuffer();

        updateGL();
    }

    void GLWidget::showVertices(){
        emit statusMessageChanged("Rendering vertices...");
        std::vector<DCoordinate3> vertices = _actual_model->GetVertexes();
        for (GLuint i = 0; i < vertices.size(); ++i) {
            cout<<i<<endl;
            glPushMatrix();
                glTranslated(vertices[i].x(), vertices[i].y(), vertices[i].z());
                glScaled(_vertices_size, _vertices_size, _vertices_size);
                MatFBSilver.Apply();
                _sphere.Render();
            glPopMatrix();
        }
    }

    void GLWidget::showNormals(TriangulatedHalfEdgeDataStructure3 *mesh){
        emit statusMessageChanged("Rendering normals...");
        _two_sided_lighting.Disable();
        _reflection_lines.Disable();
        _toonify.Disable();
        glDisable(GL_LIGHTING);
        glColor3f(0.2f, 0.8f, 0.2f);
        mesh->RenderVertexNormals(_normals_size);
        glEnable(GL_LIGHTING);
    }

    void GLWidget::showTangents(TriangulatedHalfEdgeDataStructure3 *mesh){
        emit statusMessageChanged("Rendering tangents...");
        _two_sided_lighting.Disable();
        _reflection_lines.Disable();
        _toonify.Disable();
        glDisable(GL_LIGHTING);
        glColor3f(0.8f, 0.2f, 0.2f);
        mesh->RenderApproximatedUnitTangents(_tangents_size);
        glEnable(GL_LIGHTING);
    }

    void GLWidget::showBoundaries(TriangulatedHalfEdgeDataStructure3 *mesh){
        emit statusMessageChanged("Rendering boundary lines...");
        _two_sided_lighting.Disable();
        _reflection_lines.Disable();
        _toonify.Disable();
        glDisable(GL_LIGHTING);
        glColor3f(0.4f, 0.2f, 0.6f);
        mesh->RenderHalfEdges();
        glEnable(GL_LIGHTING);
    }

    void GLWidget::showBoundaryCurves(TriangulatedHalfEdgeDataStructure3 *mesh){
        emit statusMessageChanged("Rendering boundary curves...");
        _two_sided_lighting.Disable();
        _reflection_lines.Disable();
        _toonify.Disable();
        glDisable(GL_LIGHTING);
        glColor3f(0.4f, 0.2f, 0.6f);
        mesh->RenderCurves();
        glEnable(GL_LIGHTING);
    }

    void GLWidget::showC0BoundariesNormals(TriangulatedHalfEdgeDataStructure3 *mesh){
        emit statusMessageChanged("Rendering C0 boundary normals...");
        _two_sided_lighting.Disable();
        _reflection_lines.Disable();
        _toonify.Disable();
        glDisable(GL_LIGHTING);
        glColor3f(0.4f, 0.2f, 0.6f);
        mesh->RenderC0BoundaryNormals(_div_point_count, _boundaries_normals_size);
        glEnable(GL_LIGHTING);
    }

    void GLWidget::showG1BoundariesNormals(TriangulatedHalfEdgeDataStructure3 *mesh){
        emit statusMessageChanged("Rendering G1 boundary normals...");
        _two_sided_lighting.Disable();
        _reflection_lines.Disable();
        _toonify.Disable();
        glDisable(GL_LIGHTING);
        glColor3f(0.4f, 0.2f, 0.6f);
        mesh->RenderG1BoundaryNormals(_div_point_count, _boundaries_normals_size);
        glEnable(GL_LIGHTING);
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            updateGL();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            updateGL();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            updateGL();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            updateGL();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            updateGL();
        }
    }

    void GLWidget::elephant_v102_f200_checked(bool value){
        _actual_file_name = "Models/elephant_v102_f200.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::m2_v3495_f666_checked(bool value){
        _actual_file_name = "Models/m2_v3495_f666.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::spot_v122_f240_checked(bool value){
        _actual_file_name = "Models/spot_v122_f240.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::bunny_v113_f222_checked(bool value){
        _actual_file_name = "Models/bunny_v113_f222.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::bunny_v239_f474_checked(bool value){
        _actual_file_name = "Models/bunny_v239_f474.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::horse_v341_f678_checked(bool value){
        _actual_file_name = "Models/horse_v341_f678.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::m195_v2472_f4809_checked(bool value){
        _actual_file_name = "Models/m195_v2472_f4809.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::angel_v1352_f2706_checked(bool value){
        _actual_file_name = "Models/angel_v1352_f2706.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::dragon_v1386_f2772_checked(bool value){
        _actual_file_name = "Models/dragon_v1386_f2772.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::icosahedron_v12_f20_checked(bool value){
        _actual_file_name = "Models/icosahedron_v12_f20.off";
        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }
        initActualModel(_actual_file_name);
        updateGL();
    }

    void GLWidget::set_eps_1(double value)
    {
        if (_epsilon[1] != value)
        {
            _epsilon[1] = value;

            if(_actual_model){
                delete _actual_model;
            }

            initActualModel(_actual_file_name);
            refreshTriangles();
            updateGL();
        }
    }

    void GLWidget::set_eps_2(double value)
    {
        if (_epsilon[2] != value)
        {
            _epsilon[2] = value;

            if(_actual_model){
                delete _actual_model;
            }

            initActualModel(_actual_file_name);
            refreshTriangles();
            updateGL();
        }
    }

    void GLWidget::set_theta_1(double value)
    {
        if (_theta[1] != value)
        {
            _theta[1] = value;

            if(_actual_model){
                delete _actual_model;
            }

            initActualModel(_actual_file_name);

            refreshCurves();

            updateGL();
        }
    }

    void GLWidget::set_theta_2(double value)
    {
        if (_theta[2] != value)
        {
            _theta[2] = value;


            if(_actual_model){
                delete _actual_model;
                _actual_model = nullptr;
            }

            initActualModel(_actual_file_name);

            refreshCurves();
            updateGL();
        }
    }

    void GLWidget::set_div_point_count(int value){
        if(_div_point_count != static_cast<GLuint>(value)){
            _div_point_count = static_cast<GLuint>(value);

            if(_actual_model){
                delete _actual_model;
                _actual_model = nullptr;
            }

            initActualModel(_actual_file_name);

            refreshTriangles();
            refreshCurves();
            updateGL();
        }
    }

    void GLWidget::set_vertex_number(int value){
        if(_vertex_number != value){
            _vertex_number = value;

            updateGL();
        }
    }

    void GLWidget::set_basis_function(int value)
    {
        if (_basis_function != value)
        {
            _basis_function = value;
            if(_actual_model){
                delete _actual_model;
                _actual_model = nullptr;
            }
            initActualModel(_actual_file_name);
            updateGL();
        }
    }

    void GLWidget::set_weight_function(int value)
    {
        if (_weight_function != value)
        {
            _weight_function = value;
            if(_actual_model){
                delete _actual_model;
                _actual_model = nullptr;
            }
            initActualModel(_actual_file_name);
            updateGL();
        }
    }

    void GLWidget::set_geometry(int value)
    {
        if (_geometry != value)
        {
            _geometry = value;
            updateGL();
        }
    }

    void GLWidget::set_selected_shape(int value)
    {
        if (_selected_shape != value)
        {
            _selected_shape = value;
            updateGL();
        }
    }

    void GLWidget::set_shader(int value)
    {
        if (_shader != value)
        {
            _shader = value;
            updateGL();
        }
    }

    void GLWidget::set_material(int value)
    {
        //cout<<"Material:" <<value<<endl;
        if (_material != value)
        {
            _material = value;
            updateGL();
        }
    }

    void GLWidget::set_poligon_mode(int value){
        if(value == 2){
            _poligon_mode = 1;
        }else{
            _poligon_mode = 0;
        }
        updateGL();
    }

    void GLWidget::set_vertices_check(int value){
        if(value == 2){
            _show_vertices = 1;
        }else{
            _show_vertices = 0;
        }
        updateGL();
    }

    void GLWidget::set_vertices_size(double value){
        if(_vertices_size != value){
            _vertices_size = value;
        }
        if(_show_vertices == 1){//update just if the show vertices checkbox is checked
            updateGL();
        }
    }

    void GLWidget::set_normals_check(int value){
        if(value == 2){
            _show_normals = 1;
        }else{
            _show_normals = 0;
        }
        updateGL();
    }

    void GLWidget::set_normals_size(double value){
        cout << value << endl;
        if(_normals_size != value){
            _normals_size = value;
        }
        if(_show_normals == 1){//update just if the show vertices checkbox is checked
            cout << "update" << endl;
            updateGL();
        }
    }

    void GLWidget::set_tangents_size(double value){
        if(_tangents_size != value){
            _tangents_size = value;
        }
        if(_show_tangents == 1){//update just if the show vertices checkbox is checked
            updateGL();
        }
    }

    void GLWidget::set_tangents_check(int value){
        if(value == 2){
            _show_tangents = 1;
        }else{
            _show_tangents = 0;
        }
        updateGL();
    }

    void GLWidget::set_geometry_check(int value){
        if(value == 2){
            _show_geometry = 1;
        }else{
            _show_geometry = 0;
        }
        updateGL();
    }

    void GLWidget::set_boundaries_normal_size(double value){
        if(_boundaries_normals_size != value){
            _boundaries_normals_size = value;
        }
        if(_show_boundaries_normals == 1){//update just if the show vertices checkbox is checked
            updateGL();
        }
    }

    void GLWidget::set_boundaries_normal_check(int value){
        if(value == 2){
            _show_boundaries_normals = 1;
        }else{
            _show_boundaries_normals = 0;
        }
        updateGL();
    }

    void GLWidget::set_boundaries_check(int value){
        if(value == 2){
            _show_boundaries = 1;
        }else{
            _show_boundaries = 0;
        }
        updateGL();
    }

    void GLWidget::set_shape_parameter(double value)
    {
        if (_shape_parameter != value)
        {

            _shape_parameter = value;
            if(_actual_model){
                delete _actual_model;
            }

            initActualModel(_actual_file_name);

            refreshTriangles();
            refreshCurves();
            updateGL();
        }
    }

    void GLWidget::refreshTriangles(){
        if (_stt){
            delete _stt;
            _stt = nullptr;
        }

        if (_img_stt){
            delete _img_stt;
            _img_stt = nullptr;
        }

        if (_sht){
            delete _sht;
            _sht = nullptr;
        }

        if (_img_sht){
            delete _img_sht;
            _img_sht = nullptr;
        }

        if (_att){
            delete _att;
            _att = nullptr;
        }

        if (_img_att){
            delete _img_att;
            _img_att = nullptr;
        }

        if (_bt){
            delete _bt;
            _bt = nullptr;
        }

        if (_img_bt){
            delete _img_bt;
            _img_bt = nullptr;
        }

        if (_qt){
            delete _qt;
            _qt = nullptr;
        }

        if (_img_qt){
            delete _img_qt;
            _img_qt = nullptr;
        }

        initBezierTriangle();
        initQuarticPolinomialTriangle();
        initSecondOrderTrigonometricTriangle();
        initAlgebraicTrigonometricTriangle();
    }

    void GLWidget::refreshCurves(){
        if (_bc){
            delete _bc;
            _bc = nullptr;
        }

        if (_img_bc){
            delete _img_bc;
            _img_bc = nullptr;
        }

        if (_qpc){
            delete _qpc;
            _qpc = nullptr;
        }

        if (_img_qpc){
            delete _img_qpc;
            _img_qpc = nullptr;
        }

        if (_fatc){
            delete _fatc;
            _fatc = nullptr;
        }

        if (_img_fatc){
            delete _img_fatc;
            _img_fatc = nullptr;
        }


        if (_stc){
            delete _stc;
            _stc = nullptr;
        }

        if (_img_stc){
            delete _img_stc;
            _img_stc = nullptr;
        }

        initBezierCurve();
        initQuarticPolinomialCurve();
        initSecondOrderTrigonometricCurve();
        initFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3();
    }

    GLWidget::~GLWidget(){

//        for(GLuint i=0; i<_pc.GetColumnCount(); i++)
//        {

//            if(_pc[i]){
//                delete _pc[i];
//                _pc[i]=nullptr;
//            }

//            if(_img_pc[i]){
//                delete _img_pc[i];
//                _img_pc[i] = nullptr;
//            }
//        }

//        if (_cc){
//            delete _cc;
//            _cc = nullptr;
//        }

//        if (_img_cc){
//            delete _img_cc;
//            _img_cc = nullptr;
//        }

        if (_bc){
            delete _bc;
            _bc = nullptr;
        }

        if (_img_bc){
            delete _img_bc;
            _img_bc = nullptr;
        }

        if (_qpc){
            delete _qpc;
            _qpc = nullptr;
        }

        if (_img_qpc){
            delete _img_qpc;
            _img_qpc = nullptr;
        }

        if (_fatc){
            delete _fatc;
            _fatc = nullptr;
        }

        if (_img_fatc){
            delete _img_fatc;
            _img_fatc = nullptr;
        }

//        if (_shc){
//            delete _shc;
//            _shc = nullptr;
//        }

//        if (_img_shc){
//            delete _img_shc;
//            _img_shc = nullptr;
//        }

        if (_stc){
            delete _stc;
            _stc = nullptr;
        }

        if (_img_stc){
            delete _img_stc;
            _img_stc = nullptr;
        }

        if (_stt){
            delete _stt;
            _stt = nullptr;
        }

        if (_img_stt){
            delete _img_stt;
            _img_stt = nullptr;
        }

//        if (_sht){
//            delete _sht;
//            _sht = nullptr;
//        }

//        if (_img_sht){
//            delete _img_sht;
//            _img_sht = nullptr;
//        }

        if (_att){
            delete _att;
            _att = nullptr;
        }

        if (_img_att){
            delete _img_att;
            _img_att = nullptr;
        }

        if (_bt){
            delete _bt;
            _bt = nullptr;
        }

        if (_img_bt){
            delete _img_bt;
            _img_bt = nullptr;
        }

        if (_qt){
            delete _qt;
            _qt = nullptr;
        }

        if (_img_qt){
            delete _img_qt;
            _img_qt = nullptr;
        }

//        if(_dl){
//            delete _dl;
//            _dl = nullptr;
//        }

        if(_actual_model){
            delete _actual_model;
            _actual_model = nullptr;
        }


    }
}
