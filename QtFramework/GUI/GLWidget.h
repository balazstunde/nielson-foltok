#pragma once

#include <GL/glew.h>
#include <QGLWidget>
#include <QGLFormat>
#include "../Core/Matrices.h"
#include "../Core/GenericCurves3.h"
#include "../Cyclic/CyclicCurves3.h"
#include "../LinearCombinationExamples/CubicBezierCurve3.h"
#include "../LinearCombinationExamples/QuarticPolinomialCurve3.h"
#include "../LinearCombinationExamples/SecondOrderTrigonometricCurve3.h"
#include "../LinearCombinationExamples/SecondOrderHyperbolicCurve3.h"
#include "../LinearCombinationExamples/AlgebraicTrigonometricCurve3.h"
#include "../TriangularSurfaceExamples/QuarticPolinomialTriangle3.h"
#include "../TriangularSurfaceExamples/CubicBezierTriangle3.h"
#include "../TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.h"
#include "../TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.h"
#include "../TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.h"
#include "../Parametric/ParametricCurves3.h"
#include "../Parametric/ParametricSurfaces3.h"
#include "../Core/TriangulatedMeshes3.h"
#include "../Core/Lights.h"
#include "../Core/ShaderPrograms.h"
#include "../SpecificStructures/TriangulatedHalfEdgeDataStructure3.h"

namespace cagd
{
    class GLWidget: public QGLWidget
    {
        Q_OBJECT

    private:

        QTimer *_timer;
        GLfloat _angle;
        // variables defining the projection matrix
        float       _aspect;            // aspect ratio of the rendering window
        float       _fovy;              // field of view in direction y
        float       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        float       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;
        RowMatrix<GLdouble>  _theta;
        RowMatrix<GLdouble>  _epsilon;
        double    _shape_parameter = 1.0;
        int       _basis_function = 0;
        int       _weight_function = 0;
        int       _triangle = 1;
        int       _curve = 0;
        int       _shader = 0;
        int       _poligon_mode = 0;
        int       _material = 0;
        int       _selected_shape = 0;
        int       _shape_curve_network = 0;
        int       _shape_C0= 0;
        int       _show_vertices = 0;
        double    _vertices_size = 0.04;
        int       _show_normals = 0;
        double    _normals_size = 0.09;
        int       _show_tangents = 0;
        double    _tangents_size = 0.09;
        int       _show_boundaries = 0;
        int       _show_boundaries_normals = 0;
        double    _boundaries_normals_size = 0.09;
        int       _show_geometry = 1;
        int       _geometry = 0;
        string    _actual_file_name = "Models/elephant_v102_f200.off";
        int       _vertex_number = 60;

        // your other declarations
        GLdouble                                 _u_min, _u_max;   // endpoints of the definition domain
        RowMatrix<ParametricCurve3::Derivative> _derivatives;     // matrix of vector valued function pointers

        RowMatrix<ParametricCurve3*>            _pc = NULL;             // pointer to some traditional parametric curve

        GLuint                                   _div_point_count = 10; // number of subdivision points
        RowMatrix<GenericCurve3*>                _img_pc = NULL;         // pointer to the image of the parametric curve

        GLuint                                   _index_of_parametric_curve = 2; /*
                                                                                   0. spiral on cone
                                                                                   1. epicycloid
                                                                                   2. lissajous
                                                                                   3. nephroid
                                                                                   4. involute of a circle
                                                                                   5. Hypocycloid
                                                                                   */

        GLuint                                   _n;
        GLuint                                   _max_order_of_derivatives;
        GLuint                                   _usage_flag;
        CyclicCurve3                             *_cc = nullptr;
        GenericCurve3                            *_img_cc = nullptr;
        CubicBezierCurve3                        *_bc = nullptr;
        GenericCurve3                            *_img_bc = nullptr;
        QuarticPolinomialCurve3                  *_qpc = nullptr;
        GenericCurve3                            *_img_qpc = nullptr;
        SecondOrderTrigonometricCurve3           *_stc = nullptr;
        GenericCurve3                            *_img_stc = nullptr;
        SecondOrderHyperbolicCurve3              *_shc = nullptr;
        GenericCurve3                            *_img_shc = nullptr;
        FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3 *_fatc = nullptr;
        TriangulatedMesh3                        *_img_bt = nullptr;
        CubicBezierTriangle3                     *_bt = nullptr;
        TriangulatedMesh3                        *_img_qt = nullptr;
        QuarticPolinomialTriangle3               *_qt = nullptr;
        TriangulatedMesh3                        *_img_att = nullptr;
        AlgebraicTrigonometricTriangle3          *_att = nullptr;
        TriangulatedMesh3                        *_img_sht = nullptr;
        SecondOrderHyperbolicTriangle3           *_sht = nullptr;
        TriangulatedMesh3                        *_img_stt = nullptr;
        SecondOrderTrigonometricTriangle3        *_stt = nullptr;
        GenericCurve3                            *_img_fatc = nullptr;
        CyclicCurve3*                             _ipcc;
        GenericCurve3*                            _img_ipcc;
        ColumnMatrix<DCoordinate3>                _data_points_to_interpolate;
        TriangulatedHalfEdgeDataStructure3       *_actual_model = nullptr;
        TriangulatedMesh3                        _actual_model_mesh;

        GLuint                                   _number_of_homework = 6; /*
                                                                            0.   triangle
                                                                            1.   parametric curves
                                                                            2.   cyclic curve
                                                                            3.   interpolating cyclic curve
                                                                            4.   mouse
                                                                            5.   parametric surfaces
                                                                            6.   cubic Bezier curve
                                                                            7.   quartic polinomial curve
                                                                            8.   second order trigonometrical curve
                                                                            9.   second order hyperbolic curve
                                                                            10.  first order algebraic trigonometric curve
                                                                        */
        GLuint                                  _off_number = 0; /*
                                                                         0. mouse
                                                                         1. sphere
                                                                         2. elefant
                                                                         */
        TriangulatedMesh3                       _mouse;
        TriangulatedMesh3                       _sphere;
        TriangulatedMesh3                       _elephant;
        RowMatrix<ParametricSurface3*>          _ps = NULL;             // pointer to some traditional parametric curve
        RowMatrix<TriangulatedMesh3*>           _img_ps = NULL;         // pointer to the image of the parametric curve
        GLdouble                                _v_min, _v_max;   // endpoints of the definition domain
        GLuint                                  _u_div_point_count, _v_div_point_count;
        TriangularMatrix<ParametricSurface3::PartialDerivative> _partial_derivatives;     // matrix of vector valued function pointers
        GLuint                                   _index_of_parametric_surface = 2; /*
                                                                                   0. sphere
                                                                                   1. torus
                                                                                   2. elliptic_paraboloid
                                                                                   3. cylinder
                                                                                   4. hyperbolic_paraboloid    */

        DirectionalLight                         *_dl = nullptr;
        ShaderProgram                            _two_sided_lighting, _reflection_lines, _toonify;

        void    initParametricCurves();
        void    renderParametricCurves();
        void    initCyclicCurve();
        void    renderCyclicCurve();
        void    initInterpolatingCyclicCurve();
        void    renderInterpolatingCyclicCurve();
        void    initActualModel(string file_name);
        void    initHomeworkOffs();
        void    renderHomeworkOffs();
        void    initParametricSurfaces();
        void    renderParametricSurfaces();
        void    renderTriangle();

        void    initBezierCurve();
        void    renderBezierCurve();
        void    initSecondOrderTrigonometricCurve();
        void    renderSecondOrderTrigonometricCurve();
        void    initQuarticPolinomialCurve();
        void    renderQuarticPolinomialCurve();
        void    initSecondOrderHyperbolicCurve();
        void    renderSecondOrderHyperbolicCurve();
        void    initFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3();
        void    renderFirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3();

        void    initBezierTriangle();
        void    renderBezierTriangle();
        void    initSecondOrderTrigonometricTriangle();
        void    renderSecondOrderTrigonometricTriangle();
        void    initQuarticPolinomialTriangle();
        void    renderQuarticPolinomialTriangle();
        void    initSecondOrderHyperbolicTriangle();
        void    renderSecondOrderHyperbolicTriangle();
        void    initAlgebraicTrigonometricTriangle();
        void    renderAlgebraicTrigonometricTriangle();
        void    renderActualModel();


        void    showVertices();
        void    showNormals(TriangulatedHalfEdgeDataStructure3 *mesh);
        void    showTangents(TriangulatedHalfEdgeDataStructure3 *mesh);
        void    showBoundaries(TriangulatedHalfEdgeDataStructure3 *mesh);
        void    showBoundaryCurves(TriangulatedHalfEdgeDataStructure3 *mesh);
        void    showC0BoundariesNormals(TriangulatedHalfEdgeDataStructure3 *mesh);
        void    showG1BoundariesNormals(TriangulatedHalfEdgeDataStructure3 *mesh);
        void    initSphere();

        void    refreshTriangles();
        void    refreshCurves();

        // loads the geometry (i.e. the array of vertices and faces) stored in an OFF file
        // at the same time calculates the unit normal vectors associated with vertices
        GLboolean LoadFromOFF(const std::string& file_name);

    private slots:
        void _animate();

    public:
        // desctructor
        ~GLWidget();

    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = nullptr, const QGLFormat& format = QGL::Rgba | QGL::DepthBuffer | QGL::DoubleBuffer);

        // redeclared virtual functions
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);

    public slots:
        // public event handling methods/slots
        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);

        void set_theta_1(double value);
        void set_theta_2(double value);

        void set_basis_function(int value);
        void set_weight_function(int value);
        void set_shape_parameter(double value);

        void set_shader(int value);
        void set_poligon_mode(int value);
        void set_material(int value);

        void set_selected_shape(int value);
        void set_div_point_count(int value);

        //set epsilons
        void set_eps_1(double value);
        void set_eps_2(double value);

        //vertices as sphere is checked or not and the scale factor
        void set_vertices_check(int value);
        void set_vertices_size(double value);

        //show normals is checked or not and the scale factor
        void set_normals_check(int value);
        void set_normals_size(double value);

        //show tangents is checked or not and the scale factor
        void set_tangents_check(int value);
        void set_tangents_size(double value);

        void set_boundaries_check(int value);

        //show tangents is checked or not and the scale factor
        void set_boundaries_normal_check(int value);
        void set_boundaries_normal_size(double value);

        void set_geometry_check(int value);
        void set_geometry(int value);

        //set which vertex would you like to become higlighted
        void set_vertex_number(int value);

        //change models
        void elephant_v102_f200_checked(bool value);
        void m2_v3495_f666_checked(bool value);
        void spot_v122_f240_checked(bool value);
        void bunny_v113_f222_checked(bool value);
        void bunny_v239_f474_checked(bool value);
        void horse_v341_f678_checked(bool value);
        void m195_v2472_f4809_checked(bool value);
        void angel_v1352_f2706_checked(bool value);
        void dragon_v1386_f2772_checked(bool value);
        void icosahedron_v12_f20_checked(bool value);

    signals:
        void statusMessageChanged(const QString& text);
        void setCalculationTimeCurveNetwork(double time);
        void setCalculationTimeC0Surfaces(double time);
        void setCalculationTimeG1Surfaces(double time);
        void setCursor(const QCursor& cursor);
    };
}
