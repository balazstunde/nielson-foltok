#include <QApplication>
#include "GUI/MainWindow.h"
#include "Core/Matrices.h"
#include <QTime>

using namespace cagd;

GLboolean multiply(const Matrix<GLdouble> &A, const Matrix<GLdouble> &B, Matrix<GLdouble> &C, GLboolean useMultiThreading = GL_FALSE)
{
    if (A.GetColumnCount() != B.GetRowCount())
    {
        return GL_FALSE;
    }

    C.ResizeRows(A.GetRowCount());
    C.ResizeColumns(B.GetColumnCount());

    if (!useMultiThreading)
    {
        for (GLuint i = 0; i < C.GetRowCount(); i++)
        {
            for (GLuint j = 0; j < C.GetColumnCount(); j++)
            {
                C(i, j) = 0.0;
                for (GLuint k = 0; k < A.GetColumnCount(); k++)
                {
                    C(i, j) += A(i, k) * B(k, j);
                }
            }
        }
    }
    else
    {
        #pragma omp parallel for
        for (GLint ij = 0; ij < (GLint)C.GetRowCount() * (GLint)C.GetColumnCount(); ij++)
        {
            GLuint i = (GLuint)ij / C.GetColumnCount();
            GLuint j = (GLuint)ij % C.GetColumnCount();

            C(i, j) = 0.0;
            for (GLuint k = 0; k < A.GetColumnCount(); k++)
            {
                C(i, j) += A(i, k) * B(k, j);
            }

        }
    }

    return GL_TRUE;
}

int main(int argc, char **argv)
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);

    // creating an application object and setting one of its attributes
    QApplication app(argc, argv);

    // if you have installed a different version of Qt, it may happen that
    // the application attribute Qt::AA_UseDesktopOpenGL is not recognized
    // on Windows its existence is critical for our applications
    // on Linux or Mac you can uncomment this line
    app.setAttribute(Qt::AA_UseDesktopOpenGL, true);

    // creating a main window object
    MainWindow mwnd;
    mwnd.showMaximized();

//    Matrix<GLdouble> A(1700, 900), B(900, 1800), C;

//    for (GLuint i = 0; i < A.GetRowCount(); i++)
//    {
//        for (GLuint j = 0; j < A.GetColumnCount(); j++)
//        {
//            A(i, j) = i * A.GetColumnCount() + j;
//        }
//    }


//    for (GLuint i = 0; i < B.GetRowCount(); i++)
//    {
//        for (GLuint j = 0; j < B.GetColumnCount(); j++)
//        {
//            B(i, j) = i * B.GetColumnCount() + j;
//        }
//    }

//    cout << A << endl << B << endl;

//    QTime timer;

//    timer.start();
//    if (multiply(A, B, C))//, GL_TRUE))
//    {
//        cout << "Done in " << timer.elapsed() / 1000.0 << " seconds." << endl;
//    }

//    RowMatrix<GLdouble> sormatrix(5);
//    cout << "Sormatrix rows:" << sormatrix.GetRowCount() << endl;
//    cout << "Sormatrix columns:" << sormatrix.GetColumnCount() << endl;
//    sormatrix.FillWithZeros();
//    cout << sormatrix << endl;

//    CyclicCurve3 *_ccc;
//    _ccc = new (nothrow) CyclicCurve3(3);
//    delete _ccc;
//    cout<<"Here: "<<endl;
//    cout<<_ccc<<endl;

//    cout<<"--------------Triangle: ";
//    UpperLeftTriangularMatrix<int> triangle;
//    triangle.ResizeRows(4);
//    triangle.FillWithZeros();
//    triangle(1,2) = 3;
//    cout<<triangle<<endl;

//    RowMatrix<GLdouble> theta;
//    RowMatrix<GLdouble> alpha(4);
//    for(GLuint i = 0; i < 4; i++){
//        alpha[i] = i;
//    }
//    theta = alpha;
//    cout<<"alpha:"<<endl<<alpha<<endl;
//    cout<<"theta:"<<endl<<theta<<endl;

    // running the application
    return app.exec();
}
