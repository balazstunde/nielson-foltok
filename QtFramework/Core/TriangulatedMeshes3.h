#pragma once

#include "DCoordinates3.h"
#include <GL/glew.h>
#include <iostream>
#include <string>
#include "TriangularFaces.h"
#include "TCoordinates4.h"
#include <vector>

namespace cagd
{
    class TriangulatedMesh3
    {
        friend class ParametricSurface3;
        friend class TensorProductSurface3;
        friend class TriangularSurface3;

        // homework: output to stream:
        // vertex count, face count
        // list of vertices
        // list of unit normal vectors
        // list of texture coordinates
        // list of faces
        friend std::ostream& operator <<(std::ostream& lhs, const TriangulatedMesh3& rhs);

        // homework: input from stream: inverse of the ostream operator
        friend std::istream& operator >>(std::istream& lhs, TriangulatedMesh3& rhs);

    public:
        // vertex buffer object identifiers
        GLenum                      _usage_flag;
        GLuint                      _vbo_vertices;
        GLuint                      _vbo_normals;
        GLuint                      _vbo_tex_coordinates;
        GLuint                      _vbo_indices;

        // corners of bounding box
        DCoordinate3                 _leftmost_vertex;
        DCoordinate3                 _rightmost_vertex;

        // geometry
        std::vector<DCoordinate3>    _vertex;
        std::vector<DCoordinate3>    _normal;
        std::vector<TCoordinate4>    _tex;
        std::vector<TriangularFace>  _face;

    public:
        // special and default constructor
        TriangulatedMesh3(GLuint vertex_count = 0, GLuint face_count = 0, GLenum usage_flag = GL_STATIC_DRAW);

        // copy constructor
        TriangulatedMesh3(const TriangulatedMesh3& mesh);

        // assignment operator
        TriangulatedMesh3& operator =(const TriangulatedMesh3& rhs);

        // deletes all vertex buffer objects
        GLvoid DeleteVertexBufferObjects();

        // renders the geometry
        GLboolean Render(GLenum render_mode = GL_TRIANGLES) const;

        GLvoid RenderNormals();

        // updates all vertex buffer objects
        GLboolean UpdateVertexBufferObjects(GLenum usage_flag = GL_STATIC_DRAW);

        // loads the geometry (i.e. the array of vertices and faces) stored in an OFF file
        // at the same time calculates the unit normal vectors associated with vertices
        GLboolean LoadFromOFF(const std::string& file_name, GLboolean translate_and_scale_to_unit_cube = GL_FALSE);

        // homework: saves the geometry into an OFF file
        GLboolean SaveToOFF(const std::string& file_name) const;

        // mapping vertex buffer objects
        GLfloat* MapVertexBuffer(GLenum access_flag = GL_READ_ONLY) const;
        GLfloat* MapNormalBuffer(GLenum access_flag = GL_READ_ONLY) const;  // homework
        GLfloat* MapTextureBuffer(GLenum access_flag = GL_READ_ONLY) const; // homework

        // unmapping vertex buffer objects
        GLvoid UnmapVertexBuffer() const;
        GLvoid UnmapNormalBuffer() const;   // homework
        GLvoid UnmapTextureBuffer() const;  // homework

        // get properties of geometry
        GLuint VertexCount() const; // homework
        GLuint FaceCount() const;   // homework

        GLboolean SetVertex(GLuint index, const DCoordinate3& v);
        GLboolean SetFace(GLuint row, GLuint column, const GLuint &f);
        GLboolean SetNormal(GLuint index, const DCoordinate3& n);
        DCoordinate3 GetNormal(GLuint index);

        // destructor
        virtual ~TriangulatedMesh3();
    };

    // homework: output to stream:
    // vertex count, face count
    // list of vertices
    // list of unit normal vectors
    // list of texture coordinates
    // list of faces
    /*
      // vertex buffer object identifiers
        GLenum                      _usage_flag;
        GLuint                      _vbo_vertices;
        GLuint                      _vbo_normals;
        GLuint                      _vbo_tex_coordinates;
        GLuint                      _vbo_indices;

        // corners of bounding box
        DCoordinate3                 _leftmost_vertex;
        DCoordinate3                 _rightmost_vertex;

        // geometry
        std::vector<DCoordinate3>    _vertex;
        std::vector<DCoordinate3>    _normal;
        std::vector<TCoordinate4>    _tex;
        std::vector<TriangularFace>  _face;
        */
    inline std::ostream& operator <<(std::ostream& lhs, const TriangulatedMesh3& rhs){
        lhs << "Vertex count: " << rhs._vertex.size() << "\n";
        lhs << "Face count: " << rhs._face.size() << "\n";
        lhs << "List of vertices: ";
        for(std::vector<DCoordinate3>::const_iterator v = rhs._vertex.begin(); v < rhs._vertex.end(); v++){
            lhs << *v << std::endl;
        }

//        lhs << "List of normal vectors: ";
//        for(std::vector<DCoordinate3>::const_iterator n = rhs._normal.begin(); n < rhs._normal.end(); n++){
//            lhs << *n << std::endl;
//        }

//        lhs << "List of texture coordinates: ";
//        for(std::vector<TCoordinate4>::const_iterator t = rhs._tex.begin(); t < rhs._tex.end(); t++){
//            lhs << *t << std::endl;
//        }

        lhs << "List of faces: ";
        for(std::vector<TriangularFace>::const_iterator f = rhs._face.begin(); f < rhs._face.end(); f++){
            lhs << *f << std::endl;
        }

        return lhs;
    }


    // homework: input to stream:
    // vertex count, face count
    // list of vertices
    // list of unit normal vectors
    // list of texture coordinates
    // list of faces
    inline std::istream& operator >>(std::istream& lhs, TriangulatedMesh3& rhs){
        GLuint vertex_count, face_count;
        lhs >> vertex_count >>face_count;

        rhs._vertex.resize(vertex_count);
        rhs._face.resize(face_count);

        for(std::vector<DCoordinate3>::iterator v = rhs._vertex.begin(); v < rhs._vertex.end(); v++){
            lhs >> *v;
        }

        for(std::vector<DCoordinate3>::iterator n = rhs._normal.begin(); n < rhs._normal.end(); n++){
            lhs >> *n;
        }

        for(std::vector<TCoordinate4>::iterator t = rhs._tex.begin(); t < rhs._tex.end(); t++){
            lhs >> *t;
        }

        for(std::vector<TriangularFace>::iterator f = rhs._face.begin(); f < rhs._face.end(); f++){
            lhs >> *f;
        }

        return lhs;
    }

}
