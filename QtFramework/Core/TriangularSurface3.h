#pragma once

#include "DCoordinates3.h"
#include "Matrices.h"
#include "TriangulatedMeshes3.h"

namespace cagd
{
    //-------------------------
    // class TriangularSurface3
    //-------------------------
    class TriangularSurface3
    {
    public:
        class PartialDerivatives: public TriangularMatrix<DCoordinate3>
        {
        public:
            // special/default constructor
            PartialDerivatives(GLuint maximum_order_of_derivatives = 2);

            // copy constructor
            PartialDerivatives(const PartialDerivatives& d);

            // assignment operator
            PartialDerivatives& operator =(const PartialDerivatives& rhs);

            // all inherited Descartes coordinates are set to the null vector
            GLvoid LoadNullVectors();
        };

    protected:
        GLuint                         _vbo_data;
        GLuint                         _vbo_indices;
        GLdouble                       _beta;
        TriangularMatrix<DCoordinate3> _net;

    public:
        TriangularSurface3(GLdouble beta = 1.0);
        // copy constructor
        TriangularSurface3(const TriangularSurface3& ts);
        // assignment operator
        TriangularSurface3& operator =(const TriangularSurface3& rhs);

        DCoordinate3  operator ()(GLuint row, GLuint column) const;
        DCoordinate3& operator ()(GLuint row, GLuint column);

        virtual GLvoid DeleteVertexBufferObjectsOfData();
        GLboolean RenderControlNet(GLenum render_mode = GL_TRIANGLES) const;
        virtual GLboolean UpdateVertexBufferObjectsOfData(GLenum usage_flag = GL_STATIC_DRAW);

        virtual GLboolean BlendingFunctionValues(GLdouble u, GLdouble v, TriangularMatrix<GLdouble> &values) const = 0;
        virtual GLboolean CalculatePartialDerivatives(GLdouble u, GLdouble v, PartialDerivatives& pd) const = 0;
        TriangulatedMesh3* GenerateImage(GLuint div_point_count, GLenum usage_flag = GL_STATIC_DRAW) const;

        virtual ~TriangularSurface3();
    };
}
