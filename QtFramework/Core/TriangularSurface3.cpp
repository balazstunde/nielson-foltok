#include "TriangularSurface3.h"
#include "RealSquareMatrices.h"
#include <algorithm>

using namespace cagd;
using namespace std;

// special/default constructor
TriangularSurface3::PartialDerivatives::PartialDerivatives(GLuint maximum_order_of_derivatives):
    TriangularMatrix<DCoordinate3>(maximum_order_of_derivatives + 1)
{
}

// copy constructor
TriangularSurface3::PartialDerivatives::PartialDerivatives(const TriangularSurface3::PartialDerivatives& d):
    TriangularMatrix<DCoordinate3>(d)
{
}

// assignment operator
TriangularSurface3::PartialDerivatives& TriangularSurface3::PartialDerivatives::operator =(const TriangularSurface3::PartialDerivatives& rhs)
{
    if (this != &rhs)
    {
        TriangularMatrix<DCoordinate3>::operator =(rhs);
    }
    return *this;
}

// set every derivative to null vector
GLvoid TriangularSurface3::PartialDerivatives::LoadNullVectors()
{
    for (GLuint i = 0; i < _data.size(); ++i)
    {
        for(GLuint j = 0; j <= i; j++){
            _data[i][j] = DCoordinate3(0.0, 0.0, 0.0);
        }
    }
}

// special constructor
TriangularSurface3::TriangularSurface3(GLdouble beta):
        _vbo_data(0), _vbo_indices(0), _beta(beta)
{
    _net.ResizeRows(4);
}

// copy constructor
TriangularSurface3::TriangularSurface3(const TriangularSurface3 &ts):
        _vbo_data(0),
        _vbo_indices(0),
        _beta(ts._beta),
        _net(ts._net)
{
    if (ts._vbo_data && ts._vbo_indices)
        UpdateVertexBufferObjectsOfData();
}

// assignment operator
TriangularSurface3& TriangularSurface3::operator =(const TriangularSurface3& rhs)
{
    if (this != &rhs)
    {
        DeleteVertexBufferObjectsOfData();

        _beta = rhs._beta;
        _net = rhs._net;

        if (rhs._vbo_data && rhs._vbo_indices)
            UpdateVertexBufferObjectsOfData();
    }

    return *this;
}

GLvoid TriangularSurface3::DeleteVertexBufferObjectsOfData()
{
    if (_vbo_data)
    {
        glDeleteBuffers(1, &_vbo_data);
        _vbo_data = 0;
    }

    if (_vbo_indices)
    {
        glDeleteBuffers(1, &_vbo_indices);
        _vbo_indices = 0;
    }
}

GLboolean TriangularSurface3::RenderControlNet(GLenum render_mode) const
{
    if(!_vbo_data || !_vbo_indices || (render_mode != GL_TRIANGLES && render_mode != GL_POINTS)){
        return GL_FALSE;
    }

    //cout<<"The central point: "<<_net(2, 1)<<endl;

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


    glEnableClientState(GL_VERTEX_ARRAY);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
        glVertexPointer(3, GL_FLOAT, 0, nullptr);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_indices);
        glDrawElements(render_mode, 27, GL_UNSIGNED_INT, nullptr);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableClientState(GL_VERTEX_ARRAY);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    return GL_TRUE;
}

GLboolean TriangularSurface3::UpdateVertexBufferObjectsOfData(GLenum usage_flag){
    GLuint data_row_count = _net.GetRowCount();
    GLuint data_count = (data_row_count * (data_row_count + 1)) / 2;
    if (!data_row_count )
        return GL_FALSE;

    if (usage_flag != GL_STREAM_DRAW  && usage_flag != GL_STREAM_READ  && usage_flag != GL_STREAM_COPY
     && usage_flag != GL_DYNAMIC_DRAW && usage_flag != GL_DYNAMIC_READ && usage_flag != GL_DYNAMIC_COPY
     && usage_flag != GL_STATIC_DRAW  && usage_flag != GL_STATIC_READ  && usage_flag != GL_STATIC_COPY)
        return GL_FALSE;

    DeleteVertexBufferObjectsOfData();

    glGenBuffers(1, &_vbo_data);
    if (!_vbo_data){
        return GL_FALSE;
    }

    glGenBuffers(1, &_vbo_indices);
    if (!_vbo_indices)
    {
        glDeleteBuffers(1, &_vbo_data);
        _vbo_data = 0;
        return GL_FALSE;
    }

    glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
    glBufferData(GL_ARRAY_BUFFER, data_count * 3 * sizeof(GLfloat), nullptr, usage_flag);

    GLfloat *coordinate = static_cast <GLfloat*>(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
    if (!coordinate)
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        DeleteVertexBufferObjectsOfData();
        return GL_FALSE;
    }

    for (GLuint i = 0; i < _net.GetRowCount(); ++i)
    {
        for (GLuint j = 0; j <= i; ++j)
        {
            for(GLuint k = 0; k < 3; k++){
                *coordinate = static_cast <GLfloat>(_net(i,j)[k]);
                ++coordinate;
            }

        }
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_indices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 9 * 3 * sizeof(GLuint), nullptr, usage_flag);

     GLuint *element = static_cast <GLuint*>(glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY));

     if (!element)
     {
         DeleteVertexBufferObjectsOfData();
         return GL_FALSE;
     }
     for (GLuint i = 0; i < _net.GetRowCount(); ++i)
     {
         for (GLuint j = 0; j <= i; ++j)
         {
             GLuint index[3];

             index[0] = i * (i + 1) / 2 + j;
             index[1] = (i + 1) * (i + 2) / 2 + j;
             index[2] = (i + 1) * (i + 2) / 2 + j + 1;

             if (i < _net.GetRowCount() - 1)
             {
                 *element = index[0];
                 element++;

                 *element = index[1];
                 element++;

                 *element = index[2];
                 element++;
             }

             index[0] = i * (i + 1) / 2 + j;
             index[1] = (i + 1) * (i + 2) / 2 + j + 1;
             index[2] = i * (i + 1) / 2 + j + 1;

             if (i < _net.GetRowCount() - 1 && j < i)
             {
                 *element = index[0];
                 element++;

                 *element = index[1];
                 element++;

                 *element = index[2];
                 element++;
             }
         }
     }

    glBindBuffer(GL_ARRAY_BUFFER, _vbo_data);
    if (!glUnmapBuffer(GL_ARRAY_BUFFER))
    {
        return GL_FALSE;
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo_indices);
    if (!glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER))
        return GL_FALSE;


    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    return GL_TRUE;
}

// get data by value
DCoordinate3  TriangularSurface3::operator ()(GLuint row, GLuint column) const
{
    return _net(row,column);
}

// get data by reference
DCoordinate3& TriangularSurface3::operator ()(GLuint row, GLuint column)
{
    return _net(row,column);
}

// generate image/arc
TriangulatedMesh3* TriangularSurface3::GenerateImage(GLuint div_point_count, GLenum usage_flag) const
{
    if (div_point_count < 2)
    {
        return nullptr;
    }

    if (usage_flag != GL_STATIC_DRAW && usage_flag != GL_STREAM_DRAW && usage_flag != GL_DYNAMIC_DRAW)
    {
        return nullptr;
    }

    TriangulatedMesh3 *result = new (nothrow) TriangulatedMesh3(div_point_count * (div_point_count + 1) / 2, (div_point_count - 1) * (div_point_count - 1), usage_flag);

    if (!result)
    {
        return nullptr;
    }

    GLuint current_face = 0;

    DCoordinate3 p(_beta, 0, 0), q(0, _beta, 0), r(0, 0, _beta);

    GLdouble ds = 1.0 / static_cast <GLdouble>(div_point_count - 1);
    GLboolean aborted = GL_FALSE;

    #pragma omp parallel for
    for (GLint ij = 0; ij < static_cast <GLint>(div_point_count * (div_point_count + 1) / 2); ++ij)
    {
        #pragma omp flush (aborted)
        // if there was no problem
        if(!aborted){
            GLint i = static_cast <GLint>(floor((sqrt(1 + 8 * ij) - 1.0) / 2.0));
            GLint j = ij - (i * (i + 1) / 2);

            //calculate the allocation points
            GLdouble s = min(i * ds, 1.0);

            DCoordinate3 x = p + (q - p) * s;
            DCoordinate3 y = p + (r - p) * s;

            GLdouble dt = i ? 1.0 / static_cast <GLdouble>(i) : 0.0;

            GLdouble t = min(j * dt, 1.0);
            DCoordinate3 z = x + (y - x) * t;

            //calculate the partial derivatives
            PartialDerivatives pd;
            if (!CalculatePartialDerivatives(z[0], z[1], pd))
            {
                aborted = GL_TRUE;
                #pragma omp flush (aborted)
            }

            GLuint index_lower[3];

            // calculate the current face indices
            index_lower[0] = static_cast <GLuint>(ij);
            index_lower[1] = static_cast <GLuint>((i + 1) * (i + 2) / 2 + j);
            index_lower[2] = static_cast <GLuint>((i + 1) * (i + 2) / 2 + j + 1);

            //the vertex is the 0th order derivative
            (*result)._vertex[index_lower[0]] =  pd(0, 0);

            // the normal is the two partial derivatives product
            (*result)._normal[index_lower[0]] =  pd(1, 0);
            (*result)._normal[index_lower[0]] ^= pd(1, 1);
            (*result)._normal[index_lower[0]].normalize();

            GLuint index_upper[3];

            // the same thing like above just for the upper face
            index_upper[0] = static_cast <GLuint>(ij);
            index_upper[1] = static_cast <GLuint>((i + 1) * (i + 2) / 2 + j + 1);
            index_upper[2] = static_cast <GLuint>(ij + 1);

            #pragma omp critical
            // set the faces
            if (i < static_cast <GLint>(div_point_count - 1))
            {
                (*result)._face[current_face][0] = index_lower[0];
                (*result)._face[current_face][1] = index_lower[1];
                (*result)._face[current_face][2] = index_lower[2];
                ++current_face;
            }

            #pragma omp critical

            if (i < static_cast <GLint>(div_point_count) - 1 && j < i)
            {
                (*result)._face[current_face][0] = index_upper[0];
                (*result)._face[current_face][1] = index_upper[1];
                (*result)._face[current_face][2] = index_upper[2];
                ++current_face;
            }
        }
    }

    if(aborted){
        delete result;
        result = nullptr;
    }
    return result;

}

TriangularSurface3::~TriangularSurface3()
{
    DeleteVertexBufferObjectsOfData();
}
