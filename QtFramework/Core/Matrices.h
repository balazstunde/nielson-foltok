#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>

using namespace std;
namespace cagd
{
    // forward declaration of template class Matrix
    template <typename T>
    class Matrix;

    // forward declaration of template class RowMatrix
    template <typename T>
    class RowMatrix;

    // forward declaration of template class ColumnMatrix
    template <typename T>
    class ColumnMatrix;

	// forward declaration of template class TriangularMatrix
    template <typename T>
    class TriangularMatrix;

    // forward declarations of overloaded and templated input/output from/to stream operators
    template <typename T>
    std::ostream& operator << (std::ostream& lhs, const Matrix<T>& rhs);

    template <typename T>
    std::istream& operator >>(std::istream& lhs, Matrix<T>& rhs);

	template <typename T>
    std::istream& operator >>(std::istream& lhs, TriangularMatrix<T>& rhs);
    
    template <typename T>
    std::ostream& operator << (std::ostream& lhs, const TriangularMatrix<T>& rhs);

    //----------------------
    // template class Matrix
    //----------------------
    template <typename T>
    class Matrix
    {
        friend std::ostream& cagd::operator << <T>(std::ostream&, const Matrix<T>& rhs);
        friend std::istream& cagd::operator >> <T>(std::istream&, Matrix<T>& rhs);

    protected:
        GLuint                          _row_count;
        GLuint                          _column_count;
        std::vector< std::vector<T> >   _data;
    public:
        // special constructor (can also be used as a default constructor)
        Matrix(GLuint row_count = 1, GLuint column_count = 1);

        // copy constructor
        Matrix(const Matrix& m);

        // assignment operator
        Matrix& operator =(const Matrix& m);

        // get element by reference
        T& operator ()(GLuint row, GLuint column);

        // get copy of an element
        T operator ()(GLuint row, GLuint column) const;

        // get dimensions
        GLuint GetRowCount() const;
        GLuint GetColumnCount() const;

        // set dimensions
        virtual GLboolean ResizeRows(GLuint row_count);
        virtual GLboolean ResizeColumns(GLuint column_count);

        // update
        GLboolean SetRow(GLuint index, const RowMatrix<T>& row);
        GLboolean SetColumn(GLuint index, const ColumnMatrix<T>& column);

        // destructor
        virtual ~Matrix();
    };

    //-------------------------
    // template class RowMatrix
    //-------------------------
    template <typename T>
    class RowMatrix: public Matrix<T>
    {
    public:
        // special constructor (can also be used as a default constructor)
        RowMatrix(GLuint column_count = 1);

        // get element by reference
        T& operator ()(GLuint column);
        T& operator [](GLuint column);

        // get copy of an element
        T operator ()(GLuint column) const;
        T operator [](GLuint column) const;

        // a row matrix consists of a single row
        GLboolean ResizeRows(GLuint row_count);
        GLboolean FillWithZeros();
    };

    //----------------------------
    // template class ColumnMatrix
    //----------------------------
    template <typename T>
    class ColumnMatrix: public Matrix<T>
    {
    public:
        // special constructor (can also be used as a default constructor)
        ColumnMatrix(GLuint row_count = 1);

        // get element by reference
        T& operator ()(GLuint row);
        T& operator [](GLuint row);

        // get copy of an element
        T operator ()(GLuint row) const;
        T operator [](GLuint row) const;

        // a column matrix consists of a single column
        GLboolean ResizeColumns(GLuint column_count);
    };

	//--------------------------------
    // template class TriangularMatrix
    //--------------------------------
    template <typename T>
    class TriangularMatrix
    {
        friend std::istream& cagd::operator >> <T>(std::istream&, TriangularMatrix<T>& rhs);
        friend std::ostream& cagd::operator << <T>(std::ostream&, const TriangularMatrix<T>& rhs);

    protected:
        GLuint                        _row_count;
        std::vector< std::vector<T> > _data;

    public:
        // special constructor (can also be used as a default constructor)
        TriangularMatrix(GLuint row_count = 1);

        // get element by reference
        T& operator ()(GLuint row, GLuint column);

        // get copy of an element
        T operator ()(GLuint row, GLuint column) const;

        // get dimension
        GLuint GetRowCount() const;

        // set dimension
        GLboolean ResizeRows(GLuint row_count);

        GLboolean FillWithZeros();
    };

    //--------------------------------
    // template class TriangularMatrix
    //--------------------------------
    template <typename T>
    class UpperLeftTriangularMatrix
    {
        friend std::istream& cagd::operator >> <T>(std::istream&, UpperLeftTriangularMatrix<T>& rhs);
        friend std::ostream& cagd::operator << <T>(std::ostream&, const UpperLeftTriangularMatrix<T>& rhs);

    protected:
        GLuint                        _row_count;
        std::vector< std::vector<T> > _data;

    public:
        // special constructor (can also be used as a default constructor)
        UpperLeftTriangularMatrix(GLuint row_count = 1);

        // get element by reference
        T& operator ()(GLuint row, GLuint column);

        // get copy of an element
        T operator ()(GLuint row, GLuint column) const;

        // get dimension
        GLuint GetRowCount() const;

        // set dimension
        GLboolean ResizeRows(GLuint row_count);

        GLboolean FillWithZeros();
    };

    //--------------------------------------------------
    // homework: implementation of template class Matrix
    //--------------------------------------------------

    // special constructor (can also be used as a default constructor)
    template<typename T>
    inline Matrix<T>::Matrix(GLuint row_count, GLuint column_count):
        _row_count(row_count),
        _column_count(column_count),
        _data(_row_count, vector<T>(_column_count)) {}

    // copy constructor
    template<typename T>
    inline Matrix<T>::Matrix(const Matrix& m):
        _row_count(m._row_count),
        _column_count(m._column_count),
        _data(m._data){}

    // assignment operator
    template<typename T>
    inline Matrix<T>& Matrix<T>::operator =(const Matrix& m){
        if(this != &m){
            _row_count = m._row_count;
            _column_count = m._column_count;
            _data = m._data;
        }
        return *this;
    }

    // get element by reference
    template<typename T>
    inline T& Matrix<T>::operator ()(GLuint row, GLuint column){
        return _data[row][column];
    }

    // get copy of an element
    template<typename T>
    inline T Matrix<T>::operator ()(GLuint row, GLuint column) const{
        return _data[row][column];
    }

    // get dimensions
    template<typename T>
    inline GLuint Matrix<T>::GetRowCount() const{
        return _row_count;
    }

    template<typename T>
    inline GLboolean Matrix<T>::ResizeRows(GLuint row_count){
       _data.resize(row_count, std::vector<T>(_column_count));
       _row_count = row_count;
       return GL_TRUE;
    }

    template<typename T>
    inline GLboolean Matrix<T>::ResizeColumns(GLuint column_count){

        /*for(GLint i = 0 ; i < _row_count; i++){
            _data[i].resize(_row_count, vector<T>(column_count));
        }*/
        for (auto& row : _data){
            row.resize(column_count);
        }
       _column_count = column_count;
       return GL_TRUE;
    }

    template<typename T>
    inline GLuint Matrix<T>::GetColumnCount() const{
        return _column_count;
    }

    // update
    template<typename T>
    GLboolean Matrix<T>::SetRow(GLuint index, const RowMatrix<T>& row){
       if (index >= _row_count || _column_count != row._column_count) {
           return GL_FALSE;
       }
       _data[index] = row._data[0];
       return GL_TRUE;
    }

    template<typename T>
    GLboolean Matrix<T>::SetColumn(GLuint index, const ColumnMatrix<T>& column){
        if (index >= _column_count || _row_count != column._row_count) {
            return GL_FALSE;
        }
        for(GLuint i = 0 ; i < _row_count; i++){
            _data[i][index] = column._data[i][0];
        }
        return GL_TRUE;
    }

    template <typename T>
    Matrix<T>::~Matrix() {
        _data.clear();
        _column_count = 0;
        _row_count = 0;
    }

    //-----------------------------------------------------
    // homework: implementation of template class RowMatrix
    //-----------------------------------------------------

    // special constructor (can also be used as a default constructor)
    template<typename T>
    RowMatrix<T>::RowMatrix(GLuint column_count):
        Matrix<T>(1, column_count) {}

    // get element by reference
    template<typename T>
    T& RowMatrix<T>::operator ()(GLuint column){
        return Matrix<T>::_data[0][column];
    }

    template<typename T>
    T& RowMatrix<T>::operator [](GLuint column){
        return Matrix<T>::_data[0][column];
    }

    // get copy of an element
    template<typename T>
    T RowMatrix<T>::operator ()(GLuint column) const{
        return Matrix<T>::_data[0][column];
    }

    template<typename T>
    T RowMatrix<T>::operator [](GLuint column) const{
        return Matrix<T>::_data[0][column];
    }

    // a row matrix consists of a single row
    template<typename T>
    GLboolean RowMatrix<T>::ResizeRows(GLuint row_count){
        return (row_count == 1);
    }

    template<typename T>
    GLboolean RowMatrix<T>::FillWithZeros(){
        for(GLuint j = 0; j < this->GetColumnCount(); j++){
           Matrix<T>::_data[0][j] = 0.0;
        }
        return GL_TRUE;
    }

    //--------------------------------------------------------
    // homework: implementation of template class ColumnMatrix
    //--------------------------------------------------------

    // special constructor (can also be used as a default constructor)
    template<typename T>
    ColumnMatrix<T>::ColumnMatrix(GLuint row_count):
        Matrix<T> (row_count, 1){}

    // get element by reference
    template<typename T>
    T& ColumnMatrix<T>::operator ()(GLuint row){
        return Matrix<T>::_data[row][0];
    }
    template<typename T>
    T& ColumnMatrix<T>::operator [](GLuint row){
        return Matrix<T>::_data[row][0];
    }

    // get copy of an element
    template<typename T>
    T ColumnMatrix<T>::operator ()(GLuint row) const{
        return Matrix<T>::_data[row][0];
    }

    template<typename T>
    T ColumnMatrix<T>::operator [](GLuint row) const{
        return Matrix<T>::_data[row][0];
    }

    // a column matrix consists of a single column
    template<typename T>
    GLboolean ColumnMatrix<T>::ResizeColumns(GLuint column_count){
        return (column_count == 1);
    }

	//------------------------------------------------------------
    // homework: implementation of template class TriangularMatrix
    //------------------------------------------------------------

    // special constructor (can also be used as a default constructor)
    template<typename T>
    TriangularMatrix<T>::TriangularMatrix(GLuint row_count):
        _row_count(row_count),
        _data(row_count, vector<T>(1)){
        for(GLuint i = 0; i< _row_count; i++){
            _data[i].resize(i+1);
        }
    }


    // get element by reference
    template<typename T>
    T& TriangularMatrix<T>::operator ()(GLuint row, GLuint column){
        return _data[row][column];
    }

    // get copy of an element
    template<typename T>
    T TriangularMatrix<T>::operator ()(GLuint row, GLuint column) const{
        return _data[row][column];
    }

    // get dimension
    template<typename T>
    GLuint TriangularMatrix<T>::GetRowCount() const{
        return _row_count;
    }

    // set dimension
    template<typename T>
    GLboolean TriangularMatrix<T>::ResizeRows(GLuint row_count){
        _data.resize(row_count);
        for(GLuint i = _row_count; i < row_count; i++){
            _data[i].resize(i+1);
        }
        _row_count = row_count;
        return GL_TRUE;
    }

    template<typename T>
    GLboolean TriangularMatrix<T>::FillWithZeros(){
        for(GLuint i = 0; i < _row_count; i++){
            for(GLuint j = 0; j <= i; j++){
                _data[i][j] = 0;
            }
        }
        return GL_TRUE;
    }

    //------------------------------------------------------------
    // homework: implementation of template class UpperLeftTriangularMatrix
    //------------------------------------------------------------

    // special constructor (can also be used as a default constructor)
    template<typename T>
    UpperLeftTriangularMatrix<T>::UpperLeftTriangularMatrix(GLuint row_count):
        _row_count(row_count),
        _data(row_count, vector<T>(1)){
        for(GLuint i = 0; i < _row_count; i++){
            _data[i].resize(row_count - i);
        }
    }


    // get element by reference
    template<typename T>
    T& UpperLeftTriangularMatrix<T>::operator ()(GLuint row, GLuint column){
        return _data[row][column];
    }

    // get copy of an element
    template<typename T>
    T UpperLeftTriangularMatrix<T>::operator ()(GLuint row, GLuint column) const{
        return _data[row][column];
    }

    // get dimension
    template<typename T>
    GLuint UpperLeftTriangularMatrix<T>::GetRowCount() const{
        return _row_count;
    }

    // set dimension
    template<typename T>
    GLboolean UpperLeftTriangularMatrix<T>::ResizeRows(GLuint row_count){
        _data.resize(row_count);
        for(GLuint i = 0; i < row_count; i++){
            _data[i].resize(row_count - i);
        }
        _row_count = row_count;
        return GL_TRUE;
    }

    template<typename T>
    GLboolean UpperLeftTriangularMatrix<T>::FillWithZeros(){
        for(GLuint i = 0; i < _row_count; i++){
            for(GLuint j = 0; j < _row_count - i; j++){
                _data[i][j] = 0;
            }
        }
        return GL_TRUE;
    }

    //------------------------------------------------------------------------------
    // definitions of overloaded and templated input/output from/to stream operators
    //------------------------------------------------------------------------------

    // output to stream
    template <typename T>
    std::ostream& operator <<(std::ostream& lhs, const Matrix<T>& rhs)
    {
        lhs << rhs._row_count << " " << rhs._column_count << std::endl;
        for (typename std::vector< std::vector<T> >::const_iterator row = rhs._data.begin(); row != rhs._data.end(); ++row)
        {
            for (typename std::vector<T>::const_iterator column = row->begin();
                 column != row->end(); ++column)
                    lhs << *column << " ";
            lhs << std::endl;
        }
        return lhs;
    }

    template <typename T>
    std::ostream& operator <<(std::ostream& lhs, const TriangularMatrix<T>& rhs)
    {
        lhs << rhs._row_count << std::endl;
        for (typename std::vector< std::vector<T> >::const_iterator row = rhs._data.begin(); row != rhs._data.end(); ++row)
        {
            for (typename std::vector<T>::const_iterator column = row->begin(); column != row->end(); ++column)
                    lhs << *column << " ";
            lhs << std::endl;
        }
        return lhs;
    }

    template <typename T>
    std::ostream& operator <<(std::ostream& lhs, const UpperLeftTriangularMatrix<T>& rhs)
    {
        lhs << rhs._row_count << std::endl;
        for (typename std::vector< std::vector<T> >::const_iterator row = rhs._data.begin(); row != rhs._data.end(); ++row)
        {
            for (typename std::vector<T>::const_iterator column = row->begin(); column != row->end(); ++column)
                    lhs << *column << " ";
            lhs << std::endl;
        }
        return lhs;
    }

    // input from stream
    template <typename T>
    std::istream& operator >>(std::istream& lhs, Matrix<T>& rhs)
    {
        lhs >> rhs._row_count >> rhs._column_count;
        rhs._data.resize(rhs._row_count);
        for(typename std::vector< std::vector<T> >::iterator row = rhs._data.begin(); row != rhs._data.end(); ++row){
            row->resize(rhs._column_count);
            for (typename std::vector<T>::iterator column = row->begin(); column != row->end(); column++){
                lhs >> *column;
            }
        }
        return lhs;
    }

    template <typename T>
    std::istream& operator >>(std::istream& lhs, TriangularMatrix<T>& rhs)
    {
        lhs >> rhs._row_count;
        rhs._data.resize(rhs._row_count);
        GLuint i = 1;
        for(typename std::vector< std::vector<T> >::iterator row = rhs._data.begin(); row != rhs._data.end(); ++row, i++){
            row->resize(i);
            for (typename std::vector<T>::iterator column = row->begin(); column != row->end(); column++){
                lhs >> *column;
            }
        }
        return lhs;
    }

    template <typename T>
    std::istream& operator >>(std::istream& lhs, UpperLeftTriangularMatrix<T>& rhs)
    {
        lhs >> rhs._row_count;
        rhs._data.resize(rhs._row_count);
        GLuint i = 1;
        for(typename std::vector< std::vector<T> >::iterator row = rhs._data.begin(); row != rhs._data.end(); ++row, i++){
            row->resize(i);
            for (typename std::vector<T>::iterator column = row->begin(); column != row->end(); column++){
                lhs >> *column;
            }
        }
        return lhs;
    }

}
