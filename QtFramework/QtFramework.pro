QT += core gui widgets opengl

win32 {
    message("Windows platform...")

    INCLUDEPATH += $$PWD/Dependencies/Include
    DEPENDPATH += $$PWD/Dependencies/Include

    LIBS += opengl32.lib

    LIBS += -lopengl32 -lglu32

    CONFIG(release, debug|release): {
        contains(QT_ARCH, i386) {
            message("x86 (i.e., 32-bit) release build")
            LIBS += -L"$$PWD/Dependencies/Lib/GL/x86/" -lglew32
        } else {
            message("x86_64 (i.e., 64-bit) release build")
            LIBS += -L"$$PWD/Dependencies/Lib/GL/x86_64/" -lglew32
        }
    } else: CONFIG(debug, debug|release): {
        contains(QT_ARCH, i386) {
            message("x86 (i.e., 32-bit) debug build")
            LIBS += -L"$$PWD/Dependencies/Lib/GL/x86/" -lglew32
        } else {
            message("x86_64 (i.e., 64-bit) debug build")
            LIBS += -L"$$PWD/Dependencies/Lib/GL/x86_64" -lglew32
        }
    }

    msvc {
      QMAKE_CXXFLAGS += -openmp -arch:AVX -D "_CRT_SECURE_NO_WARNINGS"
      QMAKE_CXXFLAGS_RELEASE *= -O2
    }
}

unix: !mac {
    message("Unix/Linux platform...")

    # for GLEW installed into /usr/lib/libGLEW.so or /usr/lib/glew.lib
    LIBS += -lGLEW -lGLU
}

mac {
    message("Macintosh platform...")

    # IMPORTANT: change the letters x, y, z in the next two lines
    # to the corresponding version numbers of the GLEW library
    # which was installed by using the command 'brew install glew'
    INCLUDEPATH += "/usr/local/Cellar/glew/x.y.z/include/"
    LIBS += -L"/usr/local/Cellar/glew/x.y.z/lib/" -lGLEW

    # the OpenGL library has to added as a framework
    LIBS += -framework OpenGL
}
FORMS += \
    GUI/MainWindow.ui \
    GUI/SideWidget.ui \
    GUI/MainWindow.ui \
    GUI/SideWidget.ui



DISTFILES += \
    Models/mouse.off

HEADERS += \
    Core/Colors4.h \
    Core/Constants.h \
    Core/DCoordinates3.h \
    Core/Exceptions.h \
    Core/GenericCurves3.h \
    Core/HCoordinates3.h \
    Core/Lights.h \
    Core/LinearCombination3.h \
    Core/Materials.h \
    Core/Matrices.h \
    Core/RealSquareMatrices.h \
    Core/TCoordinates4.h \
    Core/TensorProductSurfaces3.h \
    Core/TriangularFaces.h \
    Core/TriangularSurface3.h \
    Core/TriangulatedMeshes3.h \
    Cyclic/CyclicCurves3.h \
    FourPointBasedCurves/FourPointBasedCurve3.h \
    GUI/GLWidget.h \
    GUI/MainWindow.h \
    GUI/SideWidget.h \
    LinearCombinationExamples/CubicBezierCurve3.h \
    LinearCombinationExamples/QuarticPolinomialCurve3.h \
    LinearCombinationExamples/SecondOrderHyperbolicCurve3.h \
    LinearCombinationExamples/SecondOrderTrigonometricCurve3.h \
    Parametric/ParametricCurves3.h \
    Parametric/ParametricSurfaces3.h \
    Test/TestFunctions.h \
    TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.h \
    TriangularSurfaceExamples/CubicBezierTriangle3.h \
    TriangularSurfaceExamples/QuarticPolinomialTriangle3.h \
    TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.h \
    TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.h \
    Core/ShaderPrograms.h \
    Core/Colors4.h \
    Core/Constants.h \
    Core/DCoordinates3.h \
    Core/Exceptions.h \
    Core/GenericCurves3.h \
    Core/HCoordinates3.h \
    Core/Lights.h \
    Core/LinearCombination3.h \
    Core/Materials.h \
    Core/Matrices.h \
    Core/RealSquareMatrices.h \
    Core/ShaderPrograms.h \
    Core/TCoordinates4.h \
    Core/TensorProductSurfaces3.h \
    Core/TriangularFaces.h \
    Core/TriangularSurface3.h \
    Core/TriangulatedMeshes3.h \
    Cyclic/CyclicCurves3.h \
    Dependencies/Include/GL/glew.h \
    FourPointBasedCurves/FourPointBasedCurve3.h \
    GUI/GLWidget.h \
    GUI/MainWindow.h \
    GUI/SideWidget.h \
    LinearCombinationExamples/CubicBezierCurve3.h \
    LinearCombinationExamples/QuarticPolinomialCurve3.h \
    LinearCombinationExamples/SecondOrderHyperbolicCurve3.h \
    LinearCombinationExamples/SecondOrderTrigonometricCurve3.h \
    Parametric/ParametricCurves3.h \
    Parametric/ParametricSurfaces3.h \
    TenPointsBasedTriangles/TenPointsBasedTriangle.h \
    Test/TestFunctions.h \
    TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.h \
    TriangularSurfaceExamples/CubicBezierTriangle3.h \
    TriangularSurfaceExamples/QuarticPolinomialTriangle3.h \
    TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.h \
    TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.h \
    Core/Colors4.h \
    Core/Constants.h \
    Core/DCoordinates3.h \
    Core/Exceptions.h \
    Core/GenericCurves3.h \
    Core/HCoordinates3.h \
    Core/Lights.h \
    Core/LinearCombination3.h \
    Core/Materials.h \
    Core/Matrices.h \
    Core/RealSquareMatrices.h \
    Core/ShaderPrograms.h \
    Core/TCoordinates4.h \
    Core/TensorProductSurfaces3.h \
    Core/TriangularFaces.h \
    Core/TriangularSurface3.h \
    Core/TriangulatedMeshes3.h \
    Cyclic/CyclicCurves3.h \
    Dependencies/Include/GL/glew.h \
    FourPointBasedCurves/FourPointBasedCurve3.h \
    GUI/GLWidget.h \
    GUI/MainWindow.h \
    GUI/SideWidget.h \
    LinearCombinationExamples/CubicBezierCurve3.h \
    LinearCombinationExamples/QuarticPolinomialCurve3.h \
    LinearCombinationExamples/SecondOrderHyperbolicCurve3.h \
    LinearCombinationExamples/SecondOrderTrigonometricCurve3.h \
    Parametric/ParametricCurves3.h \
    Parametric/ParametricSurfaces3.h \
    TenPointsBasedTriangles/TenPointsBasedTriangle.h \
    Test/TestFunctions.h \
    TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.h \
    TriangularSurfaceExamples/CubicBezierTriangle3.h \
    TriangularSurfaceExamples/QuarticPolinomialTriangle3.h \
    TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.h \
    TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.h \
    SpecificStructures/HalfEdge.h \
    LinearCombinationExamples/AlgebraicTrigonometricCurve3.h \
    SpecificStructures/TriangulatedHalfEdgeDataStructure3.h

SOURCES += \
    Core/GenericCurves3.cpp \
    Core/Lights.cpp \
    Core/LinearCombination3.cpp \
    Core/Materials.cpp \
    Core/RealSquareMatrices.cpp \
    Core/TensorProductSurfaces3.cpp \
    Core/TriangularSurface3.cpp \
    Core/TriangulatedMeshes3.cpp \
    Cyclic/CyclicCurves3.cpp \
    FourPointBasedCurves/FourPointBasedCurve3.cpp \
    GUI/GLWidget.cpp \
    GUI/MainWindow.cpp \
    GUI/SideWidget.cpp \
    LinearCombinationExamples/CubicBezierCurve3.cpp \
    LinearCombinationExamples/QuarticPolinomialCurve3.cpp \
    LinearCombinationExamples/SecondOrderHyperbolicCurve3.cpp \
    LinearCombinationExamples/SecondOrderTrigonometricCurve3.cpp \
    Parametric/ParametricCurves3.cpp \
    Parametric/ParametricSurfaces3.cpp \
    Test/TestFunctions.cpp \
    TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.cpp \
    TriangularSurfaceExamples/CubicBezierTriangle3.cpp \
    TriangularSurfaceExamples/QuarticPolinomialTriangle3.cpp \
    TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.cpp \
    TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.cpp \
    main.cpp \
    Core/ShaderPrograms.cpp \
    Core/GenericCurves3.cpp \
    Core/Lights.cpp \
    Core/LinearCombination3.cpp \
    Core/Materials.cpp \
    Core/RealSquareMatrices.cpp \
    Core/ShaderPrograms.cpp \
    Core/TensorProductSurfaces3.cpp \
    Core/TriangularSurface3.cpp \
    Core/TriangulatedMeshes3.cpp \
    Cyclic/CyclicCurves3.cpp \
    FourPointBasedCurves/FourPointBasedCurve3.cpp \
    GUI/GLWidget.cpp \
    GUI/MainWindow.cpp \
    GUI/SideWidget.cpp \
    LinearCombinationExamples/CubicBezierCurve3.cpp \
    LinearCombinationExamples/QuarticPolinomialCurve3.cpp \
    LinearCombinationExamples/SecondOrderHyperbolicCurve3.cpp \
    LinearCombinationExamples/SecondOrderTrigonometricCurve3.cpp \
    Parametric/ParametricCurves3.cpp \
    Parametric/ParametricSurfaces3.cpp \
    TenPointsBasedTriangles/TenPointsBasedTriangle.cpp \
    Test/TestFunctions.cpp \
    TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.cpp \
    TriangularSurfaceExamples/CubicBezierTriangle3.cpp \
    TriangularSurfaceExamples/QuarticPolinomialTriangle3.cpp \
    TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.cpp \
    TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.cpp \
    main.cpp \
    Core/GenericCurves3.cpp \
    Core/Lights.cpp \
    Core/LinearCombination3.cpp \
    Core/Materials.cpp \
    Core/RealSquareMatrices.cpp \
    Core/ShaderPrograms.cpp \
    Core/TensorProductSurfaces3.cpp \
    Core/TriangularSurface3.cpp \
    Core/TriangulatedMeshes3.cpp \
    Cyclic/CyclicCurves3.cpp \
    FourPointBasedCurves/FourPointBasedCurve3.cpp \
    GUI/GLWidget.cpp \
    GUI/MainWindow.cpp \
    GUI/SideWidget.cpp \
    LinearCombinationExamples/CubicBezierCurve3.cpp \
    LinearCombinationExamples/QuarticPolinomialCurve3.cpp \
    LinearCombinationExamples/SecondOrderHyperbolicCurve3.cpp \
    LinearCombinationExamples/SecondOrderTrigonometricCurve3.cpp \
    Parametric/ParametricCurves3.cpp \
    Parametric/ParametricSurfaces3.cpp \
    TenPointsBasedTriangles/TenPointsBasedTriangle.cpp \
    Test/TestFunctions.cpp \
    TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.cpp \
    TriangularSurfaceExamples/CubicBezierTriangle3.cpp \
    TriangularSurfaceExamples/QuarticPolinomialTriangle3.cpp \
    TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.cpp \
    TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.cpp \
    main.cpp \
    LinearCombinationExamples/AlgebraicTrigonometricCurve3.cpp \
    SpecificStructures/TriangulatedHalfEdgeDataStructure3.cpp
