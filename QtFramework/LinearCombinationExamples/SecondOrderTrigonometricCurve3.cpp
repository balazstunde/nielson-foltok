#include "SecondOrderTrigonometricCurve3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    SecondOrderTrigonometricCurve3::SecondOrderTrigonometricCurve3(GLdouble beta, DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag):
        FourPointBasedCurve3(0.0, beta, start, end, tangent_start, tangent_end, data_usage_flag),
        _beta(beta)
    {
        _c0 = (1 / pow(sin(_beta/2), 4));
        _c1 = 4 * _c0 * cos(_beta / 2);
        _c2 = _c0 * (1 + 2 * pow(cos(_beta/2), 2));

        RowMatrix<TriangularMatrix<GLdouble>> integrals(4);
        for(GLuint i = 0; i < 4; i++) {
            integrals[i].ResizeRows(4);
        }

        integrals[0].FillWithZeros();

        integrals[1].FillWithZeros();
        GLdouble cb = cos(_beta), sb = sin(_beta), sb_2 = sin(_beta / 2);
        GLdouble first_fr = 96 * pow(sb_2, 8);
        GLdouble second_fr = 6 * (1 - 4 * cb + 6 * pow(cb, 2) - 4 * pow(cb, 3) + pow(cb, 4));
        integrals[1](1,0) = integrals[1](3,2) = ((24 + 4 * cb - 18 * pow(cb, 2) + 5 * pow(cb, 3)) * sb - 3 * _beta * (6 + 2 * cb - 3 * pow(cb, 2))) / first_fr;
        integrals[1](2,0) = integrals[1](3,1) = ((-12 + 24 * cb + 2 * pow(cb, 2) + pow(cb, 3)) * sb + 3 * _beta * (2 - 2 * cb - 5 * pow(cb, 2))) / first_fr;
        integrals[1](1,1) = integrals[1](2,2) = ((-32 - 12 * cb + 34 * pow(cb, 2) - 5 * pow(cb, 3)) * sb + 3 * _beta * (8 + 4 * cb - 5 * pow(cb, 2) - 2 * pow(cb, 3))) / second_fr;
        integrals[1](2,1) = ((20 - 16 * cb - 18 * pow(cb, 2) - pow(cb, 3)) * sb - 3 * _beta * (4 - 7 * pow(cb, 2) - 2 * pow(cb, 3))) / second_fr;

        GLdouble third_fr = second_fr / 2;
        integrals[2].FillWithZeros();
        integrals[2](1,0) = integrals[2](3,2) = (12 + 5 * cb + 9 * pow(cb, 2) - 14 * pow(cb, 3)) * sb - 3 *_beta * (6 + cb - 3 * pow(cb, 2)) / third_fr;
        integrals[2](2,0) = integrals[2](3,1) = (-18 + 21 * cb + 7 * pow(cb, 2) + 2 * pow(cb, 3)) * sb + 3 * _beta * (4 - cb - 7 * pow(cb, 2)) / third_fr;
        integrals[2](2,2) = integrals[2](1,1) = (-16 - 24 * cb + 11 *pow(cb, 2) + 17 * pow(cb, 3)) * sb + 3 * _beta * (10 - 7 * pow(cb, 2) + 2 * cb - pow(cb, 3)) / third_fr;
        integrals[2](2,1) = ((22 - 2 * cb - 27 * pow(cb, 2) - 5 * pow(cb, 3)) * sb - 3 * _beta * (8 - 11 * pow(cb, 2) - pow(cb, 3))) / third_fr;

        _integrals = integrals;

    }

    GLboolean SecondOrderTrigonometricCurve3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const{

        values.ResizeColumns(4);

        GLdouble su = sin(u / 2.0), sbu = sin((_beta - u) / 2.0);

        values[0] = _c0 * pow(sbu, 4);
        values[1] = _c1 * pow(sbu, 3) * su + _c2 * pow(sbu, 2) * pow(su, 2);
        values[2] = _c1 * sbu * pow(su, 3) + _c2 * pow(sbu, 2) * pow(su, 2);
        values[3] = _c0 * pow(su, 4);

        return GL_TRUE;
    }

    GLdouble SecondOrderTrigonometricCurve3::_f0(GLuint order, GLdouble u) const
    {
        GLdouble sbu = sin((_beta - u) / 2.0);
        GLdouble cbu = cos((_beta - u) / 2.0);
        switch (order)
        {
        case 0:
            return _c0 * pow(sbu, 4.0);

        case 1:
            return -2.0 * _c0 * pow(sbu, 3.0) * cbu;

        case 2:
            return -2.0 * _c0 * (-1.5 * pow(sbu, 2.0) * pow(cbu, 2.0) + 0.5 * pow(sbu, 3.0) * sbu);

        default:
            return 0.0;
        }
    }

    GLdouble SecondOrderTrigonometricCurve3::_f1(GLuint order, GLdouble u) const
    {
        GLdouble su = sin(u / 2.0), sbu = sin((_beta - u) / 2.0);
        GLdouble cu = cos(u / 2.0), cbu = cos((_beta - u) / 2.0);
        switch (order)
        {
        case 0:
            return _c1 * pow(sbu, 3.0) * su + _c2 * pow(sbu, 2.0) * pow(su, 2.0);

        case 1:
            return _c1 * (-1.5 * pow(sbu, 2.0) * cbu * su + 0.5 * pow(sbu, 3.0) * cu) +
                   _c2 * (-sbu * cbu * pow(su, 2.0) + pow(sbu, 2.0) * su * cu);

        case 2:
            return _c1 * (-1.5 * (-sbu * pow(cbu, 2.0) * su + 0.5 * pow(sbu, 2.0) * sbu * su + 0.5 * pow(sbu, 2.0) * cbu * cu)
                          + 0.5 *(-1.5 * pow(sbu, 2.0) * cbu * cu - 0.5 * pow(sbu, 3.0) * su)) +
                   _c2 * (-(-0.5 * pow(cbu, 2.0) * pow(su, 2.0) + 0.5 * pow(sbu, 2.0) * pow(su, 2.0) + sbu * cbu * su * cu)
                          -sbu * cbu * su * cu + 0.5 * pow(sbu, 2.0) * pow(cu, 2.0) - 0.5 * pow(sbu, 2.0) * pow(su, 2.0));

        default:
            return 0.0;
        }
    }

    GLboolean SecondOrderTrigonometricCurve3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const{
        if(max_order_of_derivatives > 2){
            return GL_FALSE;
        }

        d.ResizeRows(3);
        d.LoadNullVectors();

        d[0] = _data[0] * _f0(0, u) + _data[1] * _f1(0, u)
                + _data[2] * _f1(0, _beta - u) + _data[3] * _f0(0, _beta - u);

        d[1] = _data[0] * _f0(1, u) + _data[1] * _f1(1, u)
                - _data[2] * _f1(1, _beta - u) - _data[3] * _f0(1, _beta - u);

        d[2] = _data[0] * _f0(2, u) + _data[1] * _f1(2, u)
                + _data[2] * _f1(2, _beta - u) + _data[3] * _f0(2, _beta - u);

        return GL_TRUE;
    }

    SecondOrderTrigonometricCurve3* SecondOrderTrigonometricCurve3::clone() const{
        return new SecondOrderTrigonometricCurve3(*this);
    }
}
