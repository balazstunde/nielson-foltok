#include "SecondOrderHyperbolicCurve3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    SecondOrderHyperbolicCurve3::SecondOrderHyperbolicCurve3(GLdouble beta, DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag):
        FourPointBasedCurve3(0.0, beta, start, end, tangent_start, tangent_end, data_usage_flag),
        _beta(beta)
    {
        _c0 = (1 / pow(sinh(_beta/2), 4));
        _c1 = 4 * _c0 * cosh(_beta / 2);
        _c2 = _c0 * (1 + 2 * pow(cosh(_beta/2), 2));

        RowMatrix<TriangularMatrix<GLdouble>> integrals(4);
        for(GLuint i = 0; i < 4; i++) {
            integrals[i].ResizeRows(4);
        }

        integrals[0].FillWithZeros();

        integrals[1].FillWithZeros();
        GLdouble cb = cosh(_beta), sb = sinh(_beta), sb_2 = sinh(_beta / 2);
        GLdouble s2b = sinh(_beta * 2), s3b = sinh(_beta * 3), s4b = sinh(_beta * 4);
        GLdouble first_fr = 1536 * pow(sb_2, 8);
        GLdouble second_fr = 1 - 4 * cb + 6 * pow(cb, 2) - 4 * pow(cb, 3) + pow(cb, 4);
        integrals[1](1,0) = integrals[1](3,2) = (48 * _beta * (6 + 2 * cb - 3 * pow(cb, 2)) - 312 * sb - 52 * s2b + 72 * s3b - 10 * s4b) / first_fr;
        integrals[1](2,0) = integrals[1](3,1) = (-48 * _beta * (2 - 2 * cb - 5 * pow(cb, 2)) + 184 * sb - 196 * s2b - 8 * s3b - 2 * s4b) / first_fr;
        integrals[1](1,1) = integrals[1](2,2) = (-48 * _beta * (8 + 4 * cb - 5 * pow(cb, 2) - 2 * pow(cb, 3)) + 376 * sb + 116 * s2b - 136 * s3b + 10 * s4b) / (96 * second_fr);
        integrals[1](2,1) = (48 * _beta * (4 - 7 * pow(cb, 2) - 2 * pow(cb, 3)) - 248 * sb + 132 * s2b + 72 * s3b + 2 * s4b) / (96 * second_fr);

        integrals[2].FillWithZeros();
        integrals[2](1,0) = integrals[2](3,2) = (-24 * _beta * (6 + cb - 3 * pow(cb, 2)) + 114 * sb - 8 * s2b + 18 * s3b - 14 * s4b) / (24 * second_fr);
        integrals[2](2,0) = integrals[2](3,1) = (24 * _beta * (4 - cb - 7 * pow(cb, 2)) - 130 * sb + 88 * s2b + 14 * s3b + 2 * s4b) / (24 * second_fr);
        integrals[2](2,2) = integrals[2](1,1) = (48 * _beta * (10 + 2 * cb - 7 * pow(cb, 2) - pow(cb, 3)) - 212 * sb - 124 * s2b + 44 * s3b + 34 * s4b) / (48 * second_fr);
        integrals[2](2,1) = (-48 * _beta * (8 - 11 * pow(cb, 2) - pow(cb, 3)) + 244 * sb - 36 * s2b - 108 * s3b - 10 * s4b) /(48 * second_fr);

        _integrals = integrals;
    }

    GLboolean SecondOrderHyperbolicCurve3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const{

        values.ResizeColumns(4);
        GLdouble su = sinh(u / 2.0), sbu = sinh((_beta - u) / 2.0);

        values[0] = _c0 * pow(sbu, 4);
        values[1] = _c1 * pow(sbu, 3) * su + _c2 * pow(sbu, 2) * pow(su, 2);
        values[2] = _c1 * sbu * pow(su, 3) + _c2 * pow(sbu, 2) * pow(su, 2);
        values[3] = _c0 * pow(su, 4);

        return GL_TRUE;
    }

    GLdouble SecondOrderHyperbolicCurve3::_f0(GLuint order, GLdouble u) const
    {
        GLdouble sbu = sinh((_beta - u) / 2.0);
        GLdouble cbu = cosh((_beta - u) / 2.0);
        switch (order)
        {
        case 0:
            return _c0 * pow(sbu, 4.0);

        case 1:
            return -2.0 * _c0 * pow(sbu, 3.0) * cbu;

        case 2:
            return -2.0 * _c0 * (-1.5 * pow(sbu, 2.0) * pow(cbu, 2.0) - 0.5 * pow(sbu, 3.0) * sbu);

        default:
            return 0.0;
        }
    }

    GLdouble SecondOrderHyperbolicCurve3::_f1(GLuint order, GLdouble u) const
    {
        GLdouble su = sinh(u / 2.0), sbu = sinh((_beta - u) / 2.0);
        GLdouble cu = cosh(u / 2.0), cbu = cosh((_beta - u) / 2.0);
        switch (order)
        {
        case 0:
            return _c1 * pow(sbu, 3.0) * su + _c2 * pow(sbu, 2.0) * pow(su, 2.0);

        case 1:
            return _c1 * (-1.5 * pow(sbu, 2.0) * cbu * su + 0.5 * pow(sbu, 3.0) * cu) +
                   _c2 * (-sbu * cbu * pow(su, 2.0) + pow(sbu, 2.0) * su * cu);

        case 2:
            return _c1 * (-1.5 * (-sbu * pow(cbu, 2.0) * su -0.5 * pow(sbu, 2.0) * sbu * su + 0.5 * pow(sbu, 2.0) * cbu * cu)
                          + 0.5 *(-1.5 * pow(sbu, 2.0) * cbu * cu + 0.5 * pow(sbu, 3.0) * su)) +
                   _c2 * (-(-0.5 * pow(cbu, 2.0) * pow(su, 2.0) - 0.5 * pow(sbu, 2.0) * pow(su, 2.0) + sbu * cbu * su * cu)
                          + (-sbu * cbu * su * cu + 0.5 * pow(sbu, 2.0) * pow(cu, 2.0)) + 0.5 * pow(sbu, 2.0) * pow(su, 2.0));

        default:
            return 0.0;
        }
    }

    GLboolean SecondOrderHyperbolicCurve3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const{
        if(max_order_of_derivatives > 2){
            return GL_FALSE;
        }

        d.ResizeRows(3);
        d.LoadNullVectors();

        d[0] = _data[0] * _f0(0, u) + _data[1] * _f1(0, u)
                + _data[2] * _f1(0, _beta - u) + _data[3] * _f0(0, _beta - u);

        d[1] = _data[0] * _f0(1, u) + _data[1] * _f1(1, u)
                - _data[2] * _f1(1, _beta - u) - _data[3] * _f0(1, _beta - u);

        d[2] = _data[0] * _f0(2, u) + _data[1] * _f1(2, u)
                + _data[2] * _f1(2, _beta - u) + _data[3] * _f0(2, _beta - u);


        return GL_TRUE;
    }

    SecondOrderHyperbolicCurve3* SecondOrderHyperbolicCurve3::clone() const{
        return new SecondOrderHyperbolicCurve3(*this);
    }
}
