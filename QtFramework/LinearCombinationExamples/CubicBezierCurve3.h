#pragma once
#include "../Core/LinearCombination3.h"
#include "../FourPointBasedCurves/FourPointBasedCurve3.h"
#include "../Core/Matrices.h"

namespace cagd {
    class CubicBezierCurve3: public FourPointBasedCurve3{

    private:
        GLdouble _f0(GLuint order, GLdouble u) const;
        GLdouble _f1(GLuint order, GLdouble u) const;

    public:
        //special contructor
        CubicBezierCurve3(DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag = GL_STATIC_DRAW);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const;
        CubicBezierCurve3* clone() const;
    };
}
