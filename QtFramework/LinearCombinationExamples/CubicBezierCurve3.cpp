#include "CubicBezierCurve3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    CubicBezierCurve3::CubicBezierCurve3(DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag):
        FourPointBasedCurve3(0.0, 1, start, end, tangent_start, tangent_end, data_usage_flag)
    {
        RowMatrix<TriangularMatrix<GLdouble>> integrals(4);
        for(GLuint i = 0; i < 4; i++) {
            integrals[i].ResizeRows(4);
        }

        integrals[0].FillWithZeros();

        integrals[1].FillWithZeros();
        integrals[1](1,0) = integrals[1](3,2) = -9.0 / 10.0;
        integrals[1](2,0) = integrals[1](3,1) = -3.0 / 5.0;
        integrals[1](1,1) = integrals[1](2,2) = 6.0 / 5.0;
        integrals[1](2,1) = 3.0 / 10.0;

        integrals[2].FillWithZeros();
        integrals[2](1,0) = integrals[2](2,1) = integrals[2](3,2) = -18;
        integrals[2](2,2) = integrals[2](1,1) = 36;

        _integrals = integrals;
    }

    GLboolean CubicBezierCurve3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const{

        values.ResizeColumns(4);

        values[0] = pow((1 - u), 3);
        values[1] = 3 * u * pow((1 - u), 2);
        values[2] = 3 * pow(u, 2) * (1 - u);
        values[3] = pow(u, 3);

        return GL_TRUE;
    }

    GLdouble CubicBezierCurve3::_f0(GLuint order, GLdouble u) const
    {
        switch (order)
        {
        case 0:
            return pow(1.0 - u, 3.0);

        case 1:
            return -3.0 * pow(1.0 - u, 2.0);

        case 2:
            return 6.0 * (1.0 - u);

        default:
            return 0.0;
        }
    }

    GLdouble CubicBezierCurve3::_f1(GLuint order, GLdouble u) const
    {
        switch (order)
        {
        case 0:
            return 3.0 * u * pow(1.0 - u, 2.0);
        case 1:
            return 3.0 * (pow(1.0 - u, 2.0) -2.0 * u * (1.0 - u));

        case 2:
            return 18.0 * u - 12.0;

        default:
            return 0.0;
        }
    }

    GLboolean CubicBezierCurve3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const{
        if(max_order_of_derivatives > 2){
            return GL_FALSE;
        }

        d.ResizeRows(3);
        d.LoadNullVectors();

        //d^2/du^2c(u) = p[0] * (1 - x) ^ 3 + p[1] * 3 * x * (1 - x) ^ 2 + p[2] * 3 * x ^ 2 * (1 - x) + p[3] * x ^ 3;
        d[0] = _data[0] * _f0(0, u) + _data[1] * _f1(0, u) + _data[2] * _f1(0, 1.0 - u) + _data[3] * _f0(0, 1.0 - u);
        d[1] = _data[0] * _f0(1, u) + _data[1] * _f1(1, u) - _data[2] * _f1(1, 1.0 - u) - _data[3] * _f0(1, 1.0 - u);
        d[2] = _data[0] * _f0(2, u) + _data[1] * _f1(2, u) + _data[2] * _f1(2, 1.0 - u) + _data[3] * _f0(2, 1.0 - u);

        return GL_TRUE;
    }

    CubicBezierCurve3* CubicBezierCurve3::clone() const{
        return new CubicBezierCurve3(*this);
    }
}
