#include "AlgebraicTrigonometricCurve3.h"

#include <iostream>
#include <cmath>

using namespace std;

namespace cagd {
    FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3::FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(GLdouble beta, DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag):
        FourPointBasedCurve3(0.0, beta, start, end, tangent_start, tangent_end, data_usage_flag),
        _beta(beta)
    {
        _c0 = 1 / (_beta - sin(_beta));
        _c1 = 1 / ((_beta - sin(_beta)) * (2 * sin(_beta) - _beta - _beta * cos(_beta)));

        RowMatrix<TriangularMatrix<GLdouble>> integrals(4);
        for(GLuint i = 0; i < 4; i++) {
            integrals[i].ResizeRows(4);
        }

        integrals[0].FillWithZeros();

        integrals[1].FillWithZeros();
        GLdouble cb = cos(_beta), sb = sin(_beta);
        GLdouble s2b = sin(_beta * 2), c2b = cos(_beta * 2);
        GLdouble first_fr = (2 * sb - _beta - _beta * cb) * pow(_beta - sb, 2);
        GLdouble second_fr = pow(2 * sb - _beta - _beta * cb, 2) * pow(_beta - sb, 2);
        integrals[1](1,0) = integrals[1](3,2) = ((-3 * _beta + 6 * sb + 2 * _beta * cb - 3 * s2b + _beta * c2b) * sb) / (4 * first_fr);
        integrals[1](2,0) = integrals[1](3,1) = ((-_beta - sb + pow(_beta, 2) * sb + _beta * cb + cb * sb) * sb) / (2 * first_fr);
        integrals[1](1,1) = integrals[1](2,2) = ((2 * pow(_beta, 3) - 4 * sb - 4 * pow(_beta, 2) * sb + 4 * _beta * cb + 2 * s2b - pow(_beta, 2) * s2b - 4 * _beta * c2b) * pow(sb, 2)) / (4 * second_fr);
        integrals[1](2,1) = ((6 * _beta - 2 * sb - 3 * pow(_beta, 2) * sb - 6 *_beta * cb + pow(_beta, 3) * cb + s2b ) * pow(sb, 2)) / (2 * second_fr);

        integrals[2].FillWithZeros();
        integrals[2](1,0) = integrals[2](3,2) = ((-_beta - 2 * sb + 2 * _beta * cb + s2b - _beta * c2b) * sb) / (4 * first_fr);
        integrals[2](2,0) = integrals[2](3,1) = ((-_beta - sb + pow(_beta, 2) * sb + _beta * cb + cb * sb) * sb) / (2 * first_fr);
        integrals[2](2,2) = integrals[2](1,1) = ((2 * _beta + 2 * pow(_beta, 3) + 4 * sb - 4 * pow(_beta, 2) * sb - 4 * _beta * cb - 2 * s2b + pow(_beta, 2) * s2b + 2 * _beta * c2b) * pow(sb, 2)) / (4 * second_fr);
        integrals[2](2,1) = ((_beta + 2 * sb - pow(_beta, 2) * sb - 2 * _beta * cb + pow(_beta, 3) * cb - s2b + _beta * c2b) * pow(sb, 2) / (2 * second_fr));

        _integrals = integrals;
    }

    GLboolean FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3::BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const{

        values.ResizeColumns(4);

        values[0] = _f0(0, u);
        values[1] = _f1(0, u);
        values[2] = _f1(0, _beta - u);
        values[3] = _f0(0, _beta - u);

        return GL_TRUE;
    }

    GLdouble FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3::_f0(GLuint order, GLdouble u) const
    {
        GLdouble sbu = sin(_beta - u), cbu = cos(_beta - u);
        switch (order)
        {
        case 0:
            return _c0 * (_beta - u - sbu);

        case 1:
            return _c0 * (cbu - 1);

        case 2:
            return _c0 * sbu;

        default:
            return 0.0;
        }
    }

    GLdouble FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3::_f1(GLuint order, GLdouble u) const
    {
        GLdouble sb = sin(_beta), sbu = sin(_beta - u), su = sin(u);
        GLdouble cb = cos(_beta), cbu = cos(_beta - u), cu = cos(u);
        switch (order)
        {
        case 0:
            return _c1 * (u + su + sbu - sb + (_beta - u) * cb - _beta * cbu) * sb;

        case 1:
            return _c1  * sb * (1 + cu - cbu - cb - _beta * sbu);

        case 2:
            return _c1  * sb * (-su - sbu + _beta * cbu);

        default:
            return 0.0;
        }
    }

    GLboolean FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3::CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const{
        if(max_order_of_derivatives > 2){
            return GL_FALSE;
        }

        d.ResizeRows(3);
        d.LoadNullVectors();

        d[0] = _data[0] * _f0(0, u) + _data[1] * _f1(0, u) + _data[2] * _f1(0, _beta - u) + _data[3] * _f0(0, _beta - u);
        d[1] = _data[0] * _f0(1, u) + _data[1] * _f1(1, u) - _data[2] * _f1(1, _beta - u) - _data[3] * _f0(1, _beta - u);
        d[2] = _data[0] * _f0(2, u) + _data[1] * _f1(2, u) + _data[2] * _f1(2, _beta - u) + _data[3] * _f0(2, _beta - u);

        return GL_TRUE;
    }

    FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3* FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3::clone() const {
        return new FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(*this);
    }
}
