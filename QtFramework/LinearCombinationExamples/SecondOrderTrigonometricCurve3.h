#pragma once
#include "../FourPointBasedCurves/FourPointBasedCurve3.h"
#include "../Core/Matrices.h"

namespace cagd {
    class SecondOrderTrigonometricCurve3: public FourPointBasedCurve3{

    protected:
        //_beta (0, pi)
        GLdouble _beta;

        GLdouble _c0, _c1, _c2;

        GLdouble _f0(GLuint order, GLdouble u) const;
        GLdouble _f1(GLuint order, GLdouble u) const;
    public:
        //special and default contructor
        SecondOrderTrigonometricCurve3(GLdouble beta, DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag = GL_STATIC_DRAW);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const;
        SecondOrderTrigonometricCurve3* clone() const;
    };
}
