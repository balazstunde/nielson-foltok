#pragma once
#include "../FourPointBasedCurves/FourPointBasedCurve3.h"
#include "../Core/Matrices.h"

namespace cagd {
    class QuarticPolinomialCurve3: public FourPointBasedCurve3{

    private:
        GLdouble _f0(GLuint order, GLdouble u) const;
        GLdouble _f1(GLuint order, GLdouble u) const;

    public:
        //special and default contructor
        QuarticPolinomialCurve3(DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag = GL_STATIC_DRAW);

        //redeclare and define inherited pure virtual methods
        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const;
        QuarticPolinomialCurve3* clone() const;
    };
}
