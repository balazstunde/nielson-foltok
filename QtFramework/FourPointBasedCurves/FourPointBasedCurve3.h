#pragma once
#include "../Core/LinearCombination3.h"
#include "../Core/Matrices.h"

namespace cagd {
    class FourPointBasedCurve3: public LinearCombination3{
    protected:
        DCoordinate3 _start;
        DCoordinate3 _end;
        DCoordinate3 _tangent_start;
        DCoordinate3 _tangent_end;
        RowMatrix<TriangularMatrix<GLdouble>> _integrals;

    public:
        //special constructor
        FourPointBasedCurve3(GLdouble u_min, GLdouble u_max, DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag = GL_STATIC_DRAW);

        virtual FourPointBasedCurve3* clone() const = 0;
        GLboolean UpdateControlPointsByEnergyOptimization(RowMatrix<GLdouble> theta);
    };
}
