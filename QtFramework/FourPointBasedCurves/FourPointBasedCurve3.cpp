#include "../FourPointBasedCurves/FourPointBasedCurve3.h"

#include <iostream>

using namespace std;

namespace cagd {
    FourPointBasedCurve3::FourPointBasedCurve3(GLdouble u_min, GLdouble u_max, DCoordinate3 start, DCoordinate3 end, DCoordinate3 tangent_start, DCoordinate3 tangent_end, GLenum data_usage_flag):
        LinearCombination3(u_min, u_max, 4, data_usage_flag),
        _start(start),
        _end(end),
        _tangent_start(tangent_start.normalize()),
        _tangent_end(tangent_end.normalize())
    {}

    GLboolean FourPointBasedCurve3::UpdateControlPointsByEnergyOptimization(RowMatrix<GLdouble> theta) {

        _data[0] = _start;
        _data[3] = _end;

        //if there are no theta then the default value is (0, 0, 0)
        if(theta[1] == 0.0 && theta[2] == 0.0){
            _data[1] = _data[2] = DCoordinate3(0.0, 0.0, 0.0);
            return GL_TRUE;
        }
        TriangularMatrix<GLdouble> phi(4);
        for(GLuint k = 0; k < 4; k++){
            for(GLuint l = 0; l <= k; l++){
                phi(k, l) = 0.0;
                for(GLuint r = 1; r <= 2; r++){
                    if(theta[r] == 0.0) continue;
                    phi(k,l) += theta[r] * _integrals[r](k,l);
                }
            }
        }

        GLdouble delta_ro = phi(1,1) * phi(2,2) - pow(_tangent_start * _tangent_end, 2.0) * pow(phi(2,1), 2);
        GLdouble lambda1 = -1 / delta_ro * ((phi(2,2) * ((_start * (phi(1,0) + phi(1,1)) + _end * (phi(2,1) + phi(3,1))) * _tangent_start))
                                             + (-(_tangent_start * _tangent_end) * phi(2,1) * ((_start * (phi(2,0) + phi(2,1)) + _end * (phi(2,2) + phi(3,2))) * _tangent_end)));
        GLdouble lambda2 = -1 / delta_ro * (-(_tangent_start * _tangent_end) * phi(2,1) * ((_start * (phi(1,0) + phi(1,1)) + _end * (phi(2,1) + phi(3,1))) * _tangent_start)
                                            + phi(1,1) * ((_start * (phi(2,0) + phi(2,1)) + _end * (phi(2,2) + phi(3,2))) * _tangent_end));

        _data[1] = _start + lambda1 * _tangent_start;
        _data[2] = _end + lambda2 * _tangent_end;

        return GL_TRUE;
    }

}
