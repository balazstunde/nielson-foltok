#include "../TenPointsBasedTriangles/TenPointsBasedTriangle.h"
#include "../Core/Exceptions.h"
#include <iostream>

using namespace std;

namespace cagd {

    GLdouble nChoosek( GLuint n, GLuint k )
    {
        if (k > n) return 0;
        if (k * 2 > n) k = n-k;
        if (k == 0) return 1;

        GLdouble result = n;
        for( GLuint i = 2; i <= k; ++i ) {
            result *= (n-i+1);
            result /= i;
        }
        return result;
    }

    GLuint matching_as_row_matrix(GLuint i, GLuint j, GLuint k) {
        if(i == 3 && j == 0 && k == 0) return 0;
        if(i == 2 && j == 0 && k == 1) return 1;
        if(i == 2 && j == 1 && k == 0) return 2;
        if(i == 1 && j == 0 && k == 2) return 3;
        if(i == 1 && j == 1 && k == 1) return 4;
        if(i == 1 && j == 2 && k == 0) return 5;
        if(i == 0 && j == 0 && k == 3) return 6;
        if(i == 0 && j == 1 && k == 2) return 7;
        if(i == 0 && j == 2 && k == 1) return 8;
        if(i == 0 && j == 3 && k == 0) return 9;
        throw Exception("Could not match!");
    }

    void matching_as_triangular(GLuint &i, GLuint &j, GLuint k){
        if(i == 3 && j == 0 && k == 0) {i = 0; j = 0; return;}
        if(i == 2 && j == 0 && k == 1) {i = 1; j = 0; return;}
        if(i == 2 && j == 1 && k == 0) {i = 1; j = 1; return;}
        if(i == 1 && j == 0 && k == 2) {i = 2; j = 0; return;}
        if(i == 1 && j == 1 && k == 1) {i = 2; j = 1; return;}
        if(i == 1 && j == 2 && k == 0) {i = 2; j = 2; return;}
        if(i == 0 && j == 0 && k == 3) {i = 3; j = 0; return;}
        if(i == 0 && j == 1 && k == 2) {i = 3; j = 1; return;}
        if(i == 0 && j == 2 && k == 1) {i = 3; j = 2; return;}
        if(i == 0 && j == 3 && k == 0) {i = 3; j = 3; return;}
        throw Exception("Could not match!");
    }

    TenPointsBasedTriangle::TenPointsBasedTriangle(TriangularMatrix<DCoordinate3> border_net, GLdouble beta):
        TriangularSurface3(beta),
        _border_net(border_net)
    {}

    GLboolean TenPointsBasedTriangle::UpdateControlPointByEnergyOptimization(RowMatrix<GLdouble> epsilon) {

//        _border_net(2, 1) = DCoordinate3(0.0, 0.0, 0.0);
        _net = _border_net;

        if(epsilon[1] == 0.0 && epsilon[2] == 0.0){
            return GL_TRUE;
        }

        //calculate the denominator
        GLdouble denominator = 0.0;
        for(GLuint g = 1; g <= 2; g++){
            if(epsilon[g] == 0.0) continue;
            GLdouble sum_z = 0.0;
            for(GLuint z = 0; z <= g; z++){
                sum_z += nChoosek(g,z) * _integrals(z,g-z)[matching_as_row_matrix(1,1,1)];
            }
            denominator += epsilon[g] * sum_z;
        }

        //calculate the nominator of the optimizator equation
        DCoordinate3 numerator(0.0, 0.0, 0.0);
        for(GLuint g = 1; g <= 2; g++){
            if(epsilon[g] == 0.0) continue;
            DCoordinate3 num_sum1(0.0, 0.0, 0.0);
            for(GLuint z = 0; z <= g; z++){
                DCoordinate3 num_sum2(0.0, 0.0, 0.0);
                for(GLuint r = 0; r <= 3 ; r++){
                    if(r == 1) continue;
                    for(GLuint s = 0; s <= 3 - r; s++){
                        GLuint i = r;
                        GLuint j = s;
                        GLuint k = 3 - r - s;
                        matching_as_triangular(i, j, k);
                        GLdouble integ = _integrals(z, g - z)[matching_as_row_matrix(r, s, 3 - r - s)];
                        DCoordinate3 net = _net(i, j);
                        num_sum2 += integ * net;
                    }
                }
                DCoordinate3 num_sum3(0.0, 0.0, 0.0);
                for(GLuint s = 0; s <= 2 ; s++){
                    if(s == 1) continue;
                    GLuint i = 1;
                    GLuint j = s;
                    GLuint k = 2 - s;
                    matching_as_triangular(i, j, k);
                    num_sum3 += _integrals(z, g - z)[matching_as_row_matrix(1, s, 2 - s)] * _net(i, j);
                }

                num_sum1 += nChoosek(g, z) * (num_sum2 + num_sum3);
            }
            numerator += epsilon[g] * num_sum1;
        }

        if(denominator != 0.0){
            _net(2, 1) = -numerator / denominator;
        }

        return GL_TRUE;
    }

    GLboolean TenPointsBasedTriangle::UpdateControlPointByEnergyOptimization(RowMatrix<GLdouble> epsilon, const TriangularMatrix<TriangularMatrix<GLdouble>> &tau) {

        _border_net(2, 1) = DCoordinate3(0.0, 0.0, 0.0);
        _net = _border_net;

        GLdouble denominator1 = 0.0;
        for(GLuint g = 1; g <= 2; g++){
            if(epsilon[g] == 0.0) continue;
            GLdouble sum_z = 0.0;
            for(GLuint z = 0; z <= g; z++){
                sum_z += nChoosek(g,z) * tau(g, z)(2,1);
            }
            denominator1 += epsilon[g] * sum_z;
        }

        DCoordinate3 numerator1(0.0, 0.0, 0.0);
        for(GLuint g = 1; g <= 2; g++){

            DCoordinate3 num_sum1(0.0, 0.0, 0.0);

            for(GLuint z = 0; z <= g; z++){
                DCoordinate3 num_sum2(0.0, 0.0, 0.0);
                for(GLuint r = 0; r <= 3 ; r++){
                    for(GLuint s = 0; s <= r; s++){
                        if (r == 2 && s == 1) continue;
                        num_sum2 += tau(g, z)(r, s) * _net(r, s);
                    }
                }
                num_sum1 += nChoosek(g, z) * num_sum2;
            }
            numerator1 += epsilon[g] * num_sum1;
        }

        _net(2, 1) = -numerator1 / denominator1;
        return GL_TRUE;
    }

}
