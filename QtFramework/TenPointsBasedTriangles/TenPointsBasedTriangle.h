#pragma once
#include "../Core/Matrices.h"
#include "../Core/TriangularSurface3.h"

namespace cagd {
    class TenPointsBasedTriangle: public TriangularSurface3{
    protected:
        TriangularMatrix<DCoordinate3> _border_net;
        UpperLeftTriangularMatrix<RowMatrix<GLdouble>> _integrals;

    public:
        //special constructor
        TenPointsBasedTriangle(TriangularMatrix<DCoordinate3> border_net, GLdouble beta = 1.0);

//        GLboolean UpdateControlPointsByEnergyOptimization(const DCoordinate3 &start, const DCoordinate3 &tangent_start,
//                                                          const DCoordinate3 &end, const DCoordinate3 &end_tangent,
//                                                          const RowMatrix<GLdouble> & theta,
//                                                          const RowMatrix<TriangularMatrix<GLdouble>> &integrals);

        GLboolean UpdateControlPointByEnergyOptimization(RowMatrix<GLdouble> epsilon);
        GLboolean UpdateControlPointByEnergyOptimization(RowMatrix<GLdouble> epsilon, const TriangularMatrix<TriangularMatrix<GLdouble>> &tau);
        //redeclare and define inherited pure virtual methods
        //virtual GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
        //virtual GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives &d) const;
    };
}
