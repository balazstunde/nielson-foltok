#pragma once
#include "../Core/DCoordinates3.h"
#include "../Core/TriangularFaces.h"
#include "../Core/LinearCombination3.h"
#include "../Core/TriangularSurface3.h"
#include "../FourPointBasedCurves/FourPointBasedCurve3.h"
#include "../TenPointsBasedTriangles/TenPointsBasedTriangle.h"
#include "map"

namespace cagd
{
    class TriangulatedHalfEdgeDataStructure3
    {
    public:

        // represents a face of the graph
        class Face
        {
            // input from stream
            friend std::istream& operator >>(std::istream& lhs, Face &rhs);

        public:
            GLuint              _node[3];
            TenPointsBasedTriangle *_C0;     // points to the C^{0} optimal triangular surface patch
            TriangulatedMesh3  *_img_C0; // references the image of the C^{0} optimal triangular surface patch
            TriangulatedMesh3  *_G1;     // points to the image of the final G^{1} quasi-optimal surface
            TriangulatedMesh3  *_G_i_jk;
            TriangulatedMesh3  *_G_j_ki;
            TriangulatedMesh3  *_G_k_ij;

            // constructor
            Face();

            // get node id by value
            GLuint operator [](GLuint i) const;

            // get node id by reference
            GLuint& operator [](GLuint i);

            // destructor: deletes the memory areas referenced by the pointers C0, img_C0, G1
            ~Face();
        };

        // describes an oriented half-edge of the graph
        class HalfEdge
        {
            friend class TriangulatedHalfEdgeDataStructure3;
        public:
            DCoordinate3        *_vertex, *_normal;
            HalfEdge            *_opposite, *_next;
            DCoordinate3        _fnormal;
            DCoordinate3        _binormal1;
            DCoordinate3        _binormal2;
            DCoordinate3        _tangent1;
            DCoordinate3        _tangent2;
            FourPointBasedCurve3  *_boundary;
            GenericCurve3       *_img_boundary;
            Face                *_face;

            // constructor (sets every pointer to nullptr)
            HalfEdge();

            // copy constructor
            HalfEdge(const HalfEdge &he);

            //assignment operator
            HalfEdge& operator =(const HalfEdge& rhs);

            // destructor: deletes the memory areas referenced by the pointers boundary and img_boundary
            ~HalfEdge();
        };

        enum BoundaryType{
            CUBIC_BEZIER,
            QUARTIC_POLYNOMIAL,
            SECOND_ORDER_TRIGONOMETRIC,
            SECOND_ORDER_HYPERBOLIC,
            ALGEBRAIC_TRIGONOMETRIC};

    protected:
        std::vector<DCoordinate3>                       _vertexes;  // input data loaded from OFF
        std::vector<Face>                               _faces;    // input data loaded from OFF
        std::vector<DCoordinate3>                       _normals;  // avareged unit normals calculated
                                                                  // based on the input data

        // left- and rightmost corners of the bounding box (calculated based on input data)
        DCoordinate3                                    _leftmost_vertex;
        DCoordinate3                                    _rightmost_vertex;

        std::map< std::pair<GLint, GLint>, HalfEdge* >  _edges;  // half-edge data structure

    public:

        // copy constructor
//        TriangulatedHalfEdgeDataStructure3(const TriangulatedHalfEdgeDataStructure3 &theds):
//            _vertexes(theds._vertexes),
//            _faces(theds._faces),
//            _normals(theds._normals),
//            _leftmost_vertex(theds._leftmost_vertex),
//            _rightmost_vertex(theds._rightmost_vertex),
//            _edges(theds._edges)
//        {}

        // its behavior is similar to that of the method TriangulatedMesh3::LoadFromOFF, but you should also
        // build the map _edges
        GLboolean LoadFromOFF(const std::string& file_name, GLboolean translate_and_scale_to_unit_cube = GL_FALSE);

        void GenerateBoundaryCurves(
                BoundaryType type, GLdouble beta,
                RowMatrix<GLdouble> theta,
                GLuint max_order_of_derivatives,
                GLuint div_point_count, GLenum usage_flag = GL_STATIC_DRAW);

        void GenerateC0TriangularSurfaces(
                BoundaryType type, GLdouble beta,
                RowMatrix<GLdouble> epsilon,
                GLuint div_point_count, GLenum usage_flag = GL_STATIC_DRAW);

        GLboolean GenerateG1TriangularSurfaces(
                BoundaryType type, GLdouble beta,
                RowMatrix<GLdouble> theta,
                GLuint div_point_count, GLuint weight_function, GLenum usage_flag = GL_STATIC_DRAW);
        GLboolean CalculateAverageNormals();

        // rendering methods, you may define other ones as well
        GLboolean RenderCurves(GLenum render_mode = GL_LINE_STRIP);
        GLboolean RenderHalfEdges();
        GLboolean RenderFaces();

        GLboolean RenderC0Meshes(GLenum render_mode = GL_TRIANGLES);
        GLboolean RenderC0ControlNets();
        GLboolean RenderG1Meshes(GLenum render_mode = GL_TRIANGLES);
        GLboolean RenderG1MeshesAsPointCloud(GLuint vertex_number, GLuint div_point_count, GLenum render_mode = GL_POINTS);
        GLboolean RenderVertexNormals(GLdouble scale = 1.0);//?
        GLboolean RenderC0BoundaryNormals(GLuint div_point_count, GLdouble scale = 1.0);
        GLboolean RenderG1BoundaryNormals(GLuint div_point_count, GLdouble scale = 1.0);
        GLboolean RenderApproximatedUnitTangents(GLdouble scale = 1.0);

        std::vector<DCoordinate3> GetVertexes();

        // clean-up methods
        GLvoid DeleteBoundaryCurves();
        GLvoid DeleteC0Surfaces();
        GLvoid DeleteG1Surfaces();
        GLvoid CleanAll();

        // destructor
        ~TriangulatedHalfEdgeDataStructure3();
    };
}
