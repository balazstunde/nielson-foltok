#include "TriangulatedHalfEdgeDataStructure3.h"

#include "../LinearCombinationExamples/AlgebraicTrigonometricCurve3.h"
#include "../LinearCombinationExamples/CubicBezierCurve3.h"
#include "../LinearCombinationExamples/QuarticPolinomialCurve3.h"
#include "../LinearCombinationExamples/SecondOrderHyperbolicCurve3.h"
#include "../LinearCombinationExamples/SecondOrderTrigonometricCurve3.h"

#include "../TriangularSurfaceExamples/AlgebraicTrigonometricTriangle3.h"
#include "../TriangularSurfaceExamples/CubicBezierTriangle3.h"
#include "../TriangularSurfaceExamples/QuarticPolinomialTriangle3.h"
#include "../TriangularSurfaceExamples/SecondOrderHyperbolicTriangle3.h"
#include "../TriangularSurfaceExamples/SecondOrderTrigonometricTriangle3.h"
#include "../Core/Constants.h"

#include "../Core/Exceptions.h"
#include "fstream"
#include "algorithm"
#include "../Core/Materials.h"

using namespace std;

namespace cagd {

TriangulatedHalfEdgeDataStructure3::Face::Face():
    _C0(nullptr), _img_C0(nullptr), _G1(nullptr), _G_i_jk(nullptr), _G_j_ki(nullptr), _G_k_ij(nullptr)
    {}

GLuint TriangulatedHalfEdgeDataStructure3::Face::operator[](GLuint i) const{
    return _node[i];
}

GLuint& TriangulatedHalfEdgeDataStructure3::Face::operator[](GLuint i) {
    return _node[i];
}

std::istream& operator >>(std::istream& lhs, TriangulatedHalfEdgeDataStructure3::Face &rhs){
    GLuint dim;
    lhs >> dim;
    if(dim == 3){
        for (GLuint i = 0; i < 3; ++i)
            lhs  >> rhs[i];
    }
    return lhs;
}

TriangulatedHalfEdgeDataStructure3::Face::~Face(){
    if(_C0){
        delete _C0;
        _C0 = nullptr;
    }

    if(_img_C0){
        delete _img_C0;
        _img_C0 = nullptr;
    }

    if(_G1){
        delete _G1;
        _G1 = nullptr;
    }

    if(_G_i_jk){
        delete _G_i_jk;
        _G_i_jk = nullptr;
    }

    if(_G_j_ki){
        delete _G_j_ki;
        _G_j_ki = nullptr;
    }

    if(_G_k_ij){
        delete _G_k_ij;
        _G_k_ij = nullptr;
    }
}

TriangulatedHalfEdgeDataStructure3::HalfEdge::HalfEdge():
    _vertex(nullptr),
    _normal(nullptr),
    _opposite(nullptr),
    _next(nullptr),
    _boundary(nullptr),
    _img_boundary(nullptr),
    _face(nullptr)
{}

TriangulatedHalfEdgeDataStructure3::HalfEdge::HalfEdge(const HalfEdge& he):
    _fnormal(he._fnormal),
    _binormal1(he._binormal1),
    _binormal2(he._binormal2),
    _tangent1(he._tangent1),
    _tangent2(he._tangent2)
{
//    if(_vertex) {delete _vertex; _vertex = nullptr;}
//    if(_normal) {delete _normal; _normal = nullptr;}
//    if(_opposite) {delete _opposite; _opposite = nullptr;}
//    if(_next) {delete _next; _next = nullptr;}
//    if(_boundary) {delete _boundary; _boundary = nullptr;}
//    if(_img_boundary) {delete _img_boundary; _img_boundary = nullptr;}
//    if(_face) {delete _face; _face = nullptr;}
    _vertex = new DCoordinate3;
    *_vertex = *he._vertex;
    _normal = new DCoordinate3;
    *_normal = *he._normal;
    _opposite = new TriangulatedHalfEdgeDataStructure3::HalfEdge;
    *_opposite = *he._opposite;
    _next = new TriangulatedHalfEdgeDataStructure3::HalfEdge;
    *_next = *he._next;
    _boundary = he._boundary->clone();
    _img_boundary = new GenericCurve3;
    *_img_boundary = *he._img_boundary;
    _face = new TriangulatedHalfEdgeDataStructure3::Face;
    *_face = *he._face;
}

TriangulatedHalfEdgeDataStructure3::HalfEdge& TriangulatedHalfEdgeDataStructure3::HalfEdge::operator=(const HalfEdge& he){
    if (this != &he){
        if(_vertex) {delete _vertex; _vertex = nullptr;}
        if(_normal) {delete _normal; _normal = nullptr;}
        if(_opposite) {delete _opposite; _opposite = nullptr;}
        if(_next) {delete _next; _next = nullptr;}
        if(_boundary) {delete _boundary; _boundary = nullptr;}
        if(_img_boundary) {delete _img_boundary; _img_boundary = nullptr;}
        if(_face) {delete _face; _face = nullptr;}

        _vertex = new DCoordinate3;
        *_vertex = *he._vertex;
        _normal = new DCoordinate3;
        *_normal = *he._normal;
        _opposite = new TriangulatedHalfEdgeDataStructure3::HalfEdge;
        *_opposite = *he._opposite;
        _next = new TriangulatedHalfEdgeDataStructure3::HalfEdge;
        *_next = *he._next;
        _boundary = he._boundary->clone();
        _img_boundary = new GenericCurve3;
        *_img_boundary = *he._img_boundary;
        _face = new TriangulatedHalfEdgeDataStructure3::Face;
        *_face = *he._face;

        _tangent1 = he._tangent1;
        _tangent2 = he._tangent2;
        _fnormal = he._fnormal;
        _binormal1 = he._binormal1;
        _binormal2 = he._binormal2;
    }

    return *this;
}

TriangulatedHalfEdgeDataStructure3::HalfEdge::~HalfEdge(){

    if (_boundary){
        delete _boundary;
        _boundary = nullptr;
    }

    if (_img_boundary){
        delete _img_boundary;
        _img_boundary = nullptr;
    }
}

GLboolean TriangulatedHalfEdgeDataStructure3::LoadFromOFF(const std::string& file_name, GLboolean translate_and_scale_to_unit_cube){
    fstream f(file_name.c_str(), ios_base::in);

    if (!f || !f.good())
        return GL_FALSE;

    // loading the header
    string header;

    f >> header;

    if (header != "OFF")
        return GL_FALSE;

    // loading number of vertices, faces, and edges
    GLuint vertex_count, face_count, edge_count;

    f >> vertex_count >> face_count >> edge_count;

    // allocating memory for vertices, unit normal vectors, texture coordinates, and faces
    _vertexes.resize(vertex_count);
    _normals.resize(vertex_count);
    //_tex.resize(vertex_count);
    _faces.resize(face_count);

    // initializing the leftmost and rightmost corners of the bounding box
    _leftmost_vertex.x() = _leftmost_vertex.y() = _leftmost_vertex.z() = numeric_limits<GLdouble>::max();
    _rightmost_vertex.x() = _rightmost_vertex.y() = _rightmost_vertex.z() = -numeric_limits<GLdouble>::max();

    // loading vertices and correcting the leftmost and rightmost corners of the bounding box
    for (vector<DCoordinate3>::iterator vit = _vertexes.begin(); vit != _vertexes.end(); ++vit)
    {
        f >> *vit;

        if (vit->x() < _leftmost_vertex.x())
            _leftmost_vertex.x() = vit->x();
        if (vit->y() < _leftmost_vertex.y())
            _leftmost_vertex.y() = vit->y();
        if (vit->z() < _leftmost_vertex.z())
            _leftmost_vertex.z() = vit->z();

        if (vit->x() > _rightmost_vertex.x())
            _rightmost_vertex.x() = vit->x();
        if (vit->y() > _rightmost_vertex.y())
            _rightmost_vertex.y() = vit->y();
        if (vit->z() > _rightmost_vertex.z())
            _rightmost_vertex.z() = vit->z();
    }

    // if we do not want to preserve the original positions and coordinates of vertices
    if (translate_and_scale_to_unit_cube)
    {
        GLdouble scale = 1.0 / max(_rightmost_vertex.x() - _leftmost_vertex.x(),
                                   max(_rightmost_vertex.y() - _leftmost_vertex.y(),
                                       _rightmost_vertex.z() - _leftmost_vertex.z()));

        DCoordinate3 middle(_leftmost_vertex);
        middle += _rightmost_vertex;
        middle *= 0.5;
        for (vector<DCoordinate3>::iterator vit = _vertexes.begin(); vit != _vertexes.end(); ++vit)
        {
            *vit -= middle;
            *vit *= scale;
        }
    }

    // loading faces
    for (vector<Face>::iterator fit = _faces.begin(); fit != _faces.end(); ++fit)
        f >> *fit;

    // calculating average unit normal vectors associated with vertices
    for (vector<Face>::const_iterator fit = _faces.begin(); fit != _faces.end(); ++fit)
    {
        DCoordinate3 n = _vertexes[(*fit)[1]];
        n -= _vertexes[(*fit)[0]];

        DCoordinate3 p = _vertexes[(*fit)[2]];
        p -= _vertexes[(*fit)[0]];

        n ^= p;

        for (GLuint node = 0; node < 3; ++node)
            _normals[(*fit)[node]] += n;
    }

    for (vector<DCoordinate3>::iterator nit = _normals.begin(); nit != _normals.end(); ++nit)
        nit->normalize();

    f.close();

    for (vector<Face>::iterator fit = _faces.begin(); fit != _faces.end(); ++fit)
    {
        for (GLuint i = 0; i < 3; ++i)
        {
            GLuint start = i;
            GLuint end   = (i + 1) % 3;

            pair<GLint, GLint> index_pair((*fit)[start], (*fit)[end]);

            _edges[index_pair]          = new HalfEdge();

            _edges[index_pair]->_vertex = &_vertexes[(*fit)[start]];
            _edges[index_pair]->_normal = &_normals[(*fit)[start]];

            _edges[index_pair]->_face   = &(*fit);
            _edges[index_pair]->_fnormal = -_normals[(*fit)[start]];
        }

        for (GLuint i = 0; i < 3; ++i)
        {
            GLuint start = i;
            GLuint end   = (i + 1) % 3;

            pair<GLint, GLint> index_pair((*fit)[start], (*fit)[end]);

            GLuint next_start = (start + 1) % 3;
            GLuint next_end   = (end + 1) % 3;

            pair<GLint, GLint> next_index_pair((*fit)[next_start], (*fit)[next_end]);

            _edges[index_pair]->_next = _edges[next_index_pair];
            //here I already have _next property so I can use it -> calculate the binormal
            _edges[index_pair]->_binormal1 = ((*_edges[index_pair]->_next->_vertex - *_edges[index_pair]->_vertex) ^ _edges[index_pair]->_fnormal) / ((*_edges[index_pair]->_next->_vertex - *_edges[index_pair]->_vertex) ^ _edges[index_pair]->_fnormal).length();
            _edges[index_pair]->_binormal2 = ((*_edges[index_pair]->_vertex - *_edges[index_pair]->_next->_vertex) ^ _edges[index_pair]->_next->_fnormal) / ((*_edges[index_pair]->_vertex - *_edges[index_pair]->_next->_vertex) ^ _edges[index_pair]->_next->_fnormal).length();
            //t(i,j) = fi x b(i,j)
            _edges[index_pair]->_tangent1 = _edges[index_pair]->_fnormal ^ _edges[index_pair]->_binormal1;
            //t(j,i) = fi x b(j,i)
            _edges[index_pair]->_tangent2 = _edges[index_pair]->_next->_fnormal ^ _edges[index_pair]->_binormal2;
            pair<GLuint, GLuint> opposite_index_pair((*fit)[end], (*fit)[start]);

            if (_edges.find(opposite_index_pair) != _edges.end())
            {
                _edges[index_pair]->_opposite = _edges[opposite_index_pair];
                _edges[opposite_index_pair]->_opposite = _edges[index_pair];
            }
            else
            {
                _edges[index_pair]->_opposite = nullptr;
            }
        }
    }

    return GL_TRUE;
}

void TriangulatedHalfEdgeDataStructure3::GenerateBoundaryCurves(BoundaryType type, GLdouble beta, RowMatrix<GLdouble> theta, GLuint max_order_of_derivatives, GLuint div_point_count, GLenum usage_flag){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    typedef std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator miterator;
    GLboolean aborted = GL_FALSE;

    for (it = _edges.begin(); it != _edges.end(); it++){
        switch (type) {
        case CUBIC_BEZIER: it->second->_boundary = new CubicBezierCurve3(*it->second->_vertex, *it->second->_next->_vertex, it->second->_tangent1, it->second->_tangent2, usage_flag); break;
        case QUARTIC_POLYNOMIAL: it->second->_boundary = new QuarticPolinomialCurve3(*it->second->_vertex, *it->second->_next->_vertex, it->second->_tangent1, it->second->_tangent2, usage_flag); break;
        case ALGEBRAIC_TRIGONOMETRIC: it->second->_boundary = new FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(beta, *it->second->_vertex, *it->second->_next->_vertex, it->second->_tangent1, it->second->_tangent2, usage_flag); break;
        case SECOND_ORDER_HYPERBOLIC: it->second->_boundary = new SecondOrderHyperbolicCurve3(beta, *it->second->_vertex, *it->second->_next->_vertex, it->second->_tangent1, it->second->_tangent2, usage_flag); break;
        case SECOND_ORDER_TRIGONOMETRIC: it->second->_boundary = new SecondOrderTrigonometricCurve3(beta, *it->second->_vertex, *it->second->_next->_vertex, it->second->_tangent1, it->second->_tangent2, usage_flag); break;
        }

        if (!it->second->_boundary) {
           for (miterator i = _edges.begin(); it != it; i++){
               delete i->second->_boundary;
               i->second->_boundary = nullptr;
           }
           throw Exception("Could not create one of the boundary curves!");
        }

        it->second->_boundary->UpdateControlPointsByEnergyOptimization(theta);

    }

    #pragma omp parallel for
    for(int i = 0; i < static_cast<int>(_edges.size()); i++) {
        #pragma omp flush (aborted)
        if(!aborted){
            auto it = _edges.begin();
            advance(it, i);

            it->second->_img_boundary = it->second->_boundary->GenerateImage(max_order_of_derivatives, div_point_count, usage_flag);

            if (!it->second->_img_boundary) {
                aborted = GL_TRUE;
                #pragma omp flush (aborted)
            }
        }
    }

    if(aborted){
        DeleteBoundaryCurves();
        throw Exception("Something went wrong in GenerateBoundaryCurves method.");
    }

    for (miterator it = _edges.begin(); it != _edges.end(); it++) {
        if (!it->second->_img_boundary->UpdateVertexBufferObjects()) {
            DeleteBoundaryCurves();
            throw Exception("Could not update the vertex buffer objects of one of the cubic boundary curve's image!");
        }
    }
}

void TriangulatedHalfEdgeDataStructure3::GenerateC0TriangularSurfaces(BoundaryType type, GLdouble beta, RowMatrix<GLdouble> epsilon, GLuint div_point_count, GLenum usage_flag){

    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    typedef std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator miterator;
    GLboolean aborted = GL_FALSE;
    for ( it = _edges.begin(); it != _edges.end(); it++ ) {

        TriangularMatrix<DCoordinate3> border_net;
        border_net.ResizeRows(4);

        border_net(0, 0) = (*it->second->_boundary)[3];
        border_net(1, 1) = (*it->second->_boundary)[2];
        border_net(2, 2) = (*it->second->_boundary)[1];
        border_net(3, 3) = (*it->second->_boundary)[0];

        border_net(1, 0) = (*it->second->_next->_boundary)[1];
        border_net(2, 0) = (*it->second->_next->_boundary)[2];
        border_net(3, 0) = (*it->second->_next->_boundary)[3];

        border_net(3, 1) = (*it->second->_next->_next->_boundary)[1];
        border_net(3, 2) = (*it->second->_next->_next->_boundary)[2];

        border_net(2, 1) = DCoordinate3(0.0, 0.0, 0.0);

        if(it->second->_next->_face->_C0){
            *it->second->_face->_C0 = *it->second->_next->_face->_C0;
            *it->second->_face->_img_C0 = *it->second->_next->_face->_img_C0;
        }else{
            if(it->second->_next->_next->_face->_C0){
                *it->second->_face->_C0 = *it->second->_next->_next->_face->_C0;
                *it->second->_face->_img_C0 = *it->second->_next->_face->_img_C0;
            }else{
                switch (type) {
                case CUBIC_BEZIER: it->second->_face->_C0 = new CubicBezierTriangle3(border_net); break;
                case QUARTIC_POLYNOMIAL: it->second->_face->_C0 = new QuarticPolinomialTriangle3(border_net); break;
                case ALGEBRAIC_TRIGONOMETRIC: it->second->_face->_C0 = new AlgebraicTrigonometricTriangle3(border_net, beta); break;
                case SECOND_ORDER_HYPERBOLIC: it->second->_face->_C0 = new SecondOrderTrigonometricTriangle3(border_net, beta); break; //there isn't implemented the hyperbolic optimalization
                case SECOND_ORDER_TRIGONOMETRIC: it->second->_face->_C0 = new SecondOrderTrigonometricTriangle3(border_net, beta); break;
                }

                if (!it->second->_face->_C0){
                    DeleteC0Surfaces();
                    throw Exception("Could not create the C0 triangle!");
                }else{
                    it->second->_face->_C0->UpdateControlPointByEnergyOptimization(epsilon);
                }

                if (!(*it->second->_face->_C0).UpdateVertexBufferObjectsOfData()){
                    DeleteC0Surfaces();
                    throw Exception("Could not update the vertex buffer object of the C0 triangle's control polygon!");
                }
            }
        }
    }

    #pragma omp parallel for
    for(int i = 0; i < static_cast<int>(_edges.size()); i++) {
        #pragma omp flush (aborted)
        if(!aborted){
            auto it = _edges.begin();
            advance(it, i);
            it->second->_face->_img_C0 = it->second->_face->_C0->GenerateImage(div_point_count, usage_flag);

            if (!it->second->_face->_img_C0){
                aborted = GL_TRUE;
                #pragma omp flush (aborted)
            }
        }
    }

    if(aborted){
        DeleteC0Surfaces();
        throw Exception("Something went wrong in GenerateC0Surfaces method.");
    }

    for (miterator it = _edges.begin(); it != _edges.end(); it++){
        if (!it->second->_face->_img_C0->UpdateVertexBufferObjects()){
            DeleteC0Surfaces();
            throw Exception("Could not update the vertex buffer objects of the C0 triangle's image!");
        }
    }
}

GLboolean TriangulatedHalfEdgeDataStructure3::GenerateG1TriangularSurfaces(BoundaryType type, GLdouble beta, RowMatrix<GLdouble> theta, GLuint div_point_count, GLuint weight_function, GLenum usage_flag){

    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;

    GLboolean aborted = GL_FALSE;
#pragma omp parallel for
    for (GLint f = 0; f < _faces.size(); f++)
    {

//        cout << f << endl;

        GLint i = static_cast<GLint>(_faces[f][0]);
        GLint j = static_cast<GLint>(_faces[f][1]);
        GLint k = static_cast<GLint>(_faces[f][2]);

        DCoordinate3 p_i = _vertexes[i];
        DCoordinate3 n_i = _normals[i];

        DCoordinate3 p_j = _vertexes[j];
        DCoordinate3 n_j = _normals[j];

        DCoordinate3 p_k = _vertexes[k];
        DCoordinate3 n_k = _normals[k];

        pair<GLint, GLint> index_pair(i, j);



        LinearCombination3 *c_ij = _edges[index_pair]->_boundary;
        LinearCombination3 *c_jk = _edges[index_pair]->_next->_boundary;
        LinearCombination3 *c_ki = _edges[index_pair]->_next->_next->_boundary;

        TriangularSurface3 *s_ijk = _edges[index_pair]->_face->_C0;

        TriangularSurface3 *s_o_ij = _edges[index_pair]->_opposite ? _edges[index_pair]->_opposite->_face->_C0 : nullptr;
        TriangularSurface3 *s_o_jk = _edges[index_pair]->_next->_opposite ? _edges[index_pair]->_next->_opposite->_face->_C0 : nullptr;
        TriangularSurface3 *s_o_ki = _edges[index_pair]->_next->_next->_opposite ? _edges[index_pair]->_next->_next->_opposite->_face->_C0 : nullptr;

        TriangularSurface3 *s_i_jk = nullptr;
        TriangularSurface3 *s_j_ki = nullptr;
        TriangularSurface3 *s_k_ij = nullptr;

        _faces[f]._G1 = new (nothrow) TriangulatedMesh3(div_point_count * (div_point_count + 1) / 2, (div_point_count - 1) * (div_point_count - 1), usage_flag);
        _faces[f]._G_i_jk = new (nothrow) TriangulatedMesh3(div_point_count * (div_point_count + 1) / 2, (div_point_count - 1) * (div_point_count - 1), usage_flag);
        _faces[f]._G_j_ki = new (nothrow) TriangulatedMesh3(div_point_count * (div_point_count + 1) / 2, (div_point_count - 1) * (div_point_count - 1), usage_flag);
        _faces[f]._G_k_ij = new (nothrow) TriangulatedMesh3(div_point_count * (div_point_count + 1) / 2, (div_point_count - 1) * (div_point_count - 1), usage_flag);



        if (!_faces[f]._G1)
        {
//            DeleteG1Surfaces();
//            return GL_FALSE;
            aborted = GL_TRUE;
            #pragma omp flush (aborted)
        }

        TriangulatedMesh3 &G1 = *_faces[f]._G1;
        TriangulatedMesh3 &G_i_jk = *_faces[f]._G_i_jk;
        TriangulatedMesh3 &G_j_ki = *_faces[f]._G_j_ki;
        TriangulatedMesh3 &G_k_ij = *_faces[f]._G_k_ij;

        GLuint current_face = 0;

//        GLdouble EPS = 1.0e-4;

        DCoordinate3 p(1.0 - EPS, EPS, EPS), q(EPS, 1.0 - EPS, EPS), r(EPS, EPS, 1.0 - EPS);

        GLdouble ds = 1.0 / static_cast<GLdouble>(div_point_count - 1);

        RowMatrix<DCoordinate3> n_jk(div_point_count),
                                n_ki(div_point_count),
                                n_ij(div_point_count);



#pragma omp parallel for
        for (GLint ac = 0; ac < static_cast <GLint>(div_point_count * (div_point_count + 1) / 2); ac++)
        {
            #pragma omp flush (aborted)
            if(!aborted){
                GLint a = static_cast <GLint>(floor((sqrt(1 + 8 * ac) - 1.0) / 2.0));
                GLint c = ac - (a * (a + 1) / 2);
                //cout << "\t"<< a << "\t" << c << endl;
                GLdouble s = min(a * ds, 1.0);

                DCoordinate3 d = p + (q - p) * s;
                DCoordinate3 e = p + (r - p) * s;

                GLdouble dt = a ? 1.0 / static_cast<GLdouble>(a) : 0.0;

                GLdouble     t = min(c * dt, 1.0 - EPS);
                DCoordinate3 b = d + (e - d) * t;

                GLuint index_lower[3];

                index_lower[0] = static_cast<GLuint>(a * (a + 1) / 2 + c);
                index_lower[1] = static_cast<GLuint>((a + 1) * (a + 2) / 2 + c);
                index_lower[2] = static_cast<GLuint>((a + 1) * (a + 2) / 2 + c + 1);

                GLuint index_upper[3];

                index_upper[0] = static_cast<GLuint>(a * (a + 1) / 2 + c);
                index_upper[1] = static_cast<GLuint>((a + 1) * (a + 2) / 2 + c + 1);
                index_upper[2] = static_cast<GLuint>(a * (a + 1) / 2 + c + 1);

    //                b[2] = 1.0 - EPS - b[0] - b[1];
                //--------------------------------------------------------------------------
                LinearCombination3::Derivatives d_g_i_jk;

                // i - (j, k)
                {
                    GLdouble u = max(0.0, min(beta * b[2] / (1.0 - b[0]), beta));

                    pair<GLint, GLint> j_k(j, k);

                    switch (type)
                    {
                    case CUBIC_BEZIER:
                        s_i_jk = new (nothrow) CubicBezierTriangle3();
                        break;
                    case QUARTIC_POLYNOMIAL:
                        s_i_jk = new (nothrow) QuarticPolinomialTriangle3();
                        break;
                    case ALGEBRAIC_TRIGONOMETRIC:
                        s_i_jk = new (nothrow) AlgebraicTrigonometricTriangle3(beta);
                        break;
                    case SECOND_ORDER_TRIGONOMETRIC:
                        s_i_jk = new (nothrow) SecondOrderTrigonometricTriangle3(beta);
                        break;
                    default:
                        break;
                    }

                    if (s_i_jk)
                    {
                        (*s_i_jk)(0, 0) = (*c_ij)[0]; // p_i
                        (*s_i_jk)(1, 0) = (*c_ij)[1];
                        (*s_i_jk)(2, 0) = (*c_ij)[2];

                        (*s_i_jk)(3, 0) = (*c_jk)[0]; // p_j
                        (*s_i_jk)(3, 1) = (*c_jk)[1];
                        (*s_i_jk)(3, 2) = (*c_jk)[2];

                        (*s_i_jk)(3, 3) = (*c_ki)[0]; // p_k
                        (*s_i_jk)(2, 2) = (*c_ki)[1];
                        (*s_i_jk)(1, 1) = (*c_ki)[2];

                        (*s_i_jk)(2, 1) = (*s_ijk)(2, 1);
                    }

                    if (!s_i_jk)
                    {
//                        DeleteG1Surfaces();
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

                    TriangularSurface3::PartialDerivatives pd_s_i_jk;
                    if (!s_i_jk->CalculatePartialDerivatives(0.0, beta - u, pd_s_i_jk))
                    {
//                        DeleteG1Surfaces();
//                        delete s_i_jk;
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

                    TriangularSurface3 *s_x_kj = nullptr;
                    TriangularSurface3::PartialDerivatives pd_s_x_kj;

                    DCoordinate3 c_jk_u = pd_s_i_jk(0, 0);

                    DCoordinate3 n_jk_u = pd_s_i_jk(1, 0);
                    n_jk_u ^= pd_s_i_jk(1, 1);
                    n_jk_u.normalize();

                    if (s_o_jk)
                    {
                        LinearCombination3 *c_xk = _edges[j_k]->_opposite->_next->_next->_boundary;
                        LinearCombination3 *c_kj = _edges[j_k]->_opposite->_boundary;
                        LinearCombination3 *c_jx = _edges[j_k]->_opposite->_next->_boundary;

                        switch (type)
                        {
                        case CUBIC_BEZIER:
                            s_x_kj = new (nothrow) CubicBezierTriangle3();
                            break;
                        case QUARTIC_POLYNOMIAL:
                            s_x_kj = new (nothrow) QuarticPolinomialTriangle3();
                            break;
                        case ALGEBRAIC_TRIGONOMETRIC:
                            s_x_kj = new (nothrow) AlgebraicTrigonometricTriangle3(beta);
                            break;
                        case SECOND_ORDER_TRIGONOMETRIC:
                            s_x_kj = new (nothrow) SecondOrderTrigonometricTriangle3(beta);
                            break;
                        default:
                            break;
                        }

                        if (!s_x_kj)
                        {
                            // avoid memory leaks
//                            DeleteG1Surfaces();
//                            delete s_i_jk;
//                            return GL_FALSE;
                            aborted = GL_TRUE;
                            #pragma omp flush (aborted)
                        }

                        (*s_x_kj)(0, 0) = (*c_xk)[0]; // p_x
                        (*s_x_kj)(1, 0) = (*c_xk)[1];
                        (*s_x_kj)(2, 0) = (*c_xk)[2];

                        (*s_x_kj)(3, 0) = (*c_kj)[0]; // p_k
                        (*s_x_kj)(3, 1) = (*c_kj)[1];
                        (*s_x_kj)(3, 2) = (*c_kj)[2];

                        (*s_x_kj)(3, 3) = (*c_jx)[0]; // p_j
                        (*s_x_kj)(2, 2) = (*c_jx)[1];
                        (*s_x_kj)(1, 1) = (*c_jx)[2];

                        (*s_x_kj)(2, 1) = (*s_o_jk)(2, 1);

                        if (!s_x_kj->CalculatePartialDerivatives(0.0, u, pd_s_x_kj))
                        {
                            // avoid memory leaks
//                            DeleteG1Surfaces();
//                            delete s_i_jk;
//                            delete s_x_kj;
//                            return GL_FALSE;
                            aborted = GL_TRUE;
                            #pragma omp flush (aborted)
                        }

                        DCoordinate3 n_kj_u = pd_s_x_kj(1, 0);
                        n_kj_u ^= pd_s_x_kj(1, 1);
                        n_kj_u.normalize();

                        n_jk_u += n_kj_u;
                        n_jk_u.normalize();

                        delete s_x_kj;
                    }

                    if (a == static_cast<GLint>(div_point_count - 1))
                    {
                        n_jk[static_cast<GLuint>(c)] = n_jk_u;
                    }

                    DCoordinate3 f_i = -n_i;

                    DCoordinate3 b_i_jk_u = (c_jk_u - p_i);
                    b_i_jk_u ^= f_i;
                    b_i_jk_u.normalize();

                    DCoordinate3 t_i_jk_u = f_i;
                    t_i_jk_u ^= b_i_jk_u;
                    t_i_jk_u.normalize();

                    DCoordinate3 f_jk_u = -n_jk_u;
                    DCoordinate3 b_jk_i_u = (p_i - c_jk_u);
                    b_jk_i_u ^= f_jk_u;
                    b_jk_i_u.normalize();

                    DCoordinate3 t_jk_i_u = f_jk_u;
                    t_jk_i_u ^= b_jk_i_u;
                    t_jk_i_u.normalize();

                    FourPointBasedCurve3 *g_i_jk = nullptr;

                    switch (type)
                    {
                    case CUBIC_BEZIER:
                        g_i_jk = new (nothrow) CubicBezierCurve3(p_i, c_jk_u, t_i_jk_u, t_jk_i_u);
                        break;
                    case QUARTIC_POLYNOMIAL:
                        g_i_jk = new (nothrow) QuarticPolinomialCurve3(p_i, c_jk_u, t_i_jk_u, t_jk_i_u);
                        break;
                    case ALGEBRAIC_TRIGONOMETRIC:
                        g_i_jk = new (nothrow) FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(beta, p_i, c_jk_u, t_i_jk_u, t_jk_i_u);
                        break;
                    case SECOND_ORDER_TRIGONOMETRIC:
                        g_i_jk = new (nothrow) SecondOrderTrigonometricCurve3(beta, p_i, c_jk_u, t_i_jk_u, t_jk_i_u);
                        break;
                    default: break;
                    }

                    if (!g_i_jk || !g_i_jk->UpdateControlPointsByEnergyOptimization(theta) ||
                        !g_i_jk->CalculateDerivatives(0, min(beta * (1.0 - b[0]), beta), d_g_i_jk))
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        delete s_i_jk;
//                        delete g_i_jk;
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

//                    delete s_i_jk;
                }

                // j - (k, i)
                LinearCombination3::Derivatives d_g_j_ki;

                {
                    GLdouble u = max(0.0, min(beta * b[0] / (1.0 - b[1]), beta));
                    pair<GLint, GLint> k_i(k, i);

                    switch (type)
                    {
                    case CUBIC_BEZIER:
                        s_j_ki = new (nothrow) CubicBezierTriangle3();
                        break;
                    case QUARTIC_POLYNOMIAL:
                        s_j_ki = new (nothrow) QuarticPolinomialTriangle3();
                        break;
                    case ALGEBRAIC_TRIGONOMETRIC:
                        s_j_ki = new (nothrow) AlgebraicTrigonometricTriangle3(beta);
                        break;
                    case SECOND_ORDER_TRIGONOMETRIC:
                        s_j_ki = new (nothrow) SecondOrderTrigonometricTriangle3(beta);
                        break;
                    default:
                        break;
                    }

                    if (s_j_ki)
                    {
                        (*s_j_ki)(0, 0) = (*c_jk)[0]; // p_j
                        (*s_j_ki)(1, 0) = (*c_jk)[1];
                        (*s_j_ki)(2, 0) = (*c_jk)[2];

                        (*s_j_ki)(3, 0) = (*c_ki)[0]; // p_k
                        (*s_j_ki)(3, 1) = (*c_ki)[1];
                        (*s_j_ki)(3, 2) = (*c_ki)[2];

                        (*s_j_ki)(3, 3) = (*c_ij)[0]; // p_i
                        (*s_j_ki)(2, 2) = (*c_ij)[1];
                        (*s_j_ki)(1, 1) = (*c_ij)[2];

                        (*s_j_ki)(2, 1) = (*s_ijk)(2, 1);
                    }

                    if (!s_j_ki)
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

                    TriangularSurface3::PartialDerivatives pd_s_j_ki;
                    if (!s_j_ki->CalculatePartialDerivatives(0.0, beta - u, pd_s_j_ki))
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        delete s_j_ki;
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

                    TriangularSurface3 *s_y_ik = nullptr;
                    TriangularSurface3::PartialDerivatives pd_s_y_ik;

                    DCoordinate3 c_ki_u = pd_s_j_ki(0, 0);

                    DCoordinate3 n_ki_u = pd_s_j_ki(1, 0);
                    n_ki_u ^= pd_s_j_ki(1, 1);
                    n_ki_u.normalize();

                    if (s_o_ki)
                    {
                        LinearCombination3 *c_yi = _edges[k_i]->_opposite->_next->_next->_boundary;
                        LinearCombination3 *c_ik = _edges[k_i]->_opposite->_boundary;
                        LinearCombination3 *c_ky = _edges[k_i]->_opposite->_next->_boundary;

                        switch (type)
                        {
                        case CUBIC_BEZIER:
                            s_y_ik = new (nothrow) CubicBezierTriangle3();
                            break;
                        case QUARTIC_POLYNOMIAL:
                            s_y_ik = new (nothrow) QuarticPolinomialTriangle3();
                            break;
                        case ALGEBRAIC_TRIGONOMETRIC:
                            s_y_ik = new (nothrow) AlgebraicTrigonometricTriangle3(beta);
                            break;
                        case SECOND_ORDER_TRIGONOMETRIC:
                            s_y_ik = new (nothrow) SecondOrderTrigonometricTriangle3(beta);
                            break;
                        default:
                            break;
                        }

                        if (!s_y_ik)
                        {
                            // avoid memory leaks
//                            DeleteG1Surfaces();
//                            delete s_j_ki;
//                            return GL_FALSE;
                            aborted = GL_TRUE;
                            #pragma omp flush (aborted)
                        }

                        (*s_y_ik)(0, 0) = (*c_yi)[0]; // p_y
                        (*s_y_ik)(1, 0) = (*c_yi)[1];
                        (*s_y_ik)(2, 0) = (*c_yi)[2];

                        (*s_y_ik)(3, 0) = (*c_ik)[0]; // p_i
                        (*s_y_ik)(3, 1) = (*c_ik)[1];
                        (*s_y_ik)(3, 2) = (*c_ik)[2];

                        (*s_y_ik)(3, 3) = (*c_ky)[0]; // p_k
                        (*s_y_ik)(2, 2) = (*c_ky)[1];
                        (*s_y_ik)(1, 1) = (*c_ky)[2];

                        (*s_y_ik)(2, 1) = (*s_o_ki)(2, 1);

                        if (!s_y_ik->CalculatePartialDerivatives(0.0, u, pd_s_y_ik))
                        {
                            // avoid memory leaks
//                            DeleteG1Surfaces();
//                            delete s_j_ki;
//                            delete s_y_ik;
//                            return GL_FALSE;
                            aborted = GL_TRUE;
                            #pragma omp flush (aborted)
                        }

                        DCoordinate3 n_ik_u = pd_s_y_ik(1, 0);
                        n_ik_u ^= pd_s_y_ik(1, 1);
                        n_ik_u.normalize();

                        n_ki_u += n_ik_u;
                        n_ki_u.normalize();

                        delete s_y_ik;
                    }

                    if (c == a)
                    {
                        n_ki[static_cast<GLuint>(c)] = n_ki_u;
                    }

                    DCoordinate3 f_j = -n_j;

                    DCoordinate3 b_j_ki_u = (c_ki_u - p_j);
                    b_j_ki_u ^= f_j;
                    b_j_ki_u.normalize();

                    DCoordinate3 t_j_ki_u = f_j;
                    t_j_ki_u ^= b_j_ki_u;
                    t_j_ki_u.normalize();

                    DCoordinate3 f_ki_u = -n_ki_u;
                    DCoordinate3 b_ki_j_u = (p_j - c_ki_u);
                    b_ki_j_u ^= f_ki_u;
                    b_ki_j_u.normalize();

                    DCoordinate3 t_ki_j_u = f_ki_u;
                    t_ki_j_u ^= b_ki_j_u;
                    t_ki_j_u.normalize();

                    FourPointBasedCurve3 *g_j_ki = nullptr;

                    switch (type)
                    {
                    case CUBIC_BEZIER:
                        g_j_ki = new (nothrow) CubicBezierCurve3(p_j, c_ki_u, t_j_ki_u, t_ki_j_u);
                        break;
                    case QUARTIC_POLYNOMIAL:
                        g_j_ki = new (nothrow) QuarticPolinomialCurve3(p_j, c_ki_u, t_j_ki_u, t_ki_j_u);
                        break;
                    case ALGEBRAIC_TRIGONOMETRIC:
                        g_j_ki = new (nothrow) FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(beta, p_j, c_ki_u, t_j_ki_u, t_ki_j_u);
                        break;
                    case SECOND_ORDER_TRIGONOMETRIC:
                        g_j_ki = new (nothrow) SecondOrderTrigonometricCurve3(beta, p_j, c_ki_u, t_j_ki_u, t_ki_j_u);
                        break;
                    default: break;
                    }

                    if (!g_j_ki || !g_j_ki->UpdateControlPointsByEnergyOptimization(theta) ||
                        !g_j_ki->CalculateDerivatives(0, min(beta * (1.0 - b[1]), beta), d_g_j_ki))
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        delete s_j_ki;
//                        delete g_j_ki;
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

//                    delete s_j_ki;
                }

                // k - (i, j)

                LinearCombination3::Derivatives d_g_k_ij;
                {
                    GLdouble u = max(0.0, min(beta * b[1] / (1.0 - b[2]), beta));
                    pair<GLint, GLint> i_j(i, j);

                    switch (type)
                    {
                    case CUBIC_BEZIER:
                        s_k_ij = new (nothrow) CubicBezierTriangle3();
                        break;
                    case QUARTIC_POLYNOMIAL:
                        s_k_ij = new (nothrow) QuarticPolinomialTriangle3();
                        break;
                    case ALGEBRAIC_TRIGONOMETRIC:
                        s_k_ij = new (nothrow) AlgebraicTrigonometricTriangle3(beta);
                        break;
                    case SECOND_ORDER_TRIGONOMETRIC:
                        s_k_ij = new (nothrow) SecondOrderTrigonometricTriangle3(beta);
                        break;
                    default:
                        break;
                    }

                    if (s_k_ij)
                    {
                        (*s_k_ij)(0, 0) = (*c_ki)[0]; // p_k
                        (*s_k_ij)(1, 0) = (*c_ki)[1];
                        (*s_k_ij)(2, 0) = (*c_ki)[2];

                        (*s_k_ij)(3, 0) = (*c_ij)[0]; // p_i
                        (*s_k_ij)(3, 1) = (*c_ij)[1];
                        (*s_k_ij)(3, 2) = (*c_ij)[2];

                        (*s_k_ij)(3, 3) = (*c_jk)[0]; // p_j
                        (*s_k_ij)(2, 2) = (*c_jk)[1];
                        (*s_k_ij)(1, 1) = (*c_jk)[2];

                        (*s_k_ij)(2, 1) = (*s_ijk)(2, 1);
                    }

                    if (!s_k_ij)
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

                    TriangularSurface3::PartialDerivatives pd_s_k_ij;
                    if (!s_k_ij->CalculatePartialDerivatives(0.0, beta - u, pd_s_k_ij))
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        delete s_k_ij;
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

                    TriangularSurface3 *s_z_ji = nullptr;
                    TriangularSurface3::PartialDerivatives pd_s_z_ji;

                    DCoordinate3 c_ij_u = pd_s_k_ij(0, 0);

                    DCoordinate3 n_ij_u = pd_s_k_ij(1, 0);
                    n_ij_u ^= pd_s_k_ij(1, 1);
                    n_ij_u.normalize();

                    if (s_o_ij)
                    {
                        LinearCombination3 *c_zj = _edges[i_j]->_opposite->_next->_next->_boundary;
                        LinearCombination3 *c_ji = _edges[i_j]->_opposite->_boundary;
                        LinearCombination3 *c_iz = _edges[i_j]->_opposite->_next->_boundary;

                        switch (type)
                        {
                        case CUBIC_BEZIER:
                            s_z_ji = new (nothrow) CubicBezierTriangle3();
                            break;
                        case QUARTIC_POLYNOMIAL:
                            s_z_ji = new (nothrow) QuarticPolinomialTriangle3();
                            break;
                        case ALGEBRAIC_TRIGONOMETRIC:
                            s_z_ji = new (nothrow) AlgebraicTrigonometricTriangle3(beta);
                            break;
                        case SECOND_ORDER_TRIGONOMETRIC:
                            s_z_ji = new (nothrow) SecondOrderTrigonometricTriangle3(beta);
                            break;
                        default: break;
                        }

                        if (!s_z_ji)
                        {
                            // avoid memory leaks
//                            DeleteG1Surfaces();
//                            delete s_k_ij;
//                            return GL_FALSE;
                            aborted = GL_TRUE;
                            #pragma omp flush (aborted)
                        }

                        (*s_z_ji)(0, 0) = (*c_zj)[0]; // p_z
                        (*s_z_ji)(1, 0) = (*c_zj)[1];
                        (*s_z_ji)(2, 0) = (*c_zj)[2];

                        (*s_z_ji)(3, 0) = (*c_ji)[0]; // p_j
                        (*s_z_ji)(3, 1) = (*c_ji)[1];
                        (*s_z_ji)(3, 2) = (*c_ji)[2];

                        (*s_z_ji)(3, 3) = (*c_iz)[0]; // pi
                        (*s_z_ji)(2, 2) = (*c_iz)[1];
                        (*s_z_ji)(1, 1) = (*c_iz)[2];

                        (*s_z_ji)(2, 1) = (*s_o_ij)(2, 1);

                        if (!s_z_ji->CalculatePartialDerivatives(0.0, u, pd_s_z_ji))
                        {
                            // avoid memory leaks
//                            DeleteG1Surfaces();
//                            delete s_k_ij;
//                            delete s_z_ji;
//                            return GL_FALSE;
                            aborted = GL_TRUE;
                            #pragma omp flush (aborted)
                        }

                        DCoordinate3 n_ji_u = pd_s_z_ji(1, 0);
                        n_ji_u ^= pd_s_z_ji(1, 1);
                        n_ji_u.normalize();

                        n_ij_u += n_ji_u;
                        n_ij_u.normalize();

                        delete s_z_ji;
                    }

                    if (c == 0)
                    {
                        n_ij[static_cast<GLuint>(a)] = n_ij_u;
                    }

                    DCoordinate3 f_k = -n_k;

                    DCoordinate3 b_k_ij_u = (c_ij_u - p_k);
                    b_k_ij_u ^= f_k;
                    b_k_ij_u.normalize();

                    DCoordinate3 t_k_ij_u = f_k;
                    t_k_ij_u ^= b_k_ij_u;
                    t_k_ij_u.normalize();

                    DCoordinate3 f_ij_u = -n_ij_u;
                    DCoordinate3 b_ij_k_u = (p_k - c_ij_u);
                    b_ij_k_u ^= f_ij_u;
                    b_ij_k_u.normalize();

                    DCoordinate3 t_ij_k_u = f_ij_u;
                    t_ij_k_u ^= b_ij_k_u;
                    t_ij_k_u.normalize();

                    FourPointBasedCurve3 *g_k_ij = nullptr;

                    switch (type)
                    {
                    case CUBIC_BEZIER:
                        g_k_ij = new (nothrow) CubicBezierCurve3(p_k, c_ij_u, t_k_ij_u, t_ij_k_u);
                        break;
                    case QUARTIC_POLYNOMIAL:
                        g_k_ij = new (nothrow) QuarticPolinomialCurve3(p_k, c_ij_u, t_k_ij_u, t_ij_k_u);
                        break;
                    case ALGEBRAIC_TRIGONOMETRIC:
                        g_k_ij = new (nothrow) FirstOrderAlgebraicTrigonometricNormalizedBbasisFunction3(beta, p_k, c_ij_u, t_k_ij_u, t_ij_k_u);
                        break;
                    case SECOND_ORDER_TRIGONOMETRIC:
                        g_k_ij = new (nothrow) SecondOrderTrigonometricCurve3(beta, p_k, c_ij_u, t_k_ij_u, t_ij_k_u);
                        break;
                    default: break;
                    }

                    if (!g_k_ij || !g_k_ij->UpdateControlPointsByEnergyOptimization(theta) ||
                        !g_k_ij->CalculateDerivatives(0, min(beta * (1.0 - b[2]), beta), d_g_k_ij))
                    {
                        // avoid memory leaks
//                        DeleteG1Surfaces();
//                        delete s_k_ij;
//                        delete g_k_ij;
//                        return GL_FALSE;
                        aborted = GL_TRUE;
                        #pragma omp flush (aborted)
                    }

//                    delete s_k_ij;
                }

                //--------------------------------------------------------------------------

                switch (weight_function) {
                case 0: //G1._vertex[index_lower[0]] = (d_g_i_jk[0] + d_g_j_ki[0] + d_g_k_ij[0]) / 3.0;
                    G1._vertex[index_lower[0]] = (((b[1]*b[2])/(b[0]*b[1]+b[0]*b[2]+b[1]*b[2])) * d_g_i_jk[0] + ((b[0]*b[2])/(b[0]*b[1]+b[0]*b[2]+b[1]*b[2])) * d_g_j_ki[0] + ((b[0]*b[1])/(b[0]*b[1]+b[0]*b[2]+b[1]*b[2])) * d_g_k_ij[0]);
                    break;
                case 1: G1._vertex[index_lower[0]] = (((b[1]*b[2])/(b[0]*b[1]+b[0]*b[2]+b[1]*b[2])) * d_g_i_jk[0] + ((b[0]*b[2])/(b[0]*b[1]+b[0]*b[2]+b[1]*b[2])) * d_g_j_ki[0] + ((b[0]*b[1])/(b[0]*b[1]+b[0]*b[2]+b[1]*b[2])) * d_g_k_ij[0]);
                    break;
                case 2: G1._vertex[index_lower[0]] = (((b[1]*b[1]*b[2]*b[2])/(b[0]*b[0]*b[1]*b[1]+b[0]*b[0]*b[2]*b[2]+b[1]*b[1]*b[2]*b[2])) * d_g_i_jk[0] + ((b[0]*b[0]*b[2]*b[2])/(b[0]*b[0]*b[1]*b[1]+b[0]*b[0]*b[2]*b[2]+b[1]*b[1]*b[2]*b[2])) * d_g_j_ki[0] + ((b[0]*b[0]*b[1]*b[1])/(b[0]*b[0]*b[1]*b[1]+b[0]*b[0]*b[2]*b[2]+b[1]*b[1]*b[2]*b[2])) * d_g_k_ij[0]);
                    break;
                default: G1._vertex[index_lower[0]] = (d_g_i_jk[0] + d_g_j_ki[0] + d_g_k_ij[0]) / 3.0;
                    break;
                }

                G_j_ki._vertex[index_lower[0]] = d_g_j_ki[0];
                G_k_ij._vertex[index_lower[0]] = d_g_k_ij[0];
                G_i_jk._vertex[index_lower[0]] = d_g_i_jk[0];

                #pragma omp critical
                if (a < static_cast <GLint>(div_point_count - 1))
                {
                    G1._face[current_face][0] = index_lower[0];
                    G1._face[current_face][1] = index_lower[1];
                    G1._face[current_face][2] = index_lower[2];

                    G_i_jk._face[current_face][0] = index_lower[0];
                    G_i_jk._face[current_face][1] = index_lower[1];
                    G_i_jk._face[current_face][2] = index_lower[2];

                    G_j_ki._face[current_face][0] = index_lower[0];
                    G_j_ki._face[current_face][1] = index_lower[1];
                    G_j_ki._face[current_face][2] = index_lower[2];

                    G_k_ij._face[current_face][0] = index_lower[0];
                    G_k_ij._face[current_face][1] = index_lower[1];
                    G_k_ij._face[current_face][2] = index_lower[2];

                    ++current_face;
                }

                #pragma omp critical
                if (a < static_cast <GLint>(div_point_count) - 1 && c < a)
                {
                    G1._face[current_face][0] = index_upper[0];
                    G1._face[current_face][1] = index_upper[1];
                    G1._face[current_face][2] = index_upper[2];

                    G_i_jk._face[current_face][0] = index_upper[0];
                    G_i_jk._face[current_face][1] = index_upper[1];
                    G_i_jk._face[current_face][2] = index_upper[2];

                    G_j_ki._face[current_face][0] = index_upper[0];
                    G_j_ki._face[current_face][1] = index_upper[1];
                    G_j_ki._face[current_face][2] = index_upper[2];

                    G_k_ij._face[current_face][0] = index_upper[0];
                    G_k_ij._face[current_face][1] = index_upper[1];
                    G_k_ij._face[current_face][2] = index_upper[2];

                    ++current_face;
                }
            }
        } // for - ac




    //---------------------------------------------

        //G1 = *it->second->_face->_img_C0;

        //cout << G1 << endl;

        // calculate the normal vectors similarly to TriangulatedMesh3::LoadFromOFF
        // replace the boundary normals with the calculated ones
        // calculating average unit normal vectors associated with vertices
//        for (vector<TriangularFace>::const_iterator fit = it->second->_face->_G1->_face.begin(); fit != it->second->_face->_G1->_face.end(); ++fit)
//        {
//            DCoordinate3 n = it->second->_face->_G1->_vertex[(*fit)[1]];
//            n -= it->second->_face->_G1->_vertex[(*fit)[0]];

//            DCoordinate3 p = it->second->_face->_G1->_vertex[(*fit)[2]];
//            p -= it->second->_face->_G1->_vertex[(*fit)[0]];

//            n ^= p;

//            for (GLint node = 0; node < 3; ++node)
//                it->second->_face->_G1->_normal[(*fit)[node]] += n;
//        }

        for (GLuint l = 0; l < G1._face.size(); ++l)
        {
            {
                DCoordinate3 pq = G1._vertex[G1._face[l][1]];
                pq -= G1._vertex[G1._face[l][0]];

                DCoordinate3 rq = G1._vertex[G1._face[l][2]];
                rq -= G1._vertex[G1._face[l][0]];

                DCoordinate3 n = pq;
                n ^= rq;

                G1._normal[G1._face[l][0]] += n;
                G1._normal[G1._face[l][1]] += n;
                G1._normal[G1._face[l][2]] += n;
            }

            {
                DCoordinate3 pq = G_i_jk._vertex[G_i_jk._face[l][1]];
                pq -= G_i_jk._vertex[G_i_jk._face[l][0]];

                DCoordinate3 rq = G_i_jk._vertex[G_i_jk._face[l][2]];
                rq -= G_i_jk._vertex[G_i_jk._face[l][0]];

                DCoordinate3 n = rq;
                n ^= pq;

                G_i_jk._normal[G_i_jk._face[l][0]] += n;
                G_i_jk._normal[G_i_jk._face[l][1]] += n;
                G_i_jk._normal[G_i_jk._face[l][2]] += n;
            }

            {
                DCoordinate3 pq = G_j_ki._vertex[G_j_ki._face[l][1]];
                pq -= G_j_ki._vertex[G_j_ki._face[l][0]];

                DCoordinate3 rq = G_j_ki._vertex[G_j_ki._face[l][2]];
                rq -= G_j_ki._vertex[G_j_ki._face[l][0]];

                DCoordinate3 n = rq;
                n ^= pq;

                G_j_ki._normal[G_j_ki._face[l][0]] += n;
                G_j_ki._normal[G_j_ki._face[l][1]] += n;
                G_j_ki._normal[G_j_ki._face[l][2]] += n;
            }

            {
                DCoordinate3 pq = G_k_ij._vertex[G_k_ij._face[l][1]];
                pq -= G_k_ij._vertex[G_k_ij._face[l][0]];

                DCoordinate3 rq = G_k_ij._vertex[G_k_ij._face[l][2]];
                rq -= G_k_ij._vertex[G_k_ij._face[l][0]];

                DCoordinate3 n = rq;
                n ^= pq;

                G_k_ij._normal[G_k_ij._face[l][0]] += n;
                G_k_ij._normal[G_k_ij._face[l][1]] += n;
                G_k_ij._normal[G_k_ij._face[l][2]] += n;
            }
        }

        for (GLuint l = 0; l < G1._normal.size(); ++l)
        {
            G1._normal[l].normalize();
            G_i_jk._normal[l].normalize();
            G_j_ki._normal[l].normalize();
            G_k_ij._normal[l].normalize();
        }

        // a c_ij boundary
        for (GLuint a = 0; a < div_point_count; ++a)
        {
            GLuint index = a * (a + 1) / 2;
            G1._normal[index] = n_ij[a];
        }

        // a c_ki boundary
        for (GLuint a = 0; a < div_point_count; ++a)
        {
            GLuint index = a * (a + 1) / 2 + a;
            G1._normal[index] = n_ki[a];
        }

        // a c_jk boundary
        GLuint offset = (div_point_count - 1) * div_point_count / 2;

        for (GLuint c = 0; c < div_point_count; ++c)
        {
            G1._normal[offset + c] = n_jk[c];
        }

        for (vector<DCoordinate3>::iterator nit = _faces[f]._G1->_normal.begin(); nit != _faces[f]._G1->_normal.end(); ++nit)
            nit->normalize();

//        G1.UpdateVertexBufferObjects();
//        G_i_jk.UpdateVertexBufferObjects();
//        G_j_ki.UpdateVertexBufferObjects();
//        G_k_ij.UpdateVertexBufferObjects();

    } // for-edge

    if(aborted){
        DeleteG1Surfaces();
    }

    for (GLuint f = 0; f < _faces.size(); f++)
    {
        _faces[f]._G1->UpdateVertexBufferObjects();
        _faces[f]._G_i_jk->UpdateVertexBufferObjects();
        _faces[f]._G_j_ki->UpdateVertexBufferObjects();
        _faces[f]._G_k_ij->UpdateVertexBufferObjects();
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderCurves(GLenum render_mode){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        it->second->_img_boundary->RenderDerivatives(0, render_mode);
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderHalfEdges(){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        glLineWidth(2.0);
        glBegin(GL_LINES);
        DCoordinate3 e1 = *it->second->_vertex;
        glVertex3d(e1[0], e1[1], e1[2]);
        DCoordinate3 e2 = *it->second->_next->_vertex;
        glVertex3d(e2[0], e2[1], e2[2]);
        glEnd();
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderFaces(){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        glBegin(GL_TRIANGLES);
        DCoordinate3 f1 = _vertexes[it->second->_face->_node[0]];
        glVertex3d(f1[0], f1[1], f1[2]);
        glNormal3dv(&_normals[it->second->_face->_node[0]][0]);
        DCoordinate3 f2 = _vertexes[it->second->_face->_node[1]];
        glVertex3d(f2[0], f2[1], f2[2]);
        glNormal3dv(&_normals[it->second->_face->_node[1]][0]);
        DCoordinate3 f3 = _vertexes[it->second->_face->_node[2]];
        glVertex3d(f3[0], f3[1], f3[2]);
        glNormal3dv(&_normals[it->second->_face->_node[2]][0]);
        glEnd();
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderC0Meshes(GLenum render_mode){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        it->second->_face->_img_C0->Render(render_mode);
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderC0ControlNets(){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        it->second->_face->_C0->RenderControlNet();
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderG1MeshesAsPointCloud(GLuint vertex_number, GLuint div_point_count, GLenum render_mode){

    if(vertex_number >= (div_point_count * (div_point_count + 1) / 2)){
        vertex_number = vertex_number % (div_point_count * (div_point_count + 1) / 2);
    }

    for (std::vector<Face>::iterator it = _faces.begin(); it != _faces.end(); it++) {

        glDisable(GL_LIGHTING);

        glBegin(GL_POINTS); // the avaraged selected point

            glPointSize(5.0);
            glColor3f(0.5f, 0.5f, 0.5f);
            glVertex3dv(&it->_G1->_vertex[vertex_number][0]);

        glEnd();


        glPointSize(3.0);
        glBegin(GL_POINTS); // another 3 (selected) points

            glColor3f(1.0f, 0.1f, 0.0f);
            glVertex3dv(&it->_G_i_jk->_vertex[vertex_number][0]);

            glColor3f(0.0f, 1.0f, 0.1f);
            glVertex3dv(&it->_G_j_ki->_vertex[vertex_number][0]);

            glColor3f(0.1f, 0.0f, 1.0f);
            glVertex3dv(&it->_G_k_ij->_vertex[vertex_number][0]);

        glEnd();


        glEnable(GL_LIGHTING);
        glPointSize(1.0);
    }
    for (std::vector<Face>::iterator it = _faces.begin(); it != _faces.end(); it++) {
        glPointSize(1.0);
        glDisable(GL_LIGHTING);
        glColor3f(0.5f, 0.5f, 0.5f);
        it->_G1->Render(GL_POINTS);

        glColor3f(1.0f, 0.0f, 0.0f);
        it->_G_i_jk->Render(GL_POINTS);

        glColor3f(0.0f, 1.0f, 0.0f);
        it->_G_j_ki->Render(GL_POINTS);

        glColor3f(0.0f, 0.0f, 1.0f);
        it->_G_k_ij->Render(GL_POINTS);
        glEnable(GL_LIGHTING);
        glPointSize(1.0);
    }
    glEnable(GL_LIGHTING);
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderG1Meshes(GLenum render_mode){
    for (std::vector<Face>::iterator it = _faces.begin(); it != _faces.end();it++) {

//        glPointSize(1.0);
//        glDisable(GL_LIGHTING);
//        glColor3f(1.0f, 0.0f, 0.0f);
        it->_G1->Render(render_mode);
    }

    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderC0BoundaryNormals(GLuint div_point_count, GLdouble scale){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        // the c_ij boundary
        for (GLuint a = 0; a < div_point_count; ++a)
        {
            glColor3f(1.0, 0.0, 0.0);
            glBegin(GL_LINES);
            GLuint index = a * (a + 1) / 2;
            DCoordinate3 vert = it->second->_face->_img_C0->_vertex[index];
            glVertex3d(vert[0], vert[1], vert[2]);
            DCoordinate3 norm = it->second->_face->_img_C0->_normal[index];
            DCoordinate3 sum = vert + norm * scale;
            //glVertex3d(it->second->_face->_img_C0->_normal[index][0], it->second->_face->_img_C0->_normal[index][1], it->second->_face->_img_C0->_normal[index][2]);
            glVertex3d(sum[0], sum[1], sum[2]);
            glEnd();
        }

        // the c_ki boundary
        for (GLuint a = 0; a < div_point_count; ++a)
        {
            glColor3f(0.0, 1.0, 0.0);
            glBegin(GL_LINES);
            GLuint index = a * (a + 1) / 2 + a;
            DCoordinate3 vert = it->second->_face->_img_C0->_vertex[index];
            glVertex3d(vert[0], vert[1], vert[2]);
            DCoordinate3 norm = it->second->_face->_img_C0->_normal[index];
            DCoordinate3 sum = vert + norm * scale;
            //glVertex3d(it->second->_face->_img_C0->_normal[index][0], it->second->_face->_img_C0->_normal[index][1], it->second->_face->_img_C0->_normal[index][2]);
            glVertex3d(sum[0], sum[1], sum[2]);
            glEnd();
        }

        // the c_jk boundary
        GLuint offset = (div_point_count - 1) * div_point_count / 2;

        for (GLuint c = 0; c < div_point_count; ++c)
        {
            glColor3f(0.0, 0.0, 1.0);
            glBegin(GL_LINES);
            DCoordinate3 vert = it->second->_face->_img_C0->_vertex[offset + c];
            glVertex3d(vert[0], vert[1], vert[2]);
            DCoordinate3 norm = it->second->_face->_img_C0->_normal[offset + c];
            DCoordinate3 sum = vert + norm * scale;
            //glVertex3d(it->second->_face->_img_C0->_normal[offset + c][0], it->second->_face->_img_C0->_normal[offset + c][1], it->second->_face->_img_C0->_normal[offset + c][2]);
            glVertex3d(sum[0], sum[1], sum[2]);
            glEnd();
        }

    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderG1BoundaryNormals(GLuint div_point_count, GLdouble scale){
//    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;//, endit, begit;
    for (std::vector<Face>::iterator it = _faces.begin(); it != _faces.end(); it++ ) {
        // the c_ij boundary
        for (GLuint a = 0; a < div_point_count; ++a)
        {
            glColor3f(1.0, 0.0, 0.0);
            glBegin(GL_LINES);
            GLuint index = a * (a + 1) / 2;
            DCoordinate3 vert = it->_G1->_vertex[index];
            glVertex3d(vert[0], vert[1], vert[2]);
            DCoordinate3 norm = it->_G1->_normal[index];
            DCoordinate3 sum = vert + norm * scale;
            //glVertex3d(it->second->_face->_G1->_normal[index][0], it->second->_face->_G1->_normal[index][1], it->second->_face->_G1->_normal[index][2]);
            glVertex3d(sum[0], sum[1], sum[2]);
            glEnd();
        }

        // the c_ki boundary
        for (GLuint a = 0; a < div_point_count; ++a)
        {
            glColor3f(0.0, 1.0, 0.0);
            glBegin(GL_LINES);
            GLuint index = a * (a + 1) / 2 + a;
            DCoordinate3 vert = it->_G1->_vertex[index];
            glVertex3d(vert[0], vert[1], vert[2]);
            DCoordinate3 norm = it->_G1->_normal[index];
            DCoordinate3 sum = vert + norm * scale;
            //glVertex3d(it->second->_face->_G1->_normal[index][0], it->second->_face->_G1->_normal[index][1], it->second->_face->_G1->_normal[index][2]);
            glVertex3d(sum[0], sum[1], sum[2]);
            glEnd();
        }

        // the c_jk boundary
        GLuint offset = (div_point_count - 1) * div_point_count / 2;

        for (GLuint c = 0; c < div_point_count; ++c)
        {
            glColor3f(0.0, 0.0, 1.0);
            glBegin(GL_LINES);
            DCoordinate3 vert = it->_G1->_vertex[offset + c];
            glVertex3d(vert[0], vert[1], vert[2]);
            DCoordinate3 norm = it->_G1->_normal[offset + c];
            DCoordinate3 sum = vert + norm * scale;
            //glVertex3d(it->second->_face->_G1->_normal[offset + c][0], it->second->_face->_G1->_normal[offset + c][1], it->second->_face->_G1->_normal[offset + c][2]);
            glVertex3d(sum[0], sum[1], sum[2]);
            glEnd();
        }

        //-------------------------------------------------
//        for (GLuint a = 0; a < div_point_count; ++a)
//        {
//            glColor3f(1.0, 0.0, 0.0);
//            glBegin(GL_LINES);
//            GLuint index = a * (a + 1) / 2;
//            glVertex3d(it->second->_opposite->_face->_G1->_vertex[index][0], it->second->_opposite->_face->_G1->_vertex[index][1], it->second->_opposite->_face->_G1->_vertex[index][2]);
//            DCoordinate3 sum = it->second->_opposite->_face->_G1->_vertex[index] + it->second->_opposite->_face->_G1->_normal[index];
//            sum.normalize();
//            //sum *= scale;
//            //glVertex3d(it->second->_face->_G1->_normal[index][0], it->second->_face->_G1->_normal[index][1], it->second->_face->_G1->_normal[index][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        // the c_ki boundary
//        for (GLuint a = 0; a < div_point_count; ++a)
//        {
//            glColor3f(0.0, 1.0, 0.0);
//            glBegin(GL_LINES);
//            GLuint index = a * (a + 1) / 2 + a;
//            glVertex3d(it->second->_opposite->_face->_G1->_vertex[index][0], it->second->_opposite->_face->_G1->_vertex[index][1], it->second->_opposite->_face->_G1->_vertex[index][2]);
//            DCoordinate3 sum = it->second->_opposite->_face->_G1->_vertex[index] + it->second->_opposite->_face->_G1->_normal[index];
//            sum.normalize();
//            //glVertex3d(it->second->_face->_G1->_normal[index][0], it->second->_face->_G1->_normal[index][1], it->second->_face->_G1->_normal[index][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        // the c_jk boundary
//        offset = (div_point_count - 1) * div_point_count / 2;

//        for (GLuint c = 0; c < div_point_count; ++c)
//        {
//            glColor3f(0.0, 0.0, 1.0);
//            glBegin(GL_LINES);
//            it->second->_face->_img_C0->_normal[offset + c];
//            glVertex3d(it->second->_opposite->_face->_G1->_vertex[offset + c][0], it->second->_opposite->_face->_G1->_vertex[offset + c][1], it->second->_opposite->_face->_G1->_vertex[offset + c][2]);
//            DCoordinate3 sum = it->second->_opposite->_face->_G1->_vertex[offset + c] + it->second->_opposite->_face->_G1->_normal[offset + c];
//            sum.normalize();
//            //glVertex3d(it->second->_face->_G1->_normal[offset + c][0], it->second->_face->_G1->_normal[offset + c][1], it->second->_face->_G1->_normal[offset + c][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        //-------------------------------------------------
//        for (GLuint a = 0; a < div_point_count; ++a)
//        {
//            glColor3f(1.0, 0.0, 0.0);
//            glBegin(GL_LINES);
//            GLuint index = a * (a + 1) / 2;
//            glVertex3d(it->second->_next->_opposite->_face->_G1->_vertex[index][0], it->second->_next->_opposite->_face->_G1->_vertex[index][1], it->second->_next->_opposite->_face->_G1->_vertex[index][2]);
//            DCoordinate3 sum = it->second->_next->_opposite->_face->_G1->_vertex[index] + it->second->_next->_opposite->_face->_G1->_normal[index];
//            sum.normalize();
//            //sum *= scale;
//            //glVertex3d(it->second->_face->_G1->_normal[index][0], it->second->_face->_G1->_normal[index][1], it->second->_face->_G1->_normal[index][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        // the c_ki boundary
//        for (GLuint a = 0; a < div_point_count; ++a)
//        {
//            glColor3f(0.0, 1.0, 0.0);
//            glBegin(GL_LINES);
//            GLuint index = a * (a + 1) / 2 + a;
//            glVertex3d(it->second->_next->_opposite->_face->_G1->_vertex[index][0], it->second->_next->_opposite->_face->_G1->_vertex[index][1], it->second->_next->_opposite->_face->_G1->_vertex[index][2]);
//            DCoordinate3 sum = it->second->_next->_opposite->_face->_G1->_vertex[index] + it->second->_next->_opposite->_face->_G1->_normal[index];
//            sum.normalize();
//            //glVertex3d(it->second->_next->_face->_G1->_normal[index][0], it->second->_next->_face->_G1->_normal[index][1], it->second->_next->_face->_G1->_normal[index][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        // the c_jk boundary
//        offset = (div_point_count - 1) * div_point_count / 2;

//        for (GLuint c = 0; c < div_point_count; ++c)
//        {
//            glColor3f(0.0, 0.0, 1.0);
//            glBegin(GL_LINES);
//            it->second->_next->_face->_img_C0->_normal[offset + c];
//            glVertex3d(it->second->_next->_opposite->_face->_G1->_vertex[offset + c][0], it->second->_next->_opposite->_face->_G1->_vertex[offset + c][1], it->second->_next->_opposite->_face->_G1->_vertex[offset + c][2]);
//            DCoordinate3 sum = it->second->_next->_opposite->_face->_G1->_vertex[offset + c] + it->second->_next->_opposite->_face->_G1->_normal[offset + c];
//            sum.normalize();
//            //glVertex3d(it->second->_next->_face->_G1->_normal[offset + c][0], it->second->_next->_face->_G1->_normal[offset + c][1], it->second->_next->_face->_G1->_normal[offset + c][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        //-------------------------------------------------
//        for (GLuint a = 0; a < div_point_count; ++a)
//        {
//            glColor3f(1.0, 0.0, 0.0);
//            glBegin(GL_LINES);
//            GLuint index = a * (a + 1) / 2;
//            glVertex3d(it->second->_next->_next->_opposite->_face->_G1->_vertex[index][0], it->second->_next->_next->_opposite->_face->_G1->_vertex[index][1], it->second->_next->_next->_opposite->_face->_G1->_vertex[index][2]);
//            DCoordinate3 sum = it->second->_next->_next->_opposite->_face->_G1->_vertex[index] + it->second->_next->_next->_opposite->_face->_G1->_normal[index];
//            sum.normalize();
//            //sum *= scale;
//            //glVertex3d(it->second->_next->_face->_G1->_normal[index][0], it->second->_next->_face->_G1->_normal[index][1], it->second->_next->_face->_G1->_normal[index][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        // the c_ki boundary
//        for (GLuint a = 0; a < div_point_count; ++a)
//        {
//            glColor3f(0.0, 1.0, 0.0);
//            glBegin(GL_LINES);
//            GLuint index = a * (a + 1) / 2 + a;
//            glVertex3d(it->second->_next->_next->_opposite->_face->_G1->_vertex[index][0], it->second->_next->_next->_opposite->_face->_G1->_vertex[index][1], it->second->_next->_next->_opposite->_face->_G1->_vertex[index][2]);
//            DCoordinate3 sum = it->second->_next->_next->_opposite->_face->_G1->_vertex[index] + it->second->_next->_next->_opposite->_face->_G1->_normal[index];
//            sum.normalize();
//            //glVertex3d(it->second->_next->_next->_face->_G1->_normal[index][0], it->second->_next->_next->_face->_G1->_normal[index][1], it->second->_next->_next->_face->_G1->_normal[index][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }

//        // the c_jk boundary
//        offset = (div_point_count - 1) * div_point_count / 2;

//        for (GLuint c = 0; c < div_point_count; ++c)
//        {
//            glColor3f(0.0, 0.0, 1.0);
//            glBegin(GL_LINES);
//            it->second->_next->_next->_face->_img_C0->_normal[offset + c];
//            glVertex3d(it->second->_next->_next->_opposite->_face->_G1->_vertex[offset + c][0], it->second->_next->_next->_opposite->_face->_G1->_vertex[offset + c][1], it->second->_next->_next->_opposite->_face->_G1->_vertex[offset + c][2]);
//            DCoordinate3 sum = it->second->_next->_next->_opposite->_face->_G1->_vertex[offset + c] + it->second->_next->_next->_opposite->_face->_G1->_normal[offset + c];
//            sum.normalize();
//            //glVertex3d(it->second->_next->_next->_face->_G1->_normal[offset + c][0], it->second->_next->_next->_face->_G1->_normal[offset + c][1], it->second->_next->_next->_face->_G1->_normal[offset + c][2]);
//            glVertex3d(sum[0], sum[1], sum[2]);
//            glEnd();
//        }
    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderApproximatedUnitTangents(GLdouble scale){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        DCoordinate3 vertex1 = *it->second->_vertex;
        DCoordinate3 vertex2 = *it->second->_next->_vertex;
        DCoordinate3 tangent1 = *it->second->_vertex + it->second->_tangent1 * scale;
        DCoordinate3 tangent2 = *it->second->_next->_vertex + it->second->_tangent2 * scale;

        glLineWidth(1.5);
        glBegin(GL_LINES);
        glVertex3d(vertex1[0], vertex1[1], vertex1[2]);
        glVertex3dv(&tangent1[0]);

        glVertex3d(vertex2[0], vertex2[1], vertex2[2]);
        glVertex3dv(&tangent2[0]);
        glEnd();
    }

//    for (GLuint i = 0; i != _faces.size(); i++ ) {
////        for (vector<TriangularFace>::const_iterator fit = it->second->_face->_G1->_face.begin(); fit != it->second->_face->_G1->_face.end(); ++fit)
////        {
//            cout << i << endl;
//            DCoordinate3 p1 = _vertexes[_faces[i][0]];
//            DCoordinate3 p2 = _vertexes[_faces[i][1]];

//            DCoordinate3 n1 = _normals[_faces[i][0]];
//            DCoordinate3 n2 = _normals[_faces[i][1]];

//            DCoordinate3 f1 = -n1;
//            DCoordinate3 f2 = -n2;

//            DCoordinate3 b1 = ((p2-p1)^f1) / ((p2-p1)^f1).length();
//            DCoordinate3 b2 = ((p1-p2)^f2) / ((p1-p2)^f2).length();

//            DCoordinate3 t1 = f1 ^ b1;
//            DCoordinate3 t2 = f2 ^ b2;

//            glLineWidth(1.5);
//            glBegin(GL_LINES);
//            glVertex3d(p1[0],p1[1],p1[2]);
//            DCoordinate3 sum = p1 + t1 * scale;
//            glVertex3dv(&sum[0]);

//            glVertex3d(p2[0],p2[1],p2[2]);
//            DCoordinate3 sum1 = p2 + t2 * scale;
//            glVertex3dv(&sum1[0]);
//            glEnd();

//            p1 = _vertexes[_faces[i][1]];
//            p2 = _vertexes[_faces[i][2]];

//            n1 = _normals[_faces[i][1]];
//            n2 = _normals[_faces[i][2]];

//            f1 = -n1;
//            f2 = -n2;

//            b1 = ((p2-p1)^f1) / ((p2-p1)^f1).length();
//            b2 = ((p1-p2)^f2) / ((p1-p2)^f2).length();

//            t1 = f1 ^ b1;
//            t2 = f2 ^ b2;

//            glBegin(GL_LINES);
//            glVertex3d(p1[0],p1[1],p1[2]);
//            sum = p1 + t1 * scale;
//            glVertex3dv(&sum[0]);

//            glVertex3d(p2[0],p2[1],p2[2]);
//            sum1 = p2 + t2 * scale;
//            glVertex3dv(&sum1[0]);
//            glEnd();

//            p1 = _vertexes[_faces[i][2]];
//            p2 = _vertexes[_faces[i][0]];

//            n1 = _normals[_faces[i][2]];
//            n2 = _normals[_faces[i][0]];

//            f1 = -n1;
//            f2 = -n2;

//            b1 = ((p2-p1)^f1) / ((p2-p1)^f1).length();
//            b2 = ((p1-p2)^f2) / ((p1-p2)^f2).length();

//            t1 = f1 ^ b1;
//            t2 = f2 ^ b2;

//            glBegin(GL_LINES);
//            glVertex3d(p1[0],p1[1],p1[2]);
//            sum = p1 + t1 * scale;
//            glVertex3dv(&sum[0]);

//            glVertex3d(p2[0],p2[1],p2[2]);
//            sum1 = p2 + t2 * scale;
//            glVertex3dv(&sum1[0]);
//            glEnd();
////        }
//    }
    return GL_TRUE;
}

GLboolean TriangulatedHalfEdgeDataStructure3::RenderVertexNormals(GLdouble scale){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        glBegin(GL_LINES);
        DCoordinate3 v = *it->second->_vertex;
        DCoordinate3 n = *it->second->_normal;
        DCoordinate3 sum = v + n * scale;
        glVertex3d(v[0], v[1], v[2]);
        glVertex3d(sum[0], sum[1], sum[2]);
//        DCoordinate3 v1 = _vertexes[it->second->_face->_node[0]];
//        DCoordinate3 v2 = _vertexes[it->second->_face->_node[1]];
//        DCoordinate3 v3 = _vertexes[it->second->_face->_node[2]];

//        DCoordinate3 n1 = _normals[it->second->_face->_node[0]];
//        DCoordinate3 n2 = _normals[it->second->_face->_node[1]];
//        DCoordinate3 n3 = _normals[it->second->_face->_node[2]];

//        DCoordinate3 sum1 = v1 + n1 * scale;
//        DCoordinate3 sum2 = v2 + n2 * scale;
//        DCoordinate3 sum3 = v3 + n3 * scale;

//        glVertex3d(v1[0], v1[1], v1[2]);
//        glVertex3d(sum1[0], sum1[1], sum1[2]);
//        glVertex3d(v2[0], v2[1], v2[2]);
//        glVertex3d(sum2[0], sum2[1], sum2[2]);
//        glVertex3d(v3[0], v3[1], v3[2]);
//        glVertex3d(sum3[0], sum3[1], sum3[2]);
        glEnd();
    }
    return GL_TRUE;
}

std::vector<DCoordinate3> TriangulatedHalfEdgeDataStructure3::GetVertexes(){
    return _vertexes;
}

GLvoid TriangulatedHalfEdgeDataStructure3::DeleteBoundaryCurves(){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        if(it->second->_boundary){
            delete it->second->_boundary; it->second->_boundary = nullptr;
        }
        if(it->second->_img_boundary){
            delete it->second->_img_boundary; it->second->_img_boundary = nullptr;
        }
    }
}

GLvoid TriangulatedHalfEdgeDataStructure3::DeleteC0Surfaces(){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        if(it->second->_face->_C0){
            delete it->second->_face->_C0; it->second->_face->_C0 = nullptr;
        }
        if(it->second->_face->_img_C0){
            delete it->second->_face->_img_C0; it->second->_face->_img_C0 = nullptr;
        }
    }
}

GLvoid TriangulatedHalfEdgeDataStructure3::DeleteG1Surfaces(){
    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    for (it = _edges.begin(); it != _edges.end(); it++ ) {
        if(it->second->_face->_G1){
            delete it->second->_face->_G1; it->second->_face->_G1 = nullptr;
        }
    }
}

GLvoid TriangulatedHalfEdgeDataStructure3::CleanAll(){
    DeleteBoundaryCurves();
    DeleteC0Surfaces();
    DeleteG1Surfaces();
    //    std::map< std::pair<GLint, GLint>, HalfEdge* >::iterator it;
    //    for (it = _edges.begin(); it != _edges.end(); it++ ) {
    //        if(it->second->_vertex){
    //            delete it->second->_vertex; it->second->_vertex = nullptr;
    //        }

    //        if(it->second->_normal){
    //            delete it->second->_normal; it->second->_normal = nullptr;
    //        }

    //        if(it->second->_opposite){
    //            delete it->second->_opposite; it->second->_opposite = nullptr;
    //        }

    //        if(it->second->_next){
    //            delete it->second->_next; it->second->_next = nullptr;
    //        }

    //        if(it->second->_face){
    //            delete it->second->_face; it->second->_face = nullptr;
    //        }

    //        if(it->second){
    //            delete it->second; it->second = nullptr;
    //        }
//    }

    _edges.clear();
    _vertexes.clear();
    _normals.clear();
    _faces.clear();
}

TriangulatedHalfEdgeDataStructure3::~TriangulatedHalfEdgeDataStructure3(){
    CleanAll();
}
}
