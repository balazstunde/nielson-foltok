#include <cmath>
#include "TestFunctions.h"
#include "../Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI;
GLdouble spiral_on_cone::u_max = +TWO_PI;

DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

GLdouble epicycloid::u_min = -TWO_PI;
GLdouble epicycloid::u_max = +TWO_PI;
GLdouble epicycloid::a = 2; //?
GLdouble epicycloid::b = 1; //?
//x(t) = (a + b) cost − b cos((a/b + 1)t)
//y(t) = (a + b)sin t − b sin((a/b + 1)t)
DCoordinate3 epicycloid::d0(GLdouble u){
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3((a + b) * c - b * cos((a / b + 1) * u), (a+b) * s - b * sin((a / b + 1) * u), 0);
}

DCoordinate3 epicycloid::d1(GLdouble u){
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3((a + b) * sin((a / b + 1) * u) - (a + b) * s, (a + b) * c - (a + b) * cos((a / b + 1) * u), 0);
}

DCoordinate3 epicycloid::d2(GLdouble u){
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-(a + b) * c + (a + b) * cos((a / b + 1) * u) * (a / b + 1), -(a + b) * s - (a + b) * sin((a / b + 1) * u) * (a / b + 1), 0);
}

GLdouble lissajous::u_min = -TWO_PI;
GLdouble lissajous::u_max = +TWO_PI;
GLdouble lissajous::A = 1;
GLdouble lissajous::a = 1; //?
GLdouble lissajous::B = 1;
GLdouble lissajous::b = 2; //?
GLdouble lissajous::delta= 0.0;

//x(t) = a sin(nt + c)
//y(t) = b sin t

DCoordinate3 lissajous::d0(GLdouble u){
    GLdouble s = sin(b * u);
    return DCoordinate3(A * sin(a * u + delta), B * s , 0);
}

DCoordinate3 lissajous::d1(GLdouble u){
    GLdouble c = cos(b * u);
    return DCoordinate3(A * cos(a * u + delta) * a, B * c * b, 0);
}

DCoordinate3 lissajous::d2(GLdouble u){
    GLdouble s = sin(b * u);
    return DCoordinate3(-A * sin(a * u + delta) * a * a, -B * s * b * b, 0);
}

GLdouble nephroid::u_min = -TWO_PI;
GLdouble nephroid::u_max = +TWO_PI;
GLdouble nephroid::a = 1;

//x(t) = a(3 cost − cos(3t))
//y(t) = a(3 sin t − sin(3t))

DCoordinate3 nephroid::d0(GLdouble u){
    return DCoordinate3(a * ( 3 * cos(u) - cos(3 * u)), a * ( 3 * sin(u) - sin(3 * u)), 0);
}

DCoordinate3 nephroid::d1(GLdouble u){
    return DCoordinate3(a * ( -3 * sin(u) + 3 * sin(3 * u)), a * ( 3 * cos(u) - 3 * cos(3 * u)), 0);
}

DCoordinate3 nephroid::d2(GLdouble u){
    return DCoordinate3(a * ( -3 * cos(u) + 9 * cos(3 * u)), a * ( -3 * sin(u) + 9 * sin(3 * u)), 0);
}

GLdouble involute_of_a_circle::u_min = -TWO_PI;
GLdouble involute_of_a_circle::u_max = +TWO_PI;
GLdouble involute_of_a_circle::a = 1;

//x(t) = a(cost + tsin t)
//y(t) = a(sin t − t cost)

DCoordinate3 involute_of_a_circle::d0(GLdouble u){
    GLdouble s = sin(u), c = cos(u);
    return DCoordinate3(a * (c + u * s), a * (s - u * c), 0);
}

DCoordinate3 involute_of_a_circle::d1(GLdouble u){
    GLdouble s = sin(u), c = cos(u);
    return DCoordinate3(a * (u * c), a * (u * s), 0);
}

DCoordinate3 involute_of_a_circle::d2(GLdouble u){
    GLdouble s = sin(u), c = cos(u);
    return DCoordinate3(a * (-u * s), a * (u * c), 0);
}

GLdouble hypocycloid::u_min = -TWO_PI;
GLdouble hypocycloid::u_max = +TWO_PI;
GLdouble hypocycloid::a = 5;
GLdouble hypocycloid::b = 1;

//x(t) = (a − b) cost + b cos((a/b − 1)t)
//y(t) = (a − b)sin t − b sin((a/b − 1)t)
//If a = 3b, tricuspoid. If a = 4b, astroid.

DCoordinate3 hypocycloid::d0(GLdouble u){
    return DCoordinate3((a - b) * cos(u) + b * cos((a / b - 1) * u), (a - b) * sin(u) - b * sin((a / b - 1) * u), 0);
}

DCoordinate3 hypocycloid::d1(GLdouble u){
    return DCoordinate3((b - a) * sin(u) - (a - b) * sin((a / b - 1) * u), (a - b) * cos(u) - (a - b) * cos((a / b - 1) * u), 0);
}

DCoordinate3 hypocycloid::d2(GLdouble u){
    return DCoordinate3((b - a) * cos(u) - (a - b) * cos((a / b - 1) * u) * (a / b - 1), (b - a) * sin(u) + (a - b) * sin((a / b - 1) * u) * (a / b - 1), 0);
}

GLdouble sphere::u_min  = 0;
GLdouble sphere::u_max  = PI;
GLdouble sphere::v_min  = 0;
GLdouble sphere::v_max  = TWO_PI;
GLdouble sphere::r      = 3;

DCoordinate3 sphere::d00(GLdouble u, GLdouble v){
    return DCoordinate3(r*sin(u)*sin(v), r*sin(u)*cos(v), r*cos(u));
}

DCoordinate3 sphere::d01(GLdouble u, GLdouble v){
    return DCoordinate3(r*cos(u)*sin(v), r*cos(u)*cos(v), -r*sin(u));
}

DCoordinate3 sphere::d10(GLdouble u, GLdouble v){
    return DCoordinate3(r*sin(u)*cos(v), -r*sin(u)*sin(v), 0);
}

/*  x	=	(c+a cosv)cos u
    y	=	(c+a cosv)sin u
    z	=	a sinv
 u,v in [0,2pi)
c>a corresponds to the ring torus (shown above), c=a corresponds to a horn torus which is tangent to itself at the point (0, 0, 0), and c<a corresponds to a self-intersecting spindle torus (Pinkall 1986)*/

GLdouble torus::u_min  = 0;
GLdouble torus::u_max  = TWO_PI;
GLdouble torus::v_min  = 0;
GLdouble torus::v_max  = TWO_PI;
GLdouble torus::a      = 2;
GLdouble torus::c      = 4;

DCoordinate3 torus::d00(GLdouble u, GLdouble v){
    return DCoordinate3((c + a * cos(v)) * cos(u), (c + a * cos(v)) * sin(u), a * sin(v));
}

DCoordinate3 torus::d01(GLdouble u, GLdouble v){
    return DCoordinate3(-(c + a * cos(v)) * sin(u), (c + a * cos(v)) * cos(u), 0);
}

DCoordinate3 torus::d10(GLdouble u, GLdouble v){
    return DCoordinate3((-a * sin(v)) * cos(u), ( -a * sin(v)) * sin(u), a * cos(v));
}

/*
A quadratic surface which has elliptical cross section. The elliptic paraboloid of height h, semimajor axis a, and semiminor axis b can be specified parametrically by

x	=	asqrt(u)cosv
y	=	bsqrt(u)sinv
z	=	u
for v in [0,2pi) and u in [0,h].
*/
GLdouble elliptic_paraboloid::u_min  = 0;
GLdouble elliptic_paraboloid::u_max  = TWO_PI;
GLdouble elliptic_paraboloid::v_min  = 0;
GLdouble elliptic_paraboloid::v_max  = TWO_PI;
GLdouble elliptic_paraboloid::a      = 4;
GLdouble elliptic_paraboloid::b      = 2;

DCoordinate3 elliptic_paraboloid::d00(GLdouble u, GLdouble v){
    return DCoordinate3(a * sqrt(u) * cos(v), b * sqrt(u) * sin(v), u);
}

DCoordinate3 elliptic_paraboloid::d01(GLdouble u, GLdouble v){
    return DCoordinate3(a * (1/(2 * sqrt(u))) * cos(v), b * (1/(2 * sqrt(u))) * sin(v), 1);
}

DCoordinate3 elliptic_paraboloid::d10(GLdouble u, GLdouble v){
    return DCoordinate3(-a * sqrt(u) * sin(v), b * sqrt(u) * cos(v), 0);
}

/*
The lateral surface of a cylinder of height h and radius r can be described parametrically by

x	=	rcos u
y	=	rsin u
z	=	v,
for v in [0,h] and u in [0,2pi).
*/

GLdouble cylinder::u_min  = 0;
GLdouble cylinder::u_max  = TWO_PI;
GLdouble cylinder::v_min  = 0;
GLdouble cylinder::v_max  = 5;
GLdouble cylinder::h      = 5;
GLdouble cylinder::r      = 3;

DCoordinate3 cylinder::d00(GLdouble u, GLdouble v){
    return DCoordinate3(r * cos(u), r * sin(u), v);
}

DCoordinate3 cylinder::d01(GLdouble u, GLdouble v){
    return DCoordinate3(-r * sin(u), r * cos(u), 0);
}

DCoordinate3 cylinder::d10(GLdouble u, GLdouble v){
    return DCoordinate3(0, 0, 1);
}

/*
x(u,v)	=	u
y(u,v)	=	v
z(u,v)	=	uv
*/

GLdouble hyperbolic_paraboloid::u_min  = 0;
GLdouble hyperbolic_paraboloid::u_max  = TWO_PI;
GLdouble hyperbolic_paraboloid::v_min  = 0;
GLdouble hyperbolic_paraboloid::v_max  = TWO_PI;

DCoordinate3 hyperbolic_paraboloid::d00(GLdouble u, GLdouble v){
    return DCoordinate3(u, v, u*v);
}

DCoordinate3 hyperbolic_paraboloid::d01(GLdouble u, GLdouble v){
    return DCoordinate3(1, 0, v);
}

DCoordinate3 hyperbolic_paraboloid::d10(GLdouble u, GLdouble v){
    return DCoordinate3(0, 1, u);
}
