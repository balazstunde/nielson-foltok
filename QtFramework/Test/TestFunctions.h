#pragma once

#include "../Core/DCoordinates3.h"

namespace cagd
{
    namespace spiral_on_cone
    {
        extern GLdouble u_min, u_max;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace epicycloid{
        extern GLdouble u_min, u_max, a, b;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace lissajous{
        extern GLdouble u_min, u_max, A, a, B, b, delta;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace nephroid{
        extern GLdouble u_min, u_max, a;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace involute_of_a_circle{
        extern GLdouble u_min, u_max, a;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace hypocycloid{
        extern GLdouble u_min, u_max, a, b;

        DCoordinate3 d0(GLdouble);
        DCoordinate3 d1(GLdouble);
        DCoordinate3 d2(GLdouble);
    }

    namespace sphere{
        extern GLdouble u_min, u_max, v_min, v_max, r;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
    }

    namespace torus{
        extern GLdouble u_min, u_max, v_min, v_max, a, c;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
    }

    namespace elliptic_paraboloid{
        extern GLdouble u_min, u_max, v_min, v_max, a, b;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
    }

    namespace cylinder{
        extern GLdouble u_min, u_max, v_min, v_max, h, r;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
    }

    namespace hyperbolic_paraboloid{
        extern GLdouble u_min, u_max, v_min, v_max;

        DCoordinate3 d00(GLdouble, GLdouble);
        DCoordinate3 d01(GLdouble, GLdouble);
        DCoordinate3 d10(GLdouble, GLdouble);
    }

}
