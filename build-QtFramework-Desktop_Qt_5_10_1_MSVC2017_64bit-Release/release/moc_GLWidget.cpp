/****************************************************************************
** Meta object code from reading C++ file 'GLWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../QtFramework/GUI/GLWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GLWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_cagd__GLWidget_t {
    QByteArrayData data[53];
    char stringdata0[924];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_cagd__GLWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_cagd__GLWidget_t qt_meta_stringdata_cagd__GLWidget = {
    {
QT_MOC_LITERAL(0, 0, 14), // "cagd::GLWidget"
QT_MOC_LITERAL(1, 15, 20), // "statusMessageChanged"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 4), // "text"
QT_MOC_LITERAL(4, 42, 30), // "setCalculationTimeCurveNetwork"
QT_MOC_LITERAL(5, 73, 4), // "time"
QT_MOC_LITERAL(6, 78, 28), // "setCalculationTimeC0Surfaces"
QT_MOC_LITERAL(7, 107, 28), // "setCalculationTimeG1Surfaces"
QT_MOC_LITERAL(8, 136, 9), // "setCursor"
QT_MOC_LITERAL(9, 146, 6), // "cursor"
QT_MOC_LITERAL(10, 153, 8), // "_animate"
QT_MOC_LITERAL(11, 162, 11), // "set_angle_x"
QT_MOC_LITERAL(12, 174, 5), // "value"
QT_MOC_LITERAL(13, 180, 11), // "set_angle_y"
QT_MOC_LITERAL(14, 192, 11), // "set_angle_z"
QT_MOC_LITERAL(15, 204, 15), // "set_zoom_factor"
QT_MOC_LITERAL(16, 220, 11), // "set_trans_x"
QT_MOC_LITERAL(17, 232, 11), // "set_trans_y"
QT_MOC_LITERAL(18, 244, 11), // "set_trans_z"
QT_MOC_LITERAL(19, 256, 11), // "set_theta_1"
QT_MOC_LITERAL(20, 268, 11), // "set_theta_2"
QT_MOC_LITERAL(21, 280, 18), // "set_basis_function"
QT_MOC_LITERAL(22, 299, 19), // "set_weight_function"
QT_MOC_LITERAL(23, 319, 19), // "set_shape_parameter"
QT_MOC_LITERAL(24, 339, 10), // "set_shader"
QT_MOC_LITERAL(25, 350, 16), // "set_poligon_mode"
QT_MOC_LITERAL(26, 367, 12), // "set_material"
QT_MOC_LITERAL(27, 380, 18), // "set_selected_shape"
QT_MOC_LITERAL(28, 399, 19), // "set_div_point_count"
QT_MOC_LITERAL(29, 419, 9), // "set_eps_1"
QT_MOC_LITERAL(30, 429, 9), // "set_eps_2"
QT_MOC_LITERAL(31, 439, 18), // "set_vertices_check"
QT_MOC_LITERAL(32, 458, 17), // "set_vertices_size"
QT_MOC_LITERAL(33, 476, 17), // "set_normals_check"
QT_MOC_LITERAL(34, 494, 16), // "set_normals_size"
QT_MOC_LITERAL(35, 511, 18), // "set_tangents_check"
QT_MOC_LITERAL(36, 530, 17), // "set_tangents_size"
QT_MOC_LITERAL(37, 548, 20), // "set_boundaries_check"
QT_MOC_LITERAL(38, 569, 27), // "set_boundaries_normal_check"
QT_MOC_LITERAL(39, 597, 26), // "set_boundaries_normal_size"
QT_MOC_LITERAL(40, 624, 18), // "set_geometry_check"
QT_MOC_LITERAL(41, 643, 12), // "set_geometry"
QT_MOC_LITERAL(42, 656, 17), // "set_vertex_number"
QT_MOC_LITERAL(43, 674, 26), // "elephant_v102_f200_checked"
QT_MOC_LITERAL(44, 701, 21), // "m2_v3495_f666_checked"
QT_MOC_LITERAL(45, 723, 22), // "spot_v122_f240_checked"
QT_MOC_LITERAL(46, 746, 23), // "bunny_v113_f222_checked"
QT_MOC_LITERAL(47, 770, 23), // "bunny_v239_f474_checked"
QT_MOC_LITERAL(48, 794, 23), // "horse_v341_f678_checked"
QT_MOC_LITERAL(49, 818, 24), // "m195_v2472_f4809_checked"
QT_MOC_LITERAL(50, 843, 25), // "angel_v1352_f2706_checked"
QT_MOC_LITERAL(51, 869, 26), // "dragon_v1386_f2772_checked"
QT_MOC_LITERAL(52, 896, 27) // "icosahedron_v12_f20_checked"

    },
    "cagd::GLWidget\0statusMessageChanged\0"
    "\0text\0setCalculationTimeCurveNetwork\0"
    "time\0setCalculationTimeC0Surfaces\0"
    "setCalculationTimeG1Surfaces\0setCursor\0"
    "cursor\0_animate\0set_angle_x\0value\0"
    "set_angle_y\0set_angle_z\0set_zoom_factor\0"
    "set_trans_x\0set_trans_y\0set_trans_z\0"
    "set_theta_1\0set_theta_2\0set_basis_function\0"
    "set_weight_function\0set_shape_parameter\0"
    "set_shader\0set_poligon_mode\0set_material\0"
    "set_selected_shape\0set_div_point_count\0"
    "set_eps_1\0set_eps_2\0set_vertices_check\0"
    "set_vertices_size\0set_normals_check\0"
    "set_normals_size\0set_tangents_check\0"
    "set_tangents_size\0set_boundaries_check\0"
    "set_boundaries_normal_check\0"
    "set_boundaries_normal_size\0"
    "set_geometry_check\0set_geometry\0"
    "set_vertex_number\0elephant_v102_f200_checked\0"
    "m2_v3495_f666_checked\0spot_v122_f240_checked\0"
    "bunny_v113_f222_checked\0bunny_v239_f474_checked\0"
    "horse_v341_f678_checked\0"
    "m195_v2472_f4809_checked\0"
    "angel_v1352_f2706_checked\0"
    "dragon_v1386_f2772_checked\0"
    "icosahedron_v12_f20_checked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_cagd__GLWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      47,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  249,    2, 0x06 /* Public */,
       4,    1,  252,    2, 0x06 /* Public */,
       6,    1,  255,    2, 0x06 /* Public */,
       7,    1,  258,    2, 0x06 /* Public */,
       8,    1,  261,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,  264,    2, 0x08 /* Private */,
      11,    1,  265,    2, 0x0a /* Public */,
      13,    1,  268,    2, 0x0a /* Public */,
      14,    1,  271,    2, 0x0a /* Public */,
      15,    1,  274,    2, 0x0a /* Public */,
      16,    1,  277,    2, 0x0a /* Public */,
      17,    1,  280,    2, 0x0a /* Public */,
      18,    1,  283,    2, 0x0a /* Public */,
      19,    1,  286,    2, 0x0a /* Public */,
      20,    1,  289,    2, 0x0a /* Public */,
      21,    1,  292,    2, 0x0a /* Public */,
      22,    1,  295,    2, 0x0a /* Public */,
      23,    1,  298,    2, 0x0a /* Public */,
      24,    1,  301,    2, 0x0a /* Public */,
      25,    1,  304,    2, 0x0a /* Public */,
      26,    1,  307,    2, 0x0a /* Public */,
      27,    1,  310,    2, 0x0a /* Public */,
      28,    1,  313,    2, 0x0a /* Public */,
      29,    1,  316,    2, 0x0a /* Public */,
      30,    1,  319,    2, 0x0a /* Public */,
      31,    1,  322,    2, 0x0a /* Public */,
      32,    1,  325,    2, 0x0a /* Public */,
      33,    1,  328,    2, 0x0a /* Public */,
      34,    1,  331,    2, 0x0a /* Public */,
      35,    1,  334,    2, 0x0a /* Public */,
      36,    1,  337,    2, 0x0a /* Public */,
      37,    1,  340,    2, 0x0a /* Public */,
      38,    1,  343,    2, 0x0a /* Public */,
      39,    1,  346,    2, 0x0a /* Public */,
      40,    1,  349,    2, 0x0a /* Public */,
      41,    1,  352,    2, 0x0a /* Public */,
      42,    1,  355,    2, 0x0a /* Public */,
      43,    1,  358,    2, 0x0a /* Public */,
      44,    1,  361,    2, 0x0a /* Public */,
      45,    1,  364,    2, 0x0a /* Public */,
      46,    1,  367,    2, 0x0a /* Public */,
      47,    1,  370,    2, 0x0a /* Public */,
      48,    1,  373,    2, 0x0a /* Public */,
      49,    1,  376,    2, 0x0a /* Public */,
      50,    1,  379,    2, 0x0a /* Public */,
      51,    1,  382,    2, 0x0a /* Public */,
      52,    1,  385,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::Double,    5,
    QMetaType::Void, QMetaType::QCursor,    9,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Double,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,

       0        // eod
};

void cagd::GLWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GLWidget *_t = static_cast<GLWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->statusMessageChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->setCalculationTimeCurveNetwork((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->setCalculationTimeC0Surfaces((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->setCalculationTimeG1Surfaces((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->setCursor((*reinterpret_cast< const QCursor(*)>(_a[1]))); break;
        case 5: _t->_animate(); break;
        case 6: _t->set_angle_x((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->set_angle_y((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->set_angle_z((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->set_zoom_factor((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->set_trans_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->set_trans_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->set_trans_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: _t->set_theta_1((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 14: _t->set_theta_2((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->set_basis_function((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->set_weight_function((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->set_shape_parameter((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->set_shader((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->set_poligon_mode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->set_material((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->set_selected_shape((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->set_div_point_count((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->set_eps_1((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->set_eps_2((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 25: _t->set_vertices_check((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->set_vertices_size((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 27: _t->set_normals_check((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->set_normals_size((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 29: _t->set_tangents_check((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: _t->set_tangents_size((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 31: _t->set_boundaries_check((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 32: _t->set_boundaries_normal_check((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 33: _t->set_boundaries_normal_size((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 34: _t->set_geometry_check((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->set_geometry((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 36: _t->set_vertex_number((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->elephant_v102_f200_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 38: _t->m2_v3495_f666_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 39: _t->spot_v122_f240_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 40: _t->bunny_v113_f222_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 41: _t->bunny_v239_f474_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 42: _t->horse_v341_f678_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->m195_v2472_f4809_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 44: _t->angel_v1352_f2706_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 45: _t->dragon_v1386_f2772_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 46: _t->icosahedron_v12_f20_checked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (GLWidget::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::statusMessageChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (GLWidget::*_t)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::setCalculationTimeCurveNetwork)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (GLWidget::*_t)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::setCalculationTimeC0Surfaces)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (GLWidget::*_t)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::setCalculationTimeG1Surfaces)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (GLWidget::*_t)(const QCursor & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::setCursor)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject cagd::GLWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_cagd__GLWidget.data,
      qt_meta_data_cagd__GLWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *cagd::GLWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *cagd::GLWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_cagd__GLWidget.stringdata0))
        return static_cast<void*>(this);
    return QGLWidget::qt_metacast(_clname);
}

int cagd::GLWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 47)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 47;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 47)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 47;
    }
    return _id;
}

// SIGNAL 0
void cagd::GLWidget::statusMessageChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void cagd::GLWidget::setCalculationTimeCurveNetwork(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void cagd::GLWidget::setCalculationTimeC0Surfaces(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void cagd::GLWidget::setCalculationTimeG1Surfaces(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void cagd::GLWidget::setCursor(const QCursor & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
