/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Quit;
    QAction *actionelephant_v102_f200;
    QAction *actionbunny_v113_f222;
    QAction *actionbunny_v239_f474;
    QAction *actionspot_v122_f240;
    QAction *actionhorse_v341_f678;
    QAction *actionicosahedron_v12_f20;
    QAction *actionm195_v2472_f4809;
    QAction *actionm2_v3495_f6667;
    QAction *actiondragon_v1386_f2772;
    QAction *actionangel_v1352_f2706;
    QWidget *centralwidget;
    QMenuBar *menubar;
    QMenu *menu_File;
    QMenu *menuChange_model;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 600);
        action_Quit = new QAction(MainWindow);
        action_Quit->setObjectName(QStringLiteral("action_Quit"));
        actionelephant_v102_f200 = new QAction(MainWindow);
        actionelephant_v102_f200->setObjectName(QStringLiteral("actionelephant_v102_f200"));
        actionbunny_v113_f222 = new QAction(MainWindow);
        actionbunny_v113_f222->setObjectName(QStringLiteral("actionbunny_v113_f222"));
        actionbunny_v239_f474 = new QAction(MainWindow);
        actionbunny_v239_f474->setObjectName(QStringLiteral("actionbunny_v239_f474"));
        actionspot_v122_f240 = new QAction(MainWindow);
        actionspot_v122_f240->setObjectName(QStringLiteral("actionspot_v122_f240"));
        actionhorse_v341_f678 = new QAction(MainWindow);
        actionhorse_v341_f678->setObjectName(QStringLiteral("actionhorse_v341_f678"));
        actionicosahedron_v12_f20 = new QAction(MainWindow);
        actionicosahedron_v12_f20->setObjectName(QStringLiteral("actionicosahedron_v12_f20"));
        actionm195_v2472_f4809 = new QAction(MainWindow);
        actionm195_v2472_f4809->setObjectName(QStringLiteral("actionm195_v2472_f4809"));
        actionm2_v3495_f6667 = new QAction(MainWindow);
        actionm2_v3495_f6667->setObjectName(QStringLiteral("actionm2_v3495_f6667"));
        actiondragon_v1386_f2772 = new QAction(MainWindow);
        actiondragon_v1386_f2772->setObjectName(QStringLiteral("actiondragon_v1386_f2772"));
        actionangel_v1352_f2706 = new QAction(MainWindow);
        actionangel_v1352_f2706->setObjectName(QStringLiteral("actionangel_v1352_f2706"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        menu_File = new QMenu(menubar);
        menu_File->setObjectName(QStringLiteral("menu_File"));
        menuChange_model = new QMenu(menu_File);
        menuChange_model->setObjectName(QStringLiteral("menuChange_model"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu_File->menuAction());
        menu_File->addAction(action_Quit);
        menu_File->addAction(menuChange_model->menuAction());
        menuChange_model->addAction(actionelephant_v102_f200);
        menuChange_model->addAction(actionbunny_v113_f222);
        menuChange_model->addAction(actionbunny_v239_f474);
        menuChange_model->addAction(actionspot_v122_f240);
        menuChange_model->addAction(actionhorse_v341_f678);
        menuChange_model->addAction(actionicosahedron_v12_f20);
        menuChange_model->addAction(actionm195_v2472_f4809);
        menuChange_model->addAction(actionm2_v3495_f6667);
        menuChange_model->addAction(actiondragon_v1386_f2772);
        menuChange_model->addAction(actionangel_v1352_f2706);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Computer Aided Geometric Design | Simple Qt-Framework | Spring, 2019", nullptr));
        action_Quit->setText(QApplication::translate("MainWindow", "&Quit", nullptr));
#ifndef QT_NO_SHORTCUT
        action_Quit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_NO_SHORTCUT
        actionelephant_v102_f200->setText(QApplication::translate("MainWindow", "elephant_v102_f200", nullptr));
        actionbunny_v113_f222->setText(QApplication::translate("MainWindow", "bunny_v113_f222", nullptr));
        actionbunny_v239_f474->setText(QApplication::translate("MainWindow", "bunny_v239_f474", nullptr));
        actionspot_v122_f240->setText(QApplication::translate("MainWindow", "spot_v122_f240", nullptr));
        actionhorse_v341_f678->setText(QApplication::translate("MainWindow", "horse_v341_f678", nullptr));
        actionicosahedron_v12_f20->setText(QApplication::translate("MainWindow", "icosahedron_v12_f20", nullptr));
        actionm195_v2472_f4809->setText(QApplication::translate("MainWindow", "m195_v2472_f4809", nullptr));
        actionm2_v3495_f6667->setText(QApplication::translate("MainWindow", "m2_v3495_f6667", nullptr));
        actiondragon_v1386_f2772->setText(QApplication::translate("MainWindow", "dragon_v1386_f2772", nullptr));
        actionangel_v1352_f2706->setText(QApplication::translate("MainWindow", "angel_v1352_f2706", nullptr));
        menu_File->setTitle(QApplication::translate("MainWindow", "Menu", nullptr));
        menuChange_model->setTitle(QApplication::translate("MainWindow", "Change model", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
