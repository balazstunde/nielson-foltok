/********************************************************************************
** Form generated from reading UI file 'SideWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIDEWIDGET_H
#define UI_SIDEWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SideWidget
{
public:
    QGroupBox *groupBox;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QSlider *rotate_x_slider;
    QLabel *label_2;
    QSlider *rotate_y_slider;
    QLabel *label_3;
    QSlider *rotate_z_slider;
    QLabel *label_4;
    QDoubleSpinBox *zoom_factor_spin_box;
    QLabel *label_5;
    QDoubleSpinBox *trans_x_spin_box;
    QLabel *label_6;
    QDoubleSpinBox *trans_y_spin_box;
    QLabel *label_7;
    QDoubleSpinBox *trans_z_spin_box;
    QPushButton *pushButton;
    QGroupBox *groupBox_2;
    QLabel *label_17;
    QDoubleSpinBox *eps_1;
    QLabel *label_12;
    QComboBox *basis_function;
    QDoubleSpinBox *eps_2;
    QSpinBox *div_point_count;
    QDoubleSpinBox *theta_1;
    QLabel *label_11;
    QLabel *label_13;
    QDoubleSpinBox *shape_parameter;
    QDoubleSpinBox *theta_2;
    QLabel *label_16;
    QLabel *label_8;
    QLabel *label_10;
    QDoubleSpinBox *theta_3;
    QLabel *label_15;
    QGroupBox *groupBox_3;
    QCheckBox *vertices_check;
    QDoubleSpinBox *vertices_size;
    QCheckBox *vertex_normals_check;
    QDoubleSpinBox *vertex_normals_size;
    QCheckBox *tangents_check;
    QDoubleSpinBox *tangents_size;
    QCheckBox *boundaries_check;
    QCheckBox *boundary_normals_check;
    QDoubleSpinBox *boundary_normals_size;
    QLabel *label_9;
    QComboBox *shader;
    QLabel *label_14;
    QComboBox *material;
    QCheckBox *poligon_mode;
    QCheckBox *show_geometry;
    QLabel *label_18;
    QComboBox *my_geometry;
    QLabel *label_19;
    QSpinBox *vertex_number;

    void setupUi(QWidget *SideWidget)
    {
        if (SideWidget->objectName().isEmpty())
            SideWidget->setObjectName(QStringLiteral("SideWidget"));
        SideWidget->resize(302, 861);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SideWidget->sizePolicy().hasHeightForWidth());
        SideWidget->setSizePolicy(sizePolicy);
        SideWidget->setMinimumSize(QSize(269, 0));
        groupBox = new QGroupBox(SideWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 10, 271, 241));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(3, 21, 261, 213));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        rotate_x_slider = new QSlider(layoutWidget);
        rotate_x_slider->setObjectName(QStringLiteral("rotate_x_slider"));
        rotate_x_slider->setMinimum(-180);
        rotate_x_slider->setMaximum(180);
        rotate_x_slider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(0, QFormLayout::FieldRole, rotate_x_slider);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        rotate_y_slider = new QSlider(layoutWidget);
        rotate_y_slider->setObjectName(QStringLiteral("rotate_y_slider"));
        rotate_y_slider->setMinimum(-180);
        rotate_y_slider->setMaximum(180);
        rotate_y_slider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(1, QFormLayout::FieldRole, rotate_y_slider);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        rotate_z_slider = new QSlider(layoutWidget);
        rotate_z_slider->setObjectName(QStringLiteral("rotate_z_slider"));
        rotate_z_slider->setMinimum(-180);
        rotate_z_slider->setMaximum(180);
        rotate_z_slider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(2, QFormLayout::FieldRole, rotate_z_slider);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        zoom_factor_spin_box = new QDoubleSpinBox(layoutWidget);
        zoom_factor_spin_box->setObjectName(QStringLiteral("zoom_factor_spin_box"));
        zoom_factor_spin_box->setDecimals(5);
        zoom_factor_spin_box->setMinimum(0.0001);
        zoom_factor_spin_box->setMaximum(10000);
        zoom_factor_spin_box->setSingleStep(0.1);
        zoom_factor_spin_box->setValue(1);

        formLayout->setWidget(3, QFormLayout::FieldRole, zoom_factor_spin_box);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        trans_x_spin_box = new QDoubleSpinBox(layoutWidget);
        trans_x_spin_box->setObjectName(QStringLiteral("trans_x_spin_box"));
        trans_x_spin_box->setMinimum(-100);
        trans_x_spin_box->setMaximum(100);
        trans_x_spin_box->setSingleStep(0.1);

        formLayout->setWidget(4, QFormLayout::FieldRole, trans_x_spin_box);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        trans_y_spin_box = new QDoubleSpinBox(layoutWidget);
        trans_y_spin_box->setObjectName(QStringLiteral("trans_y_spin_box"));
        trans_y_spin_box->setMinimum(-100);
        trans_y_spin_box->setMaximum(100);
        trans_y_spin_box->setSingleStep(0.1);

        formLayout->setWidget(5, QFormLayout::FieldRole, trans_y_spin_box);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_7);

        trans_z_spin_box = new QDoubleSpinBox(layoutWidget);
        trans_z_spin_box->setObjectName(QStringLiteral("trans_z_spin_box"));
        trans_z_spin_box->setMinimum(-100);
        trans_z_spin_box->setMaximum(100);
        trans_z_spin_box->setSingleStep(0.1);

        formLayout->setWidget(6, QFormLayout::FieldRole, trans_z_spin_box);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(7, QFormLayout::SpanningRole, pushButton);

        groupBox_2 = new QGroupBox(SideWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 250, 271, 251));
        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(10, 216, 20, 20));
        eps_1 = new QDoubleSpinBox(groupBox_2);
        eps_1->setObjectName(QStringLiteral("eps_1"));
        eps_1->setGeometry(QRect(130, 190, 121, 20));
        eps_1->setMinimum(-100);
        eps_1->setMaximum(100);
        eps_1->setSingleStep(0.1);
        eps_1->setValue(0.2);
        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(10, 164, 21, 20));
        basis_function = new QComboBox(groupBox_2);
        basis_function->addItem(QString());
        basis_function->addItem(QString());
        basis_function->addItem(QString());
        basis_function->addItem(QString());
        basis_function->setObjectName(QStringLiteral("basis_function"));
        basis_function->setGeometry(QRect(90, 50, 161, 20));
        eps_2 = new QDoubleSpinBox(groupBox_2);
        eps_2->setObjectName(QStringLiteral("eps_2"));
        eps_2->setGeometry(QRect(130, 216, 121, 20));
        eps_2->setMinimum(-100);
        eps_2->setMaximum(100);
        eps_2->setSingleStep(0.1);
        eps_2->setValue(0);
        div_point_count = new QSpinBox(groupBox_2);
        div_point_count->setObjectName(QStringLiteral("div_point_count"));
        div_point_count->setGeometry(QRect(130, 20, 121, 20));
        div_point_count->setMaximum(500000);
        div_point_count->setSingleStep(5);
        div_point_count->setValue(20);
        theta_1 = new QDoubleSpinBox(groupBox_2);
        theta_1->setObjectName(QStringLiteral("theta_1"));
        theta_1->setGeometry(QRect(130, 112, 121, 21));
        theta_1->setMinimum(-100);
        theta_1->setMaximum(100);
        theta_1->setSingleStep(0.1);
        theta_1->setValue(0.2);
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(10, 50, 71, 20));
        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(10, 86, 83, 20));
        shape_parameter = new QDoubleSpinBox(groupBox_2);
        shape_parameter->setObjectName(QStringLiteral("shape_parameter"));
        shape_parameter->setGeometry(QRect(130, 86, 121, 20));
        shape_parameter->setDecimals(5);
        shape_parameter->setMinimum(0.0001);
        shape_parameter->setMaximum(10000);
        shape_parameter->setSingleStep(0.1);
        shape_parameter->setValue(1);
        theta_2 = new QDoubleSpinBox(groupBox_2);
        theta_2->setObjectName(QStringLiteral("theta_2"));
        theta_2->setGeometry(QRect(130, 138, 121, 20));
        theta_2->setMinimum(-100);
        theta_2->setMaximum(100);
        theta_2->setSingleStep(0.1);
        label_16 = new QLabel(groupBox_2);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(10, 190, 20, 20));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 138, 21, 20));
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(10, 112, 21, 20));
        theta_3 = new QDoubleSpinBox(groupBox_2);
        theta_3->setObjectName(QStringLiteral("theta_3"));
        theta_3->setGeometry(QRect(130, 164, 121, 20));
        theta_3->setMinimum(-100);
        theta_3->setMaximum(100);
        theta_3->setSingleStep(0.1);
        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(10, 20, 72, 20));
        groupBox_3 = new QGroupBox(SideWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setGeometry(QRect(20, 500, 271, 361));
        vertices_check = new QCheckBox(groupBox_3);
        vertices_check->setObjectName(QStringLiteral("vertices_check"));
        vertices_check->setGeometry(QRect(10, 20, 86, 17));
        vertices_size = new QDoubleSpinBox(groupBox_3);
        vertices_size->setObjectName(QStringLiteral("vertices_size"));
        vertices_size->setGeometry(QRect(190, 20, 61, 22));
        vertices_size->setDecimals(5);
        vertices_size->setSingleStep(0.01);
        vertices_size->setValue(0.04);
        vertex_normals_check = new QCheckBox(groupBox_3);
        vertex_normals_check->setObjectName(QStringLiteral("vertex_normals_check"));
        vertex_normals_check->setGeometry(QRect(10, 50, 131, 17));
        vertex_normals_size = new QDoubleSpinBox(groupBox_3);
        vertex_normals_size->setObjectName(QStringLiteral("vertex_normals_size"));
        vertex_normals_size->setGeometry(QRect(190, 50, 61, 22));
        vertex_normals_size->setDecimals(5);
        vertex_normals_size->setSingleStep(0.01);
        vertex_normals_size->setValue(0.09);
        tangents_check = new QCheckBox(groupBox_3);
        tangents_check->setObjectName(QStringLiteral("tangents_check"));
        tangents_check->setGeometry(QRect(10, 80, 171, 17));
        tangents_size = new QDoubleSpinBox(groupBox_3);
        tangents_size->setObjectName(QStringLiteral("tangents_size"));
        tangents_size->setGeometry(QRect(190, 80, 61, 22));
        tangents_size->setDecimals(5);
        tangents_size->setSingleStep(0.01);
        tangents_size->setValue(0.09);
        boundaries_check = new QCheckBox(groupBox_3);
        boundaries_check->setObjectName(QStringLiteral("boundaries_check"));
        boundaries_check->setGeometry(QRect(10, 110, 171, 17));
        boundary_normals_check = new QCheckBox(groupBox_3);
        boundary_normals_check->setObjectName(QStringLiteral("boundary_normals_check"));
        boundary_normals_check->setGeometry(QRect(10, 140, 171, 17));
        boundary_normals_size = new QDoubleSpinBox(groupBox_3);
        boundary_normals_size->setObjectName(QStringLiteral("boundary_normals_size"));
        boundary_normals_size->setGeometry(QRect(190, 140, 61, 22));
        boundary_normals_size->setDecimals(5);
        boundary_normals_size->setSingleStep(0.01);
        boundary_normals_size->setValue(0.09);
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 170, 38, 20));
        shader = new QComboBox(groupBox_3);
        shader->addItem(QString());
        shader->addItem(QString());
        shader->addItem(QString());
        shader->setObjectName(QStringLiteral("shader"));
        shader->setGeometry(QRect(81, 170, 171, 20));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(10, 200, 42, 20));
        material = new QComboBox(groupBox_3);
        material->addItem(QString());
        material->addItem(QString());
        material->addItem(QString());
        material->addItem(QString());
        material->addItem(QString());
        material->addItem(QString());
        material->addItem(QString());
        material->addItem(QString());
        material->setObjectName(QStringLiteral("material"));
        material->setGeometry(QRect(81, 200, 171, 20));
        poligon_mode = new QCheckBox(groupBox_3);
        poligon_mode->setObjectName(QStringLiteral("poligon_mode"));
        poligon_mode->setGeometry(QRect(10, 230, 86, 17));
        show_geometry = new QCheckBox(groupBox_3);
        show_geometry->setObjectName(QStringLiteral("show_geometry"));
        show_geometry->setGeometry(QRect(10, 320, 171, 17));
        show_geometry->setChecked(true);
        label_18 = new QLabel(groupBox_3);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setGeometry(QRect(10, 290, 61, 20));
        my_geometry = new QComboBox(groupBox_3);
        my_geometry->addItem(QString());
        my_geometry->addItem(QString());
        my_geometry->addItem(QString());
        my_geometry->addItem(QString());
        my_geometry->addItem(QString());
        my_geometry->addItem(QString());
        my_geometry->addItem(QString());
        my_geometry->setObjectName(QStringLiteral("my_geometry"));
        my_geometry->setGeometry(QRect(80, 290, 171, 20));
        label_19 = new QLabel(groupBox_3);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setGeometry(QRect(10, 260, 91, 20));
        vertex_number = new QSpinBox(groupBox_3);
        vertex_number->setObjectName(QStringLiteral("vertex_number"));
        vertex_number->setGeometry(QRect(110, 260, 141, 20));
        vertex_number->setMaximum(500000);
        vertex_number->setSingleStep(1);
        vertex_number->setValue(60);
#ifndef QT_NO_SHORTCUT
        label->setBuddy(rotate_x_slider);
        label_2->setBuddy(rotate_y_slider);
        label_3->setBuddy(rotate_z_slider);
        label_4->setBuddy(zoom_factor_spin_box);
        label_5->setBuddy(trans_x_spin_box);
        label_6->setBuddy(trans_y_spin_box);
        label_7->setBuddy(trans_z_spin_box);
#endif // QT_NO_SHORTCUT

        retranslateUi(SideWidget);

        QMetaObject::connectSlotsByName(SideWidget);
    } // setupUi

    void retranslateUi(QWidget *SideWidget)
    {
        SideWidget->setWindowTitle(QApplication::translate("SideWidget", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("SideWidget", "Transformations", nullptr));
        label->setText(QApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_2->setText(QApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_3->setText(QApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_4->setText(QApplication::translate("SideWidget", "Zoom factor", nullptr));
#ifndef QT_NO_TOOLTIP
        zoom_factor_spin_box->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("SideWidget", "Translate along x", nullptr));
        label_6->setText(QApplication::translate("SideWidget", "Translate along y", nullptr));
        label_7->setText(QApplication::translate("SideWidget", "Translate along z", nullptr));
        pushButton->setText(QApplication::translate("SideWidget", "Reset to original view", nullptr));
        groupBox_2->setTitle(QApplication::translate("SideWidget", "Parameter settings", nullptr));
        label_17->setText(QApplication::translate("SideWidget", " \316\265_2", nullptr));
        label_12->setText(QApplication::translate("SideWidget", " \316\270_3", nullptr));
        basis_function->setItemText(0, QApplication::translate("SideWidget", "Second order trigonometric", nullptr));
        basis_function->setItemText(1, QApplication::translate("SideWidget", "Quartic polinomial", nullptr));
        basis_function->setItemText(2, QApplication::translate("SideWidget", "First order algebraic trigonometric normalized", nullptr));
        basis_function->setItemText(3, QApplication::translate("SideWidget", "Cubic Bezier", nullptr));

        label_11->setText(QApplication::translate("SideWidget", "Basis functions", nullptr));
        label_13->setText(QApplication::translate("SideWidget", "Shape parameter", nullptr));
#ifndef QT_NO_TOOLTIP
        shape_parameter->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label_16->setText(QApplication::translate("SideWidget", " \316\265_1", nullptr));
        label_8->setText(QApplication::translate("SideWidget", " \316\270_2", nullptr));
        label_10->setText(QApplication::translate("SideWidget", " \316\270_1", nullptr));
        label_15->setText(QApplication::translate("SideWidget", "Div point count", nullptr));
        groupBox_3->setTitle(QApplication::translate("SideWidget", "Render settings", nullptr));
        vertices_check->setText(QApplication::translate("SideWidget", "Show vertices", nullptr));
        vertex_normals_check->setText(QApplication::translate("SideWidget", "Show vertex normals", nullptr));
        tangents_check->setText(QApplication::translate("SideWidget", "Show approximated tangents", nullptr));
        boundaries_check->setText(QApplication::translate("SideWidget", "Show boundaries", nullptr));
        boundary_normals_check->setText(QApplication::translate("SideWidget", "Show boundary normals", nullptr));
        label_9->setText(QApplication::translate("SideWidget", "Shader:", nullptr));
        shader->setItemText(0, QApplication::translate("SideWidget", "two sided lighting", nullptr));
        shader->setItemText(1, QApplication::translate("SideWidget", "reflection lines", nullptr));
        shader->setItemText(2, QApplication::translate("SideWidget", "toonify", nullptr));

        label_14->setText(QApplication::translate("SideWidget", "Material:", nullptr));
        material->setItemText(0, QApplication::translate("SideWidget", "MatFBBrass", nullptr));
        material->setItemText(1, QApplication::translate("SideWidget", "none", nullptr));
        material->setItemText(2, QApplication::translate("SideWidget", "MatFBGold", nullptr));
        material->setItemText(3, QApplication::translate("SideWidget", "MatFBSilver", nullptr));
        material->setItemText(4, QApplication::translate("SideWidget", "MatFBEmerald", nullptr));
        material->setItemText(5, QApplication::translate("SideWidget", "MatFBPearl", nullptr));
        material->setItemText(6, QApplication::translate("SideWidget", "MatFBRuby", nullptr));
        material->setItemText(7, QApplication::translate("SideWidget", "MatFBTurquoise;", nullptr));

        poligon_mode->setText(QApplication::translate("SideWidget", "Poligon Mode", nullptr));
        show_geometry->setText(QApplication::translate("SideWidget", "Show geometry", nullptr));
        label_18->setText(QApplication::translate("SideWidget", "Geometry", nullptr));
        my_geometry->setItemText(0, QApplication::translate("SideWidget", "Curves", nullptr));
        my_geometry->setItemText(1, QApplication::translate("SideWidget", "Triangles", nullptr));
        my_geometry->setItemText(2, QApplication::translate("SideWidget", "Faces", nullptr));
        my_geometry->setItemText(3, QApplication::translate("SideWidget", "Curve network", nullptr));
        my_geometry->setItemText(4, QApplication::translate("SideWidget", "C0 surface", nullptr));
        my_geometry->setItemText(5, QApplication::translate("SideWidget", "Before avarage G1 as point cloud", nullptr));
        my_geometry->setItemText(6, QApplication::translate("SideWidget", "G1 surface", nullptr));

        label_19->setText(QApplication::translate("SideWidget", "Highlighted vertex:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SideWidget: public Ui_SideWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIDEWIDGET_H
